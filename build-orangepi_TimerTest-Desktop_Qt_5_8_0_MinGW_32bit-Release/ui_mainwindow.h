/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionExit;
    QAction *actionOptions;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_58;
    QVBoxLayout *verticalLayout_57;
    QHBoxLayout *horizontalLayout_134;
    QVBoxLayout *verticalLayout_56;
    QHBoxLayout *horizontalLayout_106;
    QVBoxLayout *verticalLayout_54;
    QHBoxLayout *horizontalLayout_102;
    QLabel *label_7;
    QLCDNumber *lcdNumber_onlineNum;
    QSpacerItem *horizontalSpacer_84;
    QHBoxLayout *horizontalLayout_103;
    QLabel *label_169;
    QLCDNumber *lcdNumber_testNum;
    QSpacerItem *horizontalSpacer_85;
    QHBoxLayout *horizontalLayout_108;
    QLabel *label_172;
    QLCDNumber *lcdNumber_errorNum;
    QPushButton *pushButton_clearErrorNum;
    QHBoxLayout *horizontalLayout_104;
    QLabel *label_168;
    QLCDNumber *lcdNumber_okNum;
    QPushButton *pushButton_clearOkNum;
    QSpacerItem *verticalSpacer;
    QLabel *label_nowStep_value;
    QLabel *label_170;
    QPushButton *pushButton_stop;
    QGroupBox *groupBox_24;
    QVBoxLayout *verticalLayout_61;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_65;
    QVBoxLayout *verticalLayout_60;
    QHBoxLayout *horizontalLayout_109;
    QPushButton *pushButton_dev_1;
    QPushButton *pushButton_dev_2;
    QPushButton *pushButton_dev_3;
    QPushButton *pushButton_dev_4;
    QPushButton *pushButton_dev_5;
    QHBoxLayout *horizontalLayout_110;
    QPushButton *pushButton_dev_6;
    QPushButton *pushButton_dev_7;
    QPushButton *pushButton_dev_8;
    QPushButton *pushButton_dev_9;
    QPushButton *pushButton_dev_10;
    QHBoxLayout *horizontalLayout_111;
    QPushButton *pushButton_dev_11;
    QPushButton *pushButton_dev_12;
    QPushButton *pushButton_dev_13;
    QPushButton *pushButton_dev_14;
    QPushButton *pushButton_dev_15;
    QHBoxLayout *horizontalLayout_112;
    QPushButton *pushButton_dev_16;
    QPushButton *pushButton_dev_17;
    QPushButton *pushButton_dev_18;
    QPushButton *pushButton_dev_19;
    QPushButton *pushButton_dev_20;
    QHBoxLayout *horizontalLayout_113;
    QPushButton *pushButton_dev_21;
    QPushButton *pushButton_dev_22;
    QPushButton *pushButton_dev_23;
    QPushButton *pushButton_dev_24;
    QPushButton *pushButton_dev_25;
    QHBoxLayout *horizontalLayout_114;
    QPushButton *pushButton_dev_26;
    QPushButton *pushButton_dev_27;
    QPushButton *pushButton_dev_28;
    QPushButton *pushButton_dev_29;
    QPushButton *pushButton_dev_30;
    QVBoxLayout *verticalLayout_62;
    QHBoxLayout *horizontalLayout_115;
    QPushButton *pushButton_dev_31;
    QPushButton *pushButton_dev_32;
    QPushButton *pushButton_dev_33;
    QPushButton *pushButton_dev_34;
    QPushButton *pushButton_dev_35;
    QHBoxLayout *horizontalLayout_116;
    QPushButton *pushButton_dev_36;
    QPushButton *pushButton_dev_37;
    QPushButton *pushButton_dev_38;
    QPushButton *pushButton_dev_39;
    QPushButton *pushButton_dev_40;
    QHBoxLayout *horizontalLayout_117;
    QPushButton *pushButton_dev_41;
    QPushButton *pushButton_dev_42;
    QPushButton *pushButton_dev_43;
    QPushButton *pushButton_dev_44;
    QPushButton *pushButton_dev_45;
    QHBoxLayout *horizontalLayout_118;
    QPushButton *pushButton_dev_46;
    QPushButton *pushButton_dev_47;
    QPushButton *pushButton_dev_48;
    QPushButton *pushButton_dev_49;
    QPushButton *pushButton_dev_50;
    QHBoxLayout *horizontalLayout_119;
    QPushButton *pushButton_dev_51;
    QPushButton *pushButton_dev_52;
    QPushButton *pushButton_dev_53;
    QPushButton *pushButton_dev_54;
    QPushButton *pushButton_dev_55;
    QHBoxLayout *horizontalLayout_120;
    QPushButton *pushButton_dev_56;
    QPushButton *pushButton_dev_57;
    QPushButton *pushButton_dev_58;
    QPushButton *pushButton_dev_59;
    QPushButton *pushButton_dev_60;
    QVBoxLayout *verticalLayout_63;
    QHBoxLayout *horizontalLayout_121;
    QPushButton *pushButton_dev_61;
    QPushButton *pushButton_dev_62;
    QPushButton *pushButton_dev_63;
    QPushButton *pushButton_dev_64;
    QPushButton *pushButton_dev_65;
    QHBoxLayout *horizontalLayout_122;
    QPushButton *pushButton_dev_66;
    QPushButton *pushButton_dev_67;
    QPushButton *pushButton_dev_68;
    QPushButton *pushButton_dev_69;
    QPushButton *pushButton_dev_70;
    QHBoxLayout *horizontalLayout_123;
    QPushButton *pushButton_dev_71;
    QPushButton *pushButton_dev_72;
    QPushButton *pushButton_dev_73;
    QPushButton *pushButton_dev_74;
    QPushButton *pushButton_dev_75;
    QHBoxLayout *horizontalLayout_124;
    QPushButton *pushButton_dev_76;
    QPushButton *pushButton_dev_77;
    QPushButton *pushButton_dev_78;
    QPushButton *pushButton_dev_79;
    QPushButton *pushButton_dev_80;
    QHBoxLayout *horizontalLayout_125;
    QPushButton *pushButton_dev_81;
    QPushButton *pushButton_dev_82;
    QPushButton *pushButton_dev_83;
    QPushButton *pushButton_dev_84;
    QPushButton *pushButton_dev_85;
    QHBoxLayout *horizontalLayout_126;
    QPushButton *pushButton_dev_86;
    QPushButton *pushButton_dev_87;
    QPushButton *pushButton_dev_88;
    QPushButton *pushButton_dev_89;
    QPushButton *pushButton_dev_90;
    QVBoxLayout *verticalLayout_64;
    QHBoxLayout *horizontalLayout_127;
    QPushButton *pushButton_dev_91;
    QPushButton *pushButton_dev_92;
    QPushButton *pushButton_dev_93;
    QPushButton *pushButton_dev_94;
    QPushButton *pushButton_dev_95;
    QHBoxLayout *horizontalLayout_128;
    QPushButton *pushButton_dev_96;
    QPushButton *pushButton_dev_97;
    QPushButton *pushButton_dev_98;
    QPushButton *pushButton_dev_99;
    QPushButton *pushButton_dev_100;
    QHBoxLayout *horizontalLayout_129;
    QPushButton *pushButton_dev_101;
    QPushButton *pushButton_dev_102;
    QPushButton *pushButton_dev_103;
    QPushButton *pushButton_dev_104;
    QPushButton *pushButton_dev_105;
    QHBoxLayout *horizontalLayout_130;
    QPushButton *pushButton_dev_106;
    QPushButton *pushButton_dev_107;
    QPushButton *pushButton_dev_108;
    QPushButton *pushButton_dev_109;
    QPushButton *pushButton_dev_110;
    QHBoxLayout *horizontalLayout_131;
    QPushButton *pushButton_dev_111;
    QPushButton *pushButton_dev_112;
    QPushButton *pushButton_dev_113;
    QPushButton *pushButton_dev_114;
    QPushButton *pushButton_dev_115;
    QHBoxLayout *horizontalLayout_132;
    QPushButton *pushButton_dev_116;
    QPushButton *pushButton_dev_117;
    QPushButton *pushButton_dev_118;
    QPushButton *pushButton_dev_119;
    QPushButton *pushButton_dev_120;
    QVBoxLayout *verticalLayout_55;
    QHBoxLayout *horizontalLayout_105;
    QSpacerItem *horizontalSpacer;
    QLabel *label_status_led;
    QHBoxLayout *horizontalLayout_133;
    QSpacerItem *horizontalSpacer_87;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_107;
    QSpacerItem *horizontalSpacer_86;
    QLabel *label_5;
    QLabel *label_timerStepTestTime;
    QPushButton *pushButton_ResetTest;
    QVBoxLayout *verticalLayout_53;
    QHBoxLayout *horizontalLayout_100;
    QPushButton *pushButton_startStep_1;
    QFrame *line_25;
    QPushButton *pushButton_startStep_2;
    QFrame *line_26;
    QPushButton *pushButton_startStep_3;
    QFrame *line_27;
    QPushButton *pushButton_startStep_4;
    QFrame *line_28;
    QPushButton *pushButton_startStep_5;
    QFrame *line_21;
    QHBoxLayout *horizontalLayout_101;
    QPushButton *pushButton_startStep_6;
    QFrame *line_32;
    QPushButton *pushButton_startStep_7;
    QFrame *line_31;
    QPushButton *pushButton_startStep_8;
    QFrame *line_30;
    QPushButton *pushButton_startStep_9;
    QFrame *line_29;
    QPushButton *pushButton_startStep_10;
    QFrame *line_22;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_40;
    QWidget *widget;
    QVBoxLayout *verticalLayout_13;
    QHBoxLayout *horizontalLayout_15;
    QVBoxLayout *verticalLayout_12;
    QHBoxLayout *horizontalLayout_5;
    QTabWidget *tabWidget_2;
    QWidget *tab_3;
    QHBoxLayout *horizontalLayout_18;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_8;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_1;
    QLabel *label_9;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_11;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_1;
    QLabel *label_10;
    QSpacerItem *horizontalSpacer_2;
    QFrame *line;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_13;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_1;
    QLabel *label_15;
    QSpacerItem *horizontalSpacer_9;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_14;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_1;
    QLabel *label_12;
    QSpacerItem *horizontalSpacer_3;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_16;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_2;
    QLabel *label_17;
    QSpacerItem *horizontalSpacer_10;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_18;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_2;
    QLabel *label_19;
    QSpacerItem *horizontalSpacer_4;
    QFrame *line_2;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_20;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_2;
    QLabel *label_21;
    QSpacerItem *horizontalSpacer_11;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_22;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_2;
    QLabel *label_23;
    QSpacerItem *horizontalSpacer_5;
    QWidget *tab_4;
    QHBoxLayout *horizontalLayout_27;
    QGroupBox *groupBox_6;
    QVBoxLayout *verticalLayout_16;
    QVBoxLayout *verticalLayout_17;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_24;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_3;
    QLabel *label_25;
    QSpacerItem *horizontalSpacer_12;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_26;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_3;
    QLabel *label_27;
    QSpacerItem *horizontalSpacer_13;
    QFrame *line_3;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_28;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_3;
    QLabel *label_29;
    QSpacerItem *horizontalSpacer_14;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_30;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_3;
    QLabel *label_31;
    QSpacerItem *horizontalSpacer_15;
    QGroupBox *groupBox_7;
    QVBoxLayout *verticalLayout_18;
    QVBoxLayout *verticalLayout_19;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_32;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_4;
    QLabel *label_33;
    QSpacerItem *horizontalSpacer_16;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_34;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_4;
    QLabel *label_35;
    QSpacerItem *horizontalSpacer_17;
    QFrame *line_4;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_36;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_4;
    QLabel *label_37;
    QSpacerItem *horizontalSpacer_18;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_38;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_4;
    QLabel *label_39;
    QSpacerItem *horizontalSpacer_19;
    QWidget *tab_5;
    QHBoxLayout *horizontalLayout_36;
    QGroupBox *groupBox_9;
    QVBoxLayout *verticalLayout_22;
    QVBoxLayout *verticalLayout_21;
    QHBoxLayout *horizontalLayout_28;
    QLabel *label_40;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_5;
    QLabel *label_41;
    QSpacerItem *horizontalSpacer_20;
    QHBoxLayout *horizontalLayout_29;
    QLabel *label_42;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_5;
    QLabel *label_43;
    QSpacerItem *horizontalSpacer_21;
    QFrame *line_5;
    QHBoxLayout *horizontalLayout_30;
    QLabel *label_44;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_5;
    QLabel *label_45;
    QSpacerItem *horizontalSpacer_22;
    QHBoxLayout *horizontalLayout_31;
    QLabel *label_46;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_5;
    QLabel *label_47;
    QSpacerItem *horizontalSpacer_23;
    QGroupBox *groupBox_8;
    QVBoxLayout *verticalLayout_20;
    QVBoxLayout *verticalLayout_23;
    QHBoxLayout *horizontalLayout_32;
    QLabel *label_48;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_6;
    QLabel *label_49;
    QSpacerItem *horizontalSpacer_24;
    QHBoxLayout *horizontalLayout_33;
    QLabel *label_50;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_6;
    QLabel *label_51;
    QSpacerItem *horizontalSpacer_25;
    QFrame *line_6;
    QHBoxLayout *horizontalLayout_34;
    QLabel *label_52;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_6;
    QLabel *label_53;
    QSpacerItem *horizontalSpacer_26;
    QHBoxLayout *horizontalLayout_35;
    QLabel *label_54;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_6;
    QLabel *label_55;
    QSpacerItem *horizontalSpacer_27;
    QWidget *tab_6;
    QHBoxLayout *horizontalLayout_45;
    QGroupBox *groupBox_11;
    QVBoxLayout *verticalLayout_26;
    QVBoxLayout *verticalLayout_25;
    QHBoxLayout *horizontalLayout_37;
    QLabel *label_56;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_7;
    QLabel *label_57;
    QSpacerItem *horizontalSpacer_28;
    QHBoxLayout *horizontalLayout_38;
    QLabel *label_58;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_7;
    QLabel *label_59;
    QSpacerItem *horizontalSpacer_29;
    QFrame *line_7;
    QHBoxLayout *horizontalLayout_39;
    QLabel *label_60;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_7;
    QLabel *label_61;
    QSpacerItem *horizontalSpacer_30;
    QHBoxLayout *horizontalLayout_40;
    QLabel *label_62;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_7;
    QLabel *label_63;
    QSpacerItem *horizontalSpacer_31;
    QGroupBox *groupBox_10;
    QVBoxLayout *verticalLayout_24;
    QVBoxLayout *verticalLayout_27;
    QHBoxLayout *horizontalLayout_41;
    QLabel *label_64;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_8;
    QLabel *label_65;
    QSpacerItem *horizontalSpacer_32;
    QHBoxLayout *horizontalLayout_42;
    QLabel *label_66;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_8;
    QLabel *label_67;
    QSpacerItem *horizontalSpacer_33;
    QFrame *line_8;
    QHBoxLayout *horizontalLayout_43;
    QLabel *label_68;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_8;
    QLabel *label_69;
    QSpacerItem *horizontalSpacer_34;
    QHBoxLayout *horizontalLayout_44;
    QLabel *label_70;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_8;
    QLabel *label_71;
    QSpacerItem *horizontalSpacer_35;
    QWidget *tab_7;
    QHBoxLayout *horizontalLayout_54;
    QGroupBox *groupBox_13;
    QVBoxLayout *verticalLayout_30;
    QVBoxLayout *verticalLayout_29;
    QHBoxLayout *horizontalLayout_46;
    QLabel *label_72;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_9;
    QLabel *label_73;
    QSpacerItem *horizontalSpacer_36;
    QHBoxLayout *horizontalLayout_47;
    QLabel *label_74;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_9;
    QLabel *label_75;
    QSpacerItem *horizontalSpacer_37;
    QFrame *line_9;
    QHBoxLayout *horizontalLayout_48;
    QLabel *label_76;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_9;
    QLabel *label_77;
    QSpacerItem *horizontalSpacer_38;
    QHBoxLayout *horizontalLayout_49;
    QLabel *label_78;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_9;
    QLabel *label_79;
    QSpacerItem *horizontalSpacer_39;
    QGroupBox *groupBox_12;
    QVBoxLayout *verticalLayout_28;
    QVBoxLayout *verticalLayout_31;
    QHBoxLayout *horizontalLayout_50;
    QLabel *label_80;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_10;
    QLabel *label_81;
    QSpacerItem *horizontalSpacer_40;
    QHBoxLayout *horizontalLayout_51;
    QLabel *label_82;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_10;
    QLabel *label_83;
    QSpacerItem *horizontalSpacer_41;
    QFrame *line_10;
    QHBoxLayout *horizontalLayout_52;
    QLabel *label_84;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_10;
    QLabel *label_85;
    QSpacerItem *horizontalSpacer_42;
    QHBoxLayout *horizontalLayout_53;
    QLabel *label_86;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_10;
    QLabel *label_87;
    QSpacerItem *horizontalSpacer_43;
    QWidget *tab_8;
    QHBoxLayout *horizontalLayout_63;
    QGroupBox *groupBox_15;
    QVBoxLayout *verticalLayout_34;
    QVBoxLayout *verticalLayout_33;
    QHBoxLayout *horizontalLayout_55;
    QLabel *label_88;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_11;
    QLabel *label_89;
    QSpacerItem *horizontalSpacer_44;
    QHBoxLayout *horizontalLayout_56;
    QLabel *label_90;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_11;
    QLabel *label_91;
    QSpacerItem *horizontalSpacer_45;
    QFrame *line_11;
    QHBoxLayout *horizontalLayout_57;
    QLabel *label_92;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_11;
    QLabel *label_93;
    QSpacerItem *horizontalSpacer_46;
    QHBoxLayout *horizontalLayout_58;
    QLabel *label_94;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_11;
    QLabel *label_95;
    QSpacerItem *horizontalSpacer_47;
    QGroupBox *groupBox_14;
    QVBoxLayout *verticalLayout_32;
    QVBoxLayout *verticalLayout_35;
    QHBoxLayout *horizontalLayout_59;
    QLabel *label_96;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_12;
    QLabel *label_97;
    QSpacerItem *horizontalSpacer_48;
    QHBoxLayout *horizontalLayout_60;
    QLabel *label_98;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_12;
    QLabel *label_99;
    QSpacerItem *horizontalSpacer_49;
    QFrame *line_12;
    QHBoxLayout *horizontalLayout_61;
    QLabel *label_100;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_12;
    QLabel *label_101;
    QSpacerItem *horizontalSpacer_50;
    QHBoxLayout *horizontalLayout_62;
    QLabel *label_102;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_12;
    QLabel *label_103;
    QSpacerItem *horizontalSpacer_51;
    QWidget *tab_9;
    QHBoxLayout *horizontalLayout_72;
    QGroupBox *groupBox_17;
    QVBoxLayout *verticalLayout_38;
    QVBoxLayout *verticalLayout_37;
    QHBoxLayout *horizontalLayout_64;
    QLabel *label_104;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_13;
    QLabel *label_105;
    QSpacerItem *horizontalSpacer_52;
    QHBoxLayout *horizontalLayout_65;
    QLabel *label_106;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_13;
    QLabel *label_107;
    QSpacerItem *horizontalSpacer_53;
    QFrame *line_13;
    QHBoxLayout *horizontalLayout_66;
    QLabel *label_108;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_13;
    QLabel *label_109;
    QSpacerItem *horizontalSpacer_54;
    QHBoxLayout *horizontalLayout_67;
    QLabel *label_110;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_13;
    QLabel *label_111;
    QSpacerItem *horizontalSpacer_55;
    QGroupBox *groupBox_16;
    QVBoxLayout *verticalLayout_36;
    QVBoxLayout *verticalLayout_39;
    QHBoxLayout *horizontalLayout_68;
    QLabel *label_112;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_14;
    QLabel *label_113;
    QSpacerItem *horizontalSpacer_56;
    QHBoxLayout *horizontalLayout_69;
    QLabel *label_114;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_14;
    QLabel *label_115;
    QSpacerItem *horizontalSpacer_57;
    QFrame *line_14;
    QHBoxLayout *horizontalLayout_70;
    QLabel *label_116;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_14;
    QLabel *label_117;
    QSpacerItem *horizontalSpacer_58;
    QHBoxLayout *horizontalLayout_71;
    QLabel *label_118;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_14;
    QLabel *label_119;
    QSpacerItem *horizontalSpacer_59;
    QWidget *tab_10;
    QHBoxLayout *horizontalLayout_81;
    QGroupBox *groupBox_19;
    QVBoxLayout *verticalLayout_43;
    QVBoxLayout *verticalLayout_42;
    QHBoxLayout *horizontalLayout_73;
    QLabel *label_120;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_15;
    QLabel *label_121;
    QSpacerItem *horizontalSpacer_60;
    QHBoxLayout *horizontalLayout_74;
    QLabel *label_122;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_15;
    QLabel *label_123;
    QSpacerItem *horizontalSpacer_61;
    QFrame *line_15;
    QHBoxLayout *horizontalLayout_75;
    QLabel *label_124;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_15;
    QLabel *label_125;
    QSpacerItem *horizontalSpacer_62;
    QHBoxLayout *horizontalLayout_76;
    QLabel *label_126;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_15;
    QLabel *label_127;
    QSpacerItem *horizontalSpacer_63;
    QGroupBox *groupBox_18;
    QVBoxLayout *verticalLayout_41;
    QVBoxLayout *verticalLayout_44;
    QHBoxLayout *horizontalLayout_77;
    QLabel *label_128;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_16;
    QLabel *label_129;
    QSpacerItem *horizontalSpacer_64;
    QHBoxLayout *horizontalLayout_78;
    QLabel *label_130;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_16;
    QLabel *label_131;
    QSpacerItem *horizontalSpacer_65;
    QFrame *line_16;
    QHBoxLayout *horizontalLayout_79;
    QLabel *label_132;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_16;
    QLabel *label_133;
    QSpacerItem *horizontalSpacer_66;
    QHBoxLayout *horizontalLayout_80;
    QLabel *label_134;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_16;
    QLabel *label_135;
    QSpacerItem *horizontalSpacer_67;
    QWidget *tab_11;
    QHBoxLayout *horizontalLayout_90;
    QGroupBox *groupBox_21;
    QVBoxLayout *verticalLayout_47;
    QVBoxLayout *verticalLayout_46;
    QHBoxLayout *horizontalLayout_82;
    QLabel *label_136;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_17;
    QLabel *label_137;
    QSpacerItem *horizontalSpacer_68;
    QHBoxLayout *horizontalLayout_83;
    QLabel *label_138;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_17;
    QLabel *label_139;
    QSpacerItem *horizontalSpacer_69;
    QFrame *line_17;
    QHBoxLayout *horizontalLayout_84;
    QLabel *label_140;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_17;
    QLabel *label_141;
    QSpacerItem *horizontalSpacer_70;
    QHBoxLayout *horizontalLayout_85;
    QLabel *label_142;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_17;
    QLabel *label_143;
    QSpacerItem *horizontalSpacer_71;
    QGroupBox *groupBox_20;
    QVBoxLayout *verticalLayout_45;
    QVBoxLayout *verticalLayout_48;
    QHBoxLayout *horizontalLayout_86;
    QLabel *label_144;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_18;
    QLabel *label_145;
    QSpacerItem *horizontalSpacer_72;
    QHBoxLayout *horizontalLayout_87;
    QLabel *label_146;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_18;
    QLabel *label_147;
    QSpacerItem *horizontalSpacer_73;
    QFrame *line_18;
    QHBoxLayout *horizontalLayout_88;
    QLabel *label_148;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_18;
    QLabel *label_149;
    QSpacerItem *horizontalSpacer_74;
    QHBoxLayout *horizontalLayout_89;
    QLabel *label_150;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_18;
    QLabel *label_151;
    QSpacerItem *horizontalSpacer_75;
    QWidget *tab_12;
    QHBoxLayout *horizontalLayout_99;
    QGroupBox *groupBox_23;
    QVBoxLayout *verticalLayout_51;
    QVBoxLayout *verticalLayout_50;
    QHBoxLayout *horizontalLayout_91;
    QLabel *label_152;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_19;
    QLabel *label_153;
    QSpacerItem *horizontalSpacer_76;
    QHBoxLayout *horizontalLayout_92;
    QLabel *label_154;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_19;
    QLabel *label_155;
    QSpacerItem *horizontalSpacer_77;
    QFrame *line_19;
    QHBoxLayout *horizontalLayout_93;
    QLabel *label_156;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_19;
    QLabel *label_157;
    QSpacerItem *horizontalSpacer_78;
    QHBoxLayout *horizontalLayout_94;
    QLabel *label_158;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_19;
    QLabel *label_159;
    QSpacerItem *horizontalSpacer_79;
    QGroupBox *groupBox_22;
    QVBoxLayout *verticalLayout_49;
    QVBoxLayout *verticalLayout_52;
    QHBoxLayout *horizontalLayout_95;
    QLabel *label_160;
    QDoubleSpinBox *doubleSpinBox_SetOnTime_20;
    QLabel *label_161;
    QSpacerItem *horizontalSpacer_80;
    QHBoxLayout *horizontalLayout_96;
    QLabel *label_162;
    QDoubleSpinBox *doubleSpinBox_SetOnTimeError_20;
    QLabel *label_163;
    QSpacerItem *horizontalSpacer_81;
    QFrame *line_20;
    QHBoxLayout *horizontalLayout_97;
    QLabel *label_164;
    QDoubleSpinBox *doubleSpinBox_SetOffTime_20;
    QLabel *label_165;
    QSpacerItem *horizontalSpacer_82;
    QHBoxLayout *horizontalLayout_98;
    QLabel *label_166;
    QDoubleSpinBox *doubleSpinBox_SetOffTimeError_20;
    QLabel *label_167;
    QSpacerItem *horizontalSpacer_83;
    QVBoxLayout *verticalLayout_15;
    QHBoxLayout *horizontalLayout_2;
    QHBoxLayout *horizontalLayout_6;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_9;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QDoubleSpinBox *doubleSpinBox_SetAllTime;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLabel *label_4;
    QDoubleSpinBox *doubleSpinBox_SetAllTimeError;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_16;
    QVBoxLayout *verticalLayout_8;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_7;
    QRadioButton *radioButton_220V;
    QRadioButton *radioButton_30V;
    QPushButton *pushButton_sendConfig;
    QHBoxLayout *horizontalLayout_17;
    QFrame *line_24;
    QVBoxLayout *verticalLayout_11;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_14;
    QLineEdit *lineEdit_fileconfig;
    QPushButton *pushButton_openfile;
    QPushButton *pushButton_save;
    QFrame *line_23;
    QVBoxLayout *verticalLayout_10;
    QGroupBox *groupBox_25;
    QVBoxLayout *verticalLayout_59;
    QLineEdit *lineEdit_filebin;
    QPushButton *pushButton_openbin;
    QPushButton *pushButton_updatebin;
    QTextEdit *textEdit;
    QHBoxLayout *horizontalLayout;
    QComboBox *comboBox_com;
    QPushButton *pushButton_openCOM;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::WindowModal);
        MainWindow->resize(1101, 684);
        MainWindow->setMinimumSize(QSize(800, 480));
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionOptions = new QAction(MainWindow);
        actionOptions->setObjectName(QStringLiteral("actionOptions"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setStyleSheet(QString::fromUtf8("QTabBar::tab{height:40;width:80;font: 20pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";}\n"
""));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_58 = new QVBoxLayout(tab);
        verticalLayout_58->setSpacing(6);
        verticalLayout_58->setContentsMargins(11, 11, 11, 11);
        verticalLayout_58->setObjectName(QStringLiteral("verticalLayout_58"));
        verticalLayout_57 = new QVBoxLayout();
        verticalLayout_57->setSpacing(6);
        verticalLayout_57->setObjectName(QStringLiteral("verticalLayout_57"));
        horizontalLayout_134 = new QHBoxLayout();
        horizontalLayout_134->setSpacing(6);
        horizontalLayout_134->setObjectName(QStringLiteral("horizontalLayout_134"));
        verticalLayout_56 = new QVBoxLayout();
        verticalLayout_56->setSpacing(6);
        verticalLayout_56->setObjectName(QStringLiteral("verticalLayout_56"));
        horizontalLayout_106 = new QHBoxLayout();
        horizontalLayout_106->setSpacing(6);
        horizontalLayout_106->setObjectName(QStringLiteral("horizontalLayout_106"));
        verticalLayout_54 = new QVBoxLayout();
        verticalLayout_54->setSpacing(6);
        verticalLayout_54->setObjectName(QStringLiteral("verticalLayout_54"));
        horizontalLayout_102 = new QHBoxLayout();
        horizontalLayout_102->setSpacing(6);
        horizontalLayout_102->setObjectName(QStringLiteral("horizontalLayout_102"));
        label_7 = new QLabel(tab);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMinimumSize(QSize(0, 30));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font.setPointSize(14);
        label_7->setFont(font);

        horizontalLayout_102->addWidget(label_7);

        lcdNumber_onlineNum = new QLCDNumber(tab);
        lcdNumber_onlineNum->setObjectName(QStringLiteral("lcdNumber_onlineNum"));
        lcdNumber_onlineNum->setMinimumSize(QSize(0, 30));
        lcdNumber_onlineNum->setFont(font);
        lcdNumber_onlineNum->setProperty("intValue", QVariant(0));

        horizontalLayout_102->addWidget(lcdNumber_onlineNum);

        horizontalSpacer_84 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_102->addItem(horizontalSpacer_84);


        verticalLayout_54->addLayout(horizontalLayout_102);

        horizontalLayout_103 = new QHBoxLayout();
        horizontalLayout_103->setSpacing(6);
        horizontalLayout_103->setObjectName(QStringLiteral("horizontalLayout_103"));
        label_169 = new QLabel(tab);
        label_169->setObjectName(QStringLiteral("label_169"));
        label_169->setMinimumSize(QSize(0, 30));
        label_169->setFont(font);

        horizontalLayout_103->addWidget(label_169);

        lcdNumber_testNum = new QLCDNumber(tab);
        lcdNumber_testNum->setObjectName(QStringLiteral("lcdNumber_testNum"));
        lcdNumber_testNum->setMinimumSize(QSize(0, 30));
        lcdNumber_testNum->setFont(font);
        lcdNumber_testNum->setProperty("intValue", QVariant(0));

        horizontalLayout_103->addWidget(lcdNumber_testNum);

        horizontalSpacer_85 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_103->addItem(horizontalSpacer_85);


        verticalLayout_54->addLayout(horizontalLayout_103);

        horizontalLayout_108 = new QHBoxLayout();
        horizontalLayout_108->setSpacing(6);
        horizontalLayout_108->setObjectName(QStringLiteral("horizontalLayout_108"));
        label_172 = new QLabel(tab);
        label_172->setObjectName(QStringLiteral("label_172"));
        label_172->setMinimumSize(QSize(0, 30));
        label_172->setFont(font);

        horizontalLayout_108->addWidget(label_172);

        lcdNumber_errorNum = new QLCDNumber(tab);
        lcdNumber_errorNum->setObjectName(QStringLiteral("lcdNumber_errorNum"));
        lcdNumber_errorNum->setMinimumSize(QSize(0, 30));
        lcdNumber_errorNum->setFont(font);

        horizontalLayout_108->addWidget(lcdNumber_errorNum);

        pushButton_clearErrorNum = new QPushButton(tab);
        pushButton_clearErrorNum->setObjectName(QStringLiteral("pushButton_clearErrorNum"));
        pushButton_clearErrorNum->setMinimumSize(QSize(0, 30));

        horizontalLayout_108->addWidget(pushButton_clearErrorNum);


        verticalLayout_54->addLayout(horizontalLayout_108);

        horizontalLayout_104 = new QHBoxLayout();
        horizontalLayout_104->setSpacing(6);
        horizontalLayout_104->setObjectName(QStringLiteral("horizontalLayout_104"));
        label_168 = new QLabel(tab);
        label_168->setObjectName(QStringLiteral("label_168"));
        label_168->setMinimumSize(QSize(0, 30));
        label_168->setFont(font);

        horizontalLayout_104->addWidget(label_168);

        lcdNumber_okNum = new QLCDNumber(tab);
        lcdNumber_okNum->setObjectName(QStringLiteral("lcdNumber_okNum"));
        lcdNumber_okNum->setMinimumSize(QSize(0, 30));
        lcdNumber_okNum->setFont(font);

        horizontalLayout_104->addWidget(lcdNumber_okNum);

        pushButton_clearOkNum = new QPushButton(tab);
        pushButton_clearOkNum->setObjectName(QStringLiteral("pushButton_clearOkNum"));
        pushButton_clearOkNum->setMinimumSize(QSize(0, 30));

        horizontalLayout_104->addWidget(pushButton_clearOkNum);


        verticalLayout_54->addLayout(horizontalLayout_104);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_54->addItem(verticalSpacer);

        label_nowStep_value = new QLabel(tab);
        label_nowStep_value->setObjectName(QStringLiteral("label_nowStep_value"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font1.setPointSize(24);
        label_nowStep_value->setFont(font1);

        verticalLayout_54->addWidget(label_nowStep_value);

        label_170 = new QLabel(tab);
        label_170->setObjectName(QStringLiteral("label_170"));
        label_170->setFont(font);
        label_170->setStyleSheet(QStringLiteral("color:rgb(255, 85, 0)"));

        verticalLayout_54->addWidget(label_170);


        horizontalLayout_106->addLayout(verticalLayout_54);


        verticalLayout_56->addLayout(horizontalLayout_106);

        pushButton_stop = new QPushButton(tab);
        pushButton_stop->setObjectName(QStringLiteral("pushButton_stop"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton_stop->sizePolicy().hasHeightForWidth());
        pushButton_stop->setSizePolicy(sizePolicy);
        pushButton_stop->setMinimumSize(QSize(0, 75));
        pushButton_stop->setFont(font1);

        verticalLayout_56->addWidget(pushButton_stop);


        horizontalLayout_134->addLayout(verticalLayout_56);

        groupBox_24 = new QGroupBox(tab);
        groupBox_24->setObjectName(QStringLiteral("groupBox_24"));
        groupBox_24->setMinimumSize(QSize(400, 0));
        groupBox_24->setMaximumSize(QSize(800, 16777215));
        verticalLayout_61 = new QVBoxLayout(groupBox_24);
        verticalLayout_61->setSpacing(6);
        verticalLayout_61->setContentsMargins(11, 11, 11, 11);
        verticalLayout_61->setObjectName(QStringLiteral("verticalLayout_61"));
        scrollArea = new QScrollArea(groupBox_24);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setStyleSheet(QStringLiteral(""));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 370, 1748));
        verticalLayout_65 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_65->setSpacing(6);
        verticalLayout_65->setContentsMargins(11, 11, 11, 11);
        verticalLayout_65->setObjectName(QStringLiteral("verticalLayout_65"));
        verticalLayout_60 = new QVBoxLayout();
        verticalLayout_60->setSpacing(6);
        verticalLayout_60->setObjectName(QStringLiteral("verticalLayout_60"));
        horizontalLayout_109 = new QHBoxLayout();
        horizontalLayout_109->setSpacing(6);
        horizontalLayout_109->setObjectName(QStringLiteral("horizontalLayout_109"));
        pushButton_dev_1 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_1->setObjectName(QStringLiteral("pushButton_dev_1"));
        pushButton_dev_1->setMinimumSize(QSize(64, 64));
        pushButton_dev_1->setMaximumSize(QSize(64, 64));
        pushButton_dev_1->setStyleSheet(QStringLiteral(""));

        horizontalLayout_109->addWidget(pushButton_dev_1);

        pushButton_dev_2 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_2->setObjectName(QStringLiteral("pushButton_dev_2"));
        pushButton_dev_2->setMinimumSize(QSize(64, 64));
        pushButton_dev_2->setMaximumSize(QSize(64, 64));

        horizontalLayout_109->addWidget(pushButton_dev_2);

        pushButton_dev_3 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_3->setObjectName(QStringLiteral("pushButton_dev_3"));
        pushButton_dev_3->setMinimumSize(QSize(64, 64));
        pushButton_dev_3->setMaximumSize(QSize(64, 64));

        horizontalLayout_109->addWidget(pushButton_dev_3);

        pushButton_dev_4 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_4->setObjectName(QStringLiteral("pushButton_dev_4"));
        pushButton_dev_4->setMinimumSize(QSize(64, 64));
        pushButton_dev_4->setMaximumSize(QSize(64, 64));

        horizontalLayout_109->addWidget(pushButton_dev_4);

        pushButton_dev_5 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_5->setObjectName(QStringLiteral("pushButton_dev_5"));
        pushButton_dev_5->setMinimumSize(QSize(64, 64));
        pushButton_dev_5->setMaximumSize(QSize(64, 64));

        horizontalLayout_109->addWidget(pushButton_dev_5);


        verticalLayout_60->addLayout(horizontalLayout_109);

        horizontalLayout_110 = new QHBoxLayout();
        horizontalLayout_110->setSpacing(6);
        horizontalLayout_110->setObjectName(QStringLiteral("horizontalLayout_110"));
        pushButton_dev_6 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_6->setObjectName(QStringLiteral("pushButton_dev_6"));
        pushButton_dev_6->setMinimumSize(QSize(64, 64));
        pushButton_dev_6->setMaximumSize(QSize(64, 64));
        pushButton_dev_6->setStyleSheet(QStringLiteral(""));

        horizontalLayout_110->addWidget(pushButton_dev_6);

        pushButton_dev_7 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_7->setObjectName(QStringLiteral("pushButton_dev_7"));
        pushButton_dev_7->setMinimumSize(QSize(64, 64));
        pushButton_dev_7->setMaximumSize(QSize(64, 64));

        horizontalLayout_110->addWidget(pushButton_dev_7);

        pushButton_dev_8 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_8->setObjectName(QStringLiteral("pushButton_dev_8"));
        pushButton_dev_8->setMinimumSize(QSize(64, 64));
        pushButton_dev_8->setMaximumSize(QSize(64, 64));

        horizontalLayout_110->addWidget(pushButton_dev_8);

        pushButton_dev_9 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_9->setObjectName(QStringLiteral("pushButton_dev_9"));
        pushButton_dev_9->setMinimumSize(QSize(64, 64));
        pushButton_dev_9->setMaximumSize(QSize(64, 64));

        horizontalLayout_110->addWidget(pushButton_dev_9);

        pushButton_dev_10 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_10->setObjectName(QStringLiteral("pushButton_dev_10"));
        pushButton_dev_10->setMinimumSize(QSize(64, 64));
        pushButton_dev_10->setMaximumSize(QSize(64, 64));

        horizontalLayout_110->addWidget(pushButton_dev_10);


        verticalLayout_60->addLayout(horizontalLayout_110);

        horizontalLayout_111 = new QHBoxLayout();
        horizontalLayout_111->setSpacing(6);
        horizontalLayout_111->setObjectName(QStringLiteral("horizontalLayout_111"));
        pushButton_dev_11 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_11->setObjectName(QStringLiteral("pushButton_dev_11"));
        pushButton_dev_11->setMinimumSize(QSize(64, 64));
        pushButton_dev_11->setMaximumSize(QSize(64, 64));

        horizontalLayout_111->addWidget(pushButton_dev_11);

        pushButton_dev_12 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_12->setObjectName(QStringLiteral("pushButton_dev_12"));
        pushButton_dev_12->setMinimumSize(QSize(64, 64));
        pushButton_dev_12->setMaximumSize(QSize(64, 64));

        horizontalLayout_111->addWidget(pushButton_dev_12);

        pushButton_dev_13 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_13->setObjectName(QStringLiteral("pushButton_dev_13"));
        pushButton_dev_13->setMinimumSize(QSize(64, 64));
        pushButton_dev_13->setMaximumSize(QSize(64, 64));

        horizontalLayout_111->addWidget(pushButton_dev_13);

        pushButton_dev_14 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_14->setObjectName(QStringLiteral("pushButton_dev_14"));
        pushButton_dev_14->setMinimumSize(QSize(64, 64));
        pushButton_dev_14->setMaximumSize(QSize(64, 64));

        horizontalLayout_111->addWidget(pushButton_dev_14);

        pushButton_dev_15 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_15->setObjectName(QStringLiteral("pushButton_dev_15"));
        pushButton_dev_15->setMinimumSize(QSize(64, 64));
        pushButton_dev_15->setMaximumSize(QSize(64, 64));

        horizontalLayout_111->addWidget(pushButton_dev_15);


        verticalLayout_60->addLayout(horizontalLayout_111);

        horizontalLayout_112 = new QHBoxLayout();
        horizontalLayout_112->setSpacing(6);
        horizontalLayout_112->setObjectName(QStringLiteral("horizontalLayout_112"));
        pushButton_dev_16 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_16->setObjectName(QStringLiteral("pushButton_dev_16"));
        pushButton_dev_16->setMinimumSize(QSize(64, 64));
        pushButton_dev_16->setMaximumSize(QSize(64, 64));

        horizontalLayout_112->addWidget(pushButton_dev_16);

        pushButton_dev_17 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_17->setObjectName(QStringLiteral("pushButton_dev_17"));
        pushButton_dev_17->setMinimumSize(QSize(64, 64));
        pushButton_dev_17->setMaximumSize(QSize(64, 64));

        horizontalLayout_112->addWidget(pushButton_dev_17);

        pushButton_dev_18 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_18->setObjectName(QStringLiteral("pushButton_dev_18"));
        pushButton_dev_18->setMinimumSize(QSize(64, 64));
        pushButton_dev_18->setMaximumSize(QSize(64, 64));

        horizontalLayout_112->addWidget(pushButton_dev_18);

        pushButton_dev_19 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_19->setObjectName(QStringLiteral("pushButton_dev_19"));
        pushButton_dev_19->setMinimumSize(QSize(64, 64));
        pushButton_dev_19->setMaximumSize(QSize(64, 64));

        horizontalLayout_112->addWidget(pushButton_dev_19);

        pushButton_dev_20 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_20->setObjectName(QStringLiteral("pushButton_dev_20"));
        pushButton_dev_20->setMinimumSize(QSize(64, 64));
        pushButton_dev_20->setMaximumSize(QSize(64, 64));

        horizontalLayout_112->addWidget(pushButton_dev_20);


        verticalLayout_60->addLayout(horizontalLayout_112);

        horizontalLayout_113 = new QHBoxLayout();
        horizontalLayout_113->setSpacing(6);
        horizontalLayout_113->setObjectName(QStringLiteral("horizontalLayout_113"));
        pushButton_dev_21 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_21->setObjectName(QStringLiteral("pushButton_dev_21"));
        pushButton_dev_21->setMinimumSize(QSize(64, 64));
        pushButton_dev_21->setMaximumSize(QSize(64, 64));

        horizontalLayout_113->addWidget(pushButton_dev_21);

        pushButton_dev_22 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_22->setObjectName(QStringLiteral("pushButton_dev_22"));
        pushButton_dev_22->setMinimumSize(QSize(64, 64));
        pushButton_dev_22->setMaximumSize(QSize(64, 64));

        horizontalLayout_113->addWidget(pushButton_dev_22);

        pushButton_dev_23 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_23->setObjectName(QStringLiteral("pushButton_dev_23"));
        pushButton_dev_23->setMinimumSize(QSize(64, 64));
        pushButton_dev_23->setMaximumSize(QSize(64, 64));

        horizontalLayout_113->addWidget(pushButton_dev_23);

        pushButton_dev_24 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_24->setObjectName(QStringLiteral("pushButton_dev_24"));
        pushButton_dev_24->setMinimumSize(QSize(64, 64));
        pushButton_dev_24->setMaximumSize(QSize(64, 64));

        horizontalLayout_113->addWidget(pushButton_dev_24);

        pushButton_dev_25 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_25->setObjectName(QStringLiteral("pushButton_dev_25"));
        pushButton_dev_25->setMinimumSize(QSize(64, 64));
        pushButton_dev_25->setMaximumSize(QSize(64, 64));

        horizontalLayout_113->addWidget(pushButton_dev_25);


        verticalLayout_60->addLayout(horizontalLayout_113);

        horizontalLayout_114 = new QHBoxLayout();
        horizontalLayout_114->setSpacing(6);
        horizontalLayout_114->setObjectName(QStringLiteral("horizontalLayout_114"));
        pushButton_dev_26 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_26->setObjectName(QStringLiteral("pushButton_dev_26"));
        pushButton_dev_26->setMinimumSize(QSize(64, 64));
        pushButton_dev_26->setMaximumSize(QSize(64, 64));

        horizontalLayout_114->addWidget(pushButton_dev_26);

        pushButton_dev_27 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_27->setObjectName(QStringLiteral("pushButton_dev_27"));
        pushButton_dev_27->setMinimumSize(QSize(64, 64));
        pushButton_dev_27->setMaximumSize(QSize(64, 64));

        horizontalLayout_114->addWidget(pushButton_dev_27);

        pushButton_dev_28 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_28->setObjectName(QStringLiteral("pushButton_dev_28"));
        pushButton_dev_28->setMinimumSize(QSize(64, 64));
        pushButton_dev_28->setMaximumSize(QSize(64, 64));

        horizontalLayout_114->addWidget(pushButton_dev_28);

        pushButton_dev_29 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_29->setObjectName(QStringLiteral("pushButton_dev_29"));
        pushButton_dev_29->setMinimumSize(QSize(64, 64));
        pushButton_dev_29->setMaximumSize(QSize(64, 64));

        horizontalLayout_114->addWidget(pushButton_dev_29);

        pushButton_dev_30 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_30->setObjectName(QStringLiteral("pushButton_dev_30"));
        pushButton_dev_30->setMinimumSize(QSize(64, 64));
        pushButton_dev_30->setMaximumSize(QSize(64, 64));

        horizontalLayout_114->addWidget(pushButton_dev_30);


        verticalLayout_60->addLayout(horizontalLayout_114);


        verticalLayout_65->addLayout(verticalLayout_60);

        verticalLayout_62 = new QVBoxLayout();
        verticalLayout_62->setSpacing(6);
        verticalLayout_62->setObjectName(QStringLiteral("verticalLayout_62"));
        horizontalLayout_115 = new QHBoxLayout();
        horizontalLayout_115->setSpacing(6);
        horizontalLayout_115->setObjectName(QStringLiteral("horizontalLayout_115"));
        pushButton_dev_31 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_31->setObjectName(QStringLiteral("pushButton_dev_31"));
        pushButton_dev_31->setMinimumSize(QSize(64, 64));
        pushButton_dev_31->setMaximumSize(QSize(64, 64));

        horizontalLayout_115->addWidget(pushButton_dev_31);

        pushButton_dev_32 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_32->setObjectName(QStringLiteral("pushButton_dev_32"));
        pushButton_dev_32->setMinimumSize(QSize(64, 64));
        pushButton_dev_32->setMaximumSize(QSize(64, 64));

        horizontalLayout_115->addWidget(pushButton_dev_32);

        pushButton_dev_33 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_33->setObjectName(QStringLiteral("pushButton_dev_33"));
        pushButton_dev_33->setMinimumSize(QSize(64, 64));
        pushButton_dev_33->setMaximumSize(QSize(64, 64));

        horizontalLayout_115->addWidget(pushButton_dev_33);

        pushButton_dev_34 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_34->setObjectName(QStringLiteral("pushButton_dev_34"));
        pushButton_dev_34->setMinimumSize(QSize(64, 64));
        pushButton_dev_34->setMaximumSize(QSize(64, 64));

        horizontalLayout_115->addWidget(pushButton_dev_34);

        pushButton_dev_35 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_35->setObjectName(QStringLiteral("pushButton_dev_35"));
        pushButton_dev_35->setMinimumSize(QSize(64, 64));
        pushButton_dev_35->setMaximumSize(QSize(64, 64));

        horizontalLayout_115->addWidget(pushButton_dev_35);


        verticalLayout_62->addLayout(horizontalLayout_115);

        horizontalLayout_116 = new QHBoxLayout();
        horizontalLayout_116->setSpacing(6);
        horizontalLayout_116->setObjectName(QStringLiteral("horizontalLayout_116"));
        pushButton_dev_36 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_36->setObjectName(QStringLiteral("pushButton_dev_36"));
        pushButton_dev_36->setMinimumSize(QSize(64, 64));
        pushButton_dev_36->setMaximumSize(QSize(64, 64));

        horizontalLayout_116->addWidget(pushButton_dev_36);

        pushButton_dev_37 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_37->setObjectName(QStringLiteral("pushButton_dev_37"));
        pushButton_dev_37->setMinimumSize(QSize(64, 64));
        pushButton_dev_37->setMaximumSize(QSize(64, 64));

        horizontalLayout_116->addWidget(pushButton_dev_37);

        pushButton_dev_38 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_38->setObjectName(QStringLiteral("pushButton_dev_38"));
        pushButton_dev_38->setMinimumSize(QSize(64, 64));
        pushButton_dev_38->setMaximumSize(QSize(64, 64));

        horizontalLayout_116->addWidget(pushButton_dev_38);

        pushButton_dev_39 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_39->setObjectName(QStringLiteral("pushButton_dev_39"));
        pushButton_dev_39->setMinimumSize(QSize(64, 64));
        pushButton_dev_39->setMaximumSize(QSize(64, 64));

        horizontalLayout_116->addWidget(pushButton_dev_39);

        pushButton_dev_40 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_40->setObjectName(QStringLiteral("pushButton_dev_40"));
        pushButton_dev_40->setMinimumSize(QSize(64, 64));
        pushButton_dev_40->setMaximumSize(QSize(64, 64));

        horizontalLayout_116->addWidget(pushButton_dev_40);


        verticalLayout_62->addLayout(horizontalLayout_116);

        horizontalLayout_117 = new QHBoxLayout();
        horizontalLayout_117->setSpacing(6);
        horizontalLayout_117->setObjectName(QStringLiteral("horizontalLayout_117"));
        pushButton_dev_41 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_41->setObjectName(QStringLiteral("pushButton_dev_41"));
        pushButton_dev_41->setMinimumSize(QSize(64, 64));
        pushButton_dev_41->setMaximumSize(QSize(64, 64));

        horizontalLayout_117->addWidget(pushButton_dev_41);

        pushButton_dev_42 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_42->setObjectName(QStringLiteral("pushButton_dev_42"));
        pushButton_dev_42->setMinimumSize(QSize(64, 64));
        pushButton_dev_42->setMaximumSize(QSize(64, 64));

        horizontalLayout_117->addWidget(pushButton_dev_42);

        pushButton_dev_43 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_43->setObjectName(QStringLiteral("pushButton_dev_43"));
        pushButton_dev_43->setMinimumSize(QSize(64, 64));
        pushButton_dev_43->setMaximumSize(QSize(64, 64));

        horizontalLayout_117->addWidget(pushButton_dev_43);

        pushButton_dev_44 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_44->setObjectName(QStringLiteral("pushButton_dev_44"));
        pushButton_dev_44->setMinimumSize(QSize(64, 64));
        pushButton_dev_44->setMaximumSize(QSize(64, 64));

        horizontalLayout_117->addWidget(pushButton_dev_44);

        pushButton_dev_45 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_45->setObjectName(QStringLiteral("pushButton_dev_45"));
        pushButton_dev_45->setMinimumSize(QSize(64, 64));
        pushButton_dev_45->setMaximumSize(QSize(64, 64));

        horizontalLayout_117->addWidget(pushButton_dev_45);


        verticalLayout_62->addLayout(horizontalLayout_117);

        horizontalLayout_118 = new QHBoxLayout();
        horizontalLayout_118->setSpacing(6);
        horizontalLayout_118->setObjectName(QStringLiteral("horizontalLayout_118"));
        pushButton_dev_46 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_46->setObjectName(QStringLiteral("pushButton_dev_46"));
        pushButton_dev_46->setMinimumSize(QSize(64, 64));
        pushButton_dev_46->setMaximumSize(QSize(64, 64));

        horizontalLayout_118->addWidget(pushButton_dev_46);

        pushButton_dev_47 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_47->setObjectName(QStringLiteral("pushButton_dev_47"));
        pushButton_dev_47->setMinimumSize(QSize(64, 64));
        pushButton_dev_47->setMaximumSize(QSize(64, 64));

        horizontalLayout_118->addWidget(pushButton_dev_47);

        pushButton_dev_48 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_48->setObjectName(QStringLiteral("pushButton_dev_48"));
        pushButton_dev_48->setMinimumSize(QSize(64, 64));
        pushButton_dev_48->setMaximumSize(QSize(64, 64));

        horizontalLayout_118->addWidget(pushButton_dev_48);

        pushButton_dev_49 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_49->setObjectName(QStringLiteral("pushButton_dev_49"));
        pushButton_dev_49->setMinimumSize(QSize(64, 64));
        pushButton_dev_49->setMaximumSize(QSize(64, 64));

        horizontalLayout_118->addWidget(pushButton_dev_49);

        pushButton_dev_50 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_50->setObjectName(QStringLiteral("pushButton_dev_50"));
        pushButton_dev_50->setMinimumSize(QSize(64, 64));
        pushButton_dev_50->setMaximumSize(QSize(64, 64));

        horizontalLayout_118->addWidget(pushButton_dev_50);


        verticalLayout_62->addLayout(horizontalLayout_118);

        horizontalLayout_119 = new QHBoxLayout();
        horizontalLayout_119->setSpacing(6);
        horizontalLayout_119->setObjectName(QStringLiteral("horizontalLayout_119"));
        pushButton_dev_51 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_51->setObjectName(QStringLiteral("pushButton_dev_51"));
        pushButton_dev_51->setMinimumSize(QSize(64, 64));
        pushButton_dev_51->setMaximumSize(QSize(64, 64));

        horizontalLayout_119->addWidget(pushButton_dev_51);

        pushButton_dev_52 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_52->setObjectName(QStringLiteral("pushButton_dev_52"));
        pushButton_dev_52->setMinimumSize(QSize(64, 64));
        pushButton_dev_52->setMaximumSize(QSize(64, 64));

        horizontalLayout_119->addWidget(pushButton_dev_52);

        pushButton_dev_53 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_53->setObjectName(QStringLiteral("pushButton_dev_53"));
        pushButton_dev_53->setMinimumSize(QSize(64, 64));
        pushButton_dev_53->setMaximumSize(QSize(64, 64));

        horizontalLayout_119->addWidget(pushButton_dev_53);

        pushButton_dev_54 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_54->setObjectName(QStringLiteral("pushButton_dev_54"));
        pushButton_dev_54->setMinimumSize(QSize(64, 64));
        pushButton_dev_54->setMaximumSize(QSize(64, 64));

        horizontalLayout_119->addWidget(pushButton_dev_54);

        pushButton_dev_55 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_55->setObjectName(QStringLiteral("pushButton_dev_55"));
        pushButton_dev_55->setMinimumSize(QSize(64, 64));
        pushButton_dev_55->setMaximumSize(QSize(64, 64));

        horizontalLayout_119->addWidget(pushButton_dev_55);


        verticalLayout_62->addLayout(horizontalLayout_119);

        horizontalLayout_120 = new QHBoxLayout();
        horizontalLayout_120->setSpacing(6);
        horizontalLayout_120->setObjectName(QStringLiteral("horizontalLayout_120"));
        pushButton_dev_56 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_56->setObjectName(QStringLiteral("pushButton_dev_56"));
        pushButton_dev_56->setMinimumSize(QSize(64, 64));
        pushButton_dev_56->setMaximumSize(QSize(64, 64));

        horizontalLayout_120->addWidget(pushButton_dev_56);

        pushButton_dev_57 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_57->setObjectName(QStringLiteral("pushButton_dev_57"));
        pushButton_dev_57->setMinimumSize(QSize(64, 64));
        pushButton_dev_57->setMaximumSize(QSize(64, 64));

        horizontalLayout_120->addWidget(pushButton_dev_57);

        pushButton_dev_58 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_58->setObjectName(QStringLiteral("pushButton_dev_58"));
        pushButton_dev_58->setMinimumSize(QSize(64, 64));
        pushButton_dev_58->setMaximumSize(QSize(64, 64));

        horizontalLayout_120->addWidget(pushButton_dev_58);

        pushButton_dev_59 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_59->setObjectName(QStringLiteral("pushButton_dev_59"));
        pushButton_dev_59->setMinimumSize(QSize(64, 64));
        pushButton_dev_59->setMaximumSize(QSize(64, 64));

        horizontalLayout_120->addWidget(pushButton_dev_59);

        pushButton_dev_60 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_60->setObjectName(QStringLiteral("pushButton_dev_60"));
        pushButton_dev_60->setMinimumSize(QSize(64, 64));
        pushButton_dev_60->setMaximumSize(QSize(64, 64));

        horizontalLayout_120->addWidget(pushButton_dev_60);


        verticalLayout_62->addLayout(horizontalLayout_120);

        verticalLayout_63 = new QVBoxLayout();
        verticalLayout_63->setSpacing(6);
        verticalLayout_63->setObjectName(QStringLiteral("verticalLayout_63"));
        horizontalLayout_121 = new QHBoxLayout();
        horizontalLayout_121->setSpacing(6);
        horizontalLayout_121->setObjectName(QStringLiteral("horizontalLayout_121"));
        pushButton_dev_61 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_61->setObjectName(QStringLiteral("pushButton_dev_61"));
        pushButton_dev_61->setMinimumSize(QSize(64, 64));
        pushButton_dev_61->setMaximumSize(QSize(64, 64));
        pushButton_dev_61->setStyleSheet(QStringLiteral(""));

        horizontalLayout_121->addWidget(pushButton_dev_61);

        pushButton_dev_62 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_62->setObjectName(QStringLiteral("pushButton_dev_62"));
        pushButton_dev_62->setMinimumSize(QSize(64, 64));
        pushButton_dev_62->setMaximumSize(QSize(64, 64));

        horizontalLayout_121->addWidget(pushButton_dev_62);

        pushButton_dev_63 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_63->setObjectName(QStringLiteral("pushButton_dev_63"));
        pushButton_dev_63->setMinimumSize(QSize(64, 64));
        pushButton_dev_63->setMaximumSize(QSize(64, 64));

        horizontalLayout_121->addWidget(pushButton_dev_63);

        pushButton_dev_64 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_64->setObjectName(QStringLiteral("pushButton_dev_64"));
        pushButton_dev_64->setMinimumSize(QSize(64, 64));
        pushButton_dev_64->setMaximumSize(QSize(64, 64));

        horizontalLayout_121->addWidget(pushButton_dev_64);

        pushButton_dev_65 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_65->setObjectName(QStringLiteral("pushButton_dev_65"));
        pushButton_dev_65->setMinimumSize(QSize(64, 64));
        pushButton_dev_65->setMaximumSize(QSize(64, 64));

        horizontalLayout_121->addWidget(pushButton_dev_65);


        verticalLayout_63->addLayout(horizontalLayout_121);

        horizontalLayout_122 = new QHBoxLayout();
        horizontalLayout_122->setSpacing(6);
        horizontalLayout_122->setObjectName(QStringLiteral("horizontalLayout_122"));
        pushButton_dev_66 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_66->setObjectName(QStringLiteral("pushButton_dev_66"));
        pushButton_dev_66->setMinimumSize(QSize(64, 64));
        pushButton_dev_66->setMaximumSize(QSize(64, 64));
        pushButton_dev_66->setStyleSheet(QStringLiteral(""));

        horizontalLayout_122->addWidget(pushButton_dev_66);

        pushButton_dev_67 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_67->setObjectName(QStringLiteral("pushButton_dev_67"));
        pushButton_dev_67->setMinimumSize(QSize(64, 64));
        pushButton_dev_67->setMaximumSize(QSize(64, 64));

        horizontalLayout_122->addWidget(pushButton_dev_67);

        pushButton_dev_68 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_68->setObjectName(QStringLiteral("pushButton_dev_68"));
        pushButton_dev_68->setMinimumSize(QSize(64, 64));
        pushButton_dev_68->setMaximumSize(QSize(64, 64));

        horizontalLayout_122->addWidget(pushButton_dev_68);

        pushButton_dev_69 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_69->setObjectName(QStringLiteral("pushButton_dev_69"));
        pushButton_dev_69->setMinimumSize(QSize(64, 64));
        pushButton_dev_69->setMaximumSize(QSize(64, 64));

        horizontalLayout_122->addWidget(pushButton_dev_69);

        pushButton_dev_70 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_70->setObjectName(QStringLiteral("pushButton_dev_70"));
        pushButton_dev_70->setMinimumSize(QSize(64, 64));
        pushButton_dev_70->setMaximumSize(QSize(64, 64));

        horizontalLayout_122->addWidget(pushButton_dev_70);


        verticalLayout_63->addLayout(horizontalLayout_122);

        horizontalLayout_123 = new QHBoxLayout();
        horizontalLayout_123->setSpacing(6);
        horizontalLayout_123->setObjectName(QStringLiteral("horizontalLayout_123"));
        pushButton_dev_71 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_71->setObjectName(QStringLiteral("pushButton_dev_71"));
        pushButton_dev_71->setMinimumSize(QSize(64, 64));
        pushButton_dev_71->setMaximumSize(QSize(64, 64));

        horizontalLayout_123->addWidget(pushButton_dev_71);

        pushButton_dev_72 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_72->setObjectName(QStringLiteral("pushButton_dev_72"));
        pushButton_dev_72->setMinimumSize(QSize(64, 64));
        pushButton_dev_72->setMaximumSize(QSize(64, 64));

        horizontalLayout_123->addWidget(pushButton_dev_72);

        pushButton_dev_73 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_73->setObjectName(QStringLiteral("pushButton_dev_73"));
        pushButton_dev_73->setMinimumSize(QSize(64, 64));
        pushButton_dev_73->setMaximumSize(QSize(64, 64));

        horizontalLayout_123->addWidget(pushButton_dev_73);

        pushButton_dev_74 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_74->setObjectName(QStringLiteral("pushButton_dev_74"));
        pushButton_dev_74->setMinimumSize(QSize(64, 64));
        pushButton_dev_74->setMaximumSize(QSize(64, 64));

        horizontalLayout_123->addWidget(pushButton_dev_74);

        pushButton_dev_75 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_75->setObjectName(QStringLiteral("pushButton_dev_75"));
        pushButton_dev_75->setMinimumSize(QSize(64, 64));
        pushButton_dev_75->setMaximumSize(QSize(64, 64));

        horizontalLayout_123->addWidget(pushButton_dev_75);


        verticalLayout_63->addLayout(horizontalLayout_123);

        horizontalLayout_124 = new QHBoxLayout();
        horizontalLayout_124->setSpacing(6);
        horizontalLayout_124->setObjectName(QStringLiteral("horizontalLayout_124"));
        pushButton_dev_76 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_76->setObjectName(QStringLiteral("pushButton_dev_76"));
        pushButton_dev_76->setMinimumSize(QSize(64, 64));
        pushButton_dev_76->setMaximumSize(QSize(64, 64));

        horizontalLayout_124->addWidget(pushButton_dev_76);

        pushButton_dev_77 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_77->setObjectName(QStringLiteral("pushButton_dev_77"));
        pushButton_dev_77->setMinimumSize(QSize(64, 64));
        pushButton_dev_77->setMaximumSize(QSize(64, 64));

        horizontalLayout_124->addWidget(pushButton_dev_77);

        pushButton_dev_78 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_78->setObjectName(QStringLiteral("pushButton_dev_78"));
        pushButton_dev_78->setMinimumSize(QSize(64, 64));
        pushButton_dev_78->setMaximumSize(QSize(64, 64));

        horizontalLayout_124->addWidget(pushButton_dev_78);

        pushButton_dev_79 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_79->setObjectName(QStringLiteral("pushButton_dev_79"));
        pushButton_dev_79->setMinimumSize(QSize(64, 64));
        pushButton_dev_79->setMaximumSize(QSize(64, 64));

        horizontalLayout_124->addWidget(pushButton_dev_79);

        pushButton_dev_80 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_80->setObjectName(QStringLiteral("pushButton_dev_80"));
        pushButton_dev_80->setMinimumSize(QSize(64, 64));
        pushButton_dev_80->setMaximumSize(QSize(64, 64));

        horizontalLayout_124->addWidget(pushButton_dev_80);


        verticalLayout_63->addLayout(horizontalLayout_124);

        horizontalLayout_125 = new QHBoxLayout();
        horizontalLayout_125->setSpacing(6);
        horizontalLayout_125->setObjectName(QStringLiteral("horizontalLayout_125"));
        pushButton_dev_81 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_81->setObjectName(QStringLiteral("pushButton_dev_81"));
        pushButton_dev_81->setMinimumSize(QSize(64, 64));
        pushButton_dev_81->setMaximumSize(QSize(64, 64));

        horizontalLayout_125->addWidget(pushButton_dev_81);

        pushButton_dev_82 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_82->setObjectName(QStringLiteral("pushButton_dev_82"));
        pushButton_dev_82->setMinimumSize(QSize(64, 64));
        pushButton_dev_82->setMaximumSize(QSize(64, 64));

        horizontalLayout_125->addWidget(pushButton_dev_82);

        pushButton_dev_83 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_83->setObjectName(QStringLiteral("pushButton_dev_83"));
        pushButton_dev_83->setMinimumSize(QSize(64, 64));
        pushButton_dev_83->setMaximumSize(QSize(64, 64));

        horizontalLayout_125->addWidget(pushButton_dev_83);

        pushButton_dev_84 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_84->setObjectName(QStringLiteral("pushButton_dev_84"));
        pushButton_dev_84->setMinimumSize(QSize(64, 64));
        pushButton_dev_84->setMaximumSize(QSize(64, 64));

        horizontalLayout_125->addWidget(pushButton_dev_84);

        pushButton_dev_85 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_85->setObjectName(QStringLiteral("pushButton_dev_85"));
        pushButton_dev_85->setMinimumSize(QSize(64, 64));
        pushButton_dev_85->setMaximumSize(QSize(64, 64));

        horizontalLayout_125->addWidget(pushButton_dev_85);


        verticalLayout_63->addLayout(horizontalLayout_125);

        horizontalLayout_126 = new QHBoxLayout();
        horizontalLayout_126->setSpacing(6);
        horizontalLayout_126->setObjectName(QStringLiteral("horizontalLayout_126"));
        pushButton_dev_86 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_86->setObjectName(QStringLiteral("pushButton_dev_86"));
        pushButton_dev_86->setMinimumSize(QSize(64, 64));
        pushButton_dev_86->setMaximumSize(QSize(64, 64));

        horizontalLayout_126->addWidget(pushButton_dev_86);

        pushButton_dev_87 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_87->setObjectName(QStringLiteral("pushButton_dev_87"));
        pushButton_dev_87->setMinimumSize(QSize(64, 64));
        pushButton_dev_87->setMaximumSize(QSize(64, 64));

        horizontalLayout_126->addWidget(pushButton_dev_87);

        pushButton_dev_88 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_88->setObjectName(QStringLiteral("pushButton_dev_88"));
        pushButton_dev_88->setMinimumSize(QSize(64, 64));
        pushButton_dev_88->setMaximumSize(QSize(64, 64));

        horizontalLayout_126->addWidget(pushButton_dev_88);

        pushButton_dev_89 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_89->setObjectName(QStringLiteral("pushButton_dev_89"));
        pushButton_dev_89->setMinimumSize(QSize(64, 64));
        pushButton_dev_89->setMaximumSize(QSize(64, 64));

        horizontalLayout_126->addWidget(pushButton_dev_89);

        pushButton_dev_90 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_90->setObjectName(QStringLiteral("pushButton_dev_90"));
        pushButton_dev_90->setMinimumSize(QSize(64, 64));
        pushButton_dev_90->setMaximumSize(QSize(64, 64));

        horizontalLayout_126->addWidget(pushButton_dev_90);


        verticalLayout_63->addLayout(horizontalLayout_126);

        verticalLayout_64 = new QVBoxLayout();
        verticalLayout_64->setSpacing(6);
        verticalLayout_64->setObjectName(QStringLiteral("verticalLayout_64"));
        horizontalLayout_127 = new QHBoxLayout();
        horizontalLayout_127->setSpacing(6);
        horizontalLayout_127->setObjectName(QStringLiteral("horizontalLayout_127"));
        pushButton_dev_91 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_91->setObjectName(QStringLiteral("pushButton_dev_91"));
        pushButton_dev_91->setMinimumSize(QSize(64, 64));
        pushButton_dev_91->setMaximumSize(QSize(64, 64));

        horizontalLayout_127->addWidget(pushButton_dev_91);

        pushButton_dev_92 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_92->setObjectName(QStringLiteral("pushButton_dev_92"));
        pushButton_dev_92->setMinimumSize(QSize(64, 64));
        pushButton_dev_92->setMaximumSize(QSize(64, 64));

        horizontalLayout_127->addWidget(pushButton_dev_92);

        pushButton_dev_93 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_93->setObjectName(QStringLiteral("pushButton_dev_93"));
        pushButton_dev_93->setMinimumSize(QSize(64, 64));
        pushButton_dev_93->setMaximumSize(QSize(64, 64));

        horizontalLayout_127->addWidget(pushButton_dev_93);

        pushButton_dev_94 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_94->setObjectName(QStringLiteral("pushButton_dev_94"));
        pushButton_dev_94->setMinimumSize(QSize(64, 64));
        pushButton_dev_94->setMaximumSize(QSize(64, 64));

        horizontalLayout_127->addWidget(pushButton_dev_94);

        pushButton_dev_95 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_95->setObjectName(QStringLiteral("pushButton_dev_95"));
        pushButton_dev_95->setMinimumSize(QSize(64, 64));
        pushButton_dev_95->setMaximumSize(QSize(64, 64));

        horizontalLayout_127->addWidget(pushButton_dev_95);


        verticalLayout_64->addLayout(horizontalLayout_127);

        horizontalLayout_128 = new QHBoxLayout();
        horizontalLayout_128->setSpacing(6);
        horizontalLayout_128->setObjectName(QStringLiteral("horizontalLayout_128"));
        pushButton_dev_96 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_96->setObjectName(QStringLiteral("pushButton_dev_96"));
        pushButton_dev_96->setMinimumSize(QSize(64, 64));
        pushButton_dev_96->setMaximumSize(QSize(64, 64));

        horizontalLayout_128->addWidget(pushButton_dev_96);

        pushButton_dev_97 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_97->setObjectName(QStringLiteral("pushButton_dev_97"));
        pushButton_dev_97->setMinimumSize(QSize(64, 64));
        pushButton_dev_97->setMaximumSize(QSize(64, 64));

        horizontalLayout_128->addWidget(pushButton_dev_97);

        pushButton_dev_98 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_98->setObjectName(QStringLiteral("pushButton_dev_98"));
        pushButton_dev_98->setMinimumSize(QSize(64, 64));
        pushButton_dev_98->setMaximumSize(QSize(64, 64));

        horizontalLayout_128->addWidget(pushButton_dev_98);

        pushButton_dev_99 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_99->setObjectName(QStringLiteral("pushButton_dev_99"));
        pushButton_dev_99->setMinimumSize(QSize(64, 64));
        pushButton_dev_99->setMaximumSize(QSize(64, 64));

        horizontalLayout_128->addWidget(pushButton_dev_99);

        pushButton_dev_100 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_100->setObjectName(QStringLiteral("pushButton_dev_100"));
        pushButton_dev_100->setMinimumSize(QSize(64, 64));
        pushButton_dev_100->setMaximumSize(QSize(64, 64));

        horizontalLayout_128->addWidget(pushButton_dev_100);


        verticalLayout_64->addLayout(horizontalLayout_128);

        horizontalLayout_129 = new QHBoxLayout();
        horizontalLayout_129->setSpacing(6);
        horizontalLayout_129->setObjectName(QStringLiteral("horizontalLayout_129"));
        pushButton_dev_101 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_101->setObjectName(QStringLiteral("pushButton_dev_101"));
        pushButton_dev_101->setMinimumSize(QSize(64, 64));
        pushButton_dev_101->setMaximumSize(QSize(64, 64));

        horizontalLayout_129->addWidget(pushButton_dev_101);

        pushButton_dev_102 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_102->setObjectName(QStringLiteral("pushButton_dev_102"));
        pushButton_dev_102->setMinimumSize(QSize(64, 64));
        pushButton_dev_102->setMaximumSize(QSize(64, 64));

        horizontalLayout_129->addWidget(pushButton_dev_102);

        pushButton_dev_103 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_103->setObjectName(QStringLiteral("pushButton_dev_103"));
        pushButton_dev_103->setMinimumSize(QSize(64, 64));
        pushButton_dev_103->setMaximumSize(QSize(64, 64));

        horizontalLayout_129->addWidget(pushButton_dev_103);

        pushButton_dev_104 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_104->setObjectName(QStringLiteral("pushButton_dev_104"));
        pushButton_dev_104->setMinimumSize(QSize(64, 64));
        pushButton_dev_104->setMaximumSize(QSize(64, 64));

        horizontalLayout_129->addWidget(pushButton_dev_104);

        pushButton_dev_105 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_105->setObjectName(QStringLiteral("pushButton_dev_105"));
        pushButton_dev_105->setMinimumSize(QSize(64, 64));
        pushButton_dev_105->setMaximumSize(QSize(64, 64));

        horizontalLayout_129->addWidget(pushButton_dev_105);


        verticalLayout_64->addLayout(horizontalLayout_129);

        horizontalLayout_130 = new QHBoxLayout();
        horizontalLayout_130->setSpacing(6);
        horizontalLayout_130->setObjectName(QStringLiteral("horizontalLayout_130"));
        pushButton_dev_106 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_106->setObjectName(QStringLiteral("pushButton_dev_106"));
        pushButton_dev_106->setMinimumSize(QSize(64, 64));
        pushButton_dev_106->setMaximumSize(QSize(64, 64));

        horizontalLayout_130->addWidget(pushButton_dev_106);

        pushButton_dev_107 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_107->setObjectName(QStringLiteral("pushButton_dev_107"));
        pushButton_dev_107->setMinimumSize(QSize(64, 64));
        pushButton_dev_107->setMaximumSize(QSize(64, 64));

        horizontalLayout_130->addWidget(pushButton_dev_107);

        pushButton_dev_108 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_108->setObjectName(QStringLiteral("pushButton_dev_108"));
        pushButton_dev_108->setMinimumSize(QSize(64, 64));
        pushButton_dev_108->setMaximumSize(QSize(64, 64));

        horizontalLayout_130->addWidget(pushButton_dev_108);

        pushButton_dev_109 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_109->setObjectName(QStringLiteral("pushButton_dev_109"));
        pushButton_dev_109->setMinimumSize(QSize(64, 64));
        pushButton_dev_109->setMaximumSize(QSize(64, 64));

        horizontalLayout_130->addWidget(pushButton_dev_109);

        pushButton_dev_110 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_110->setObjectName(QStringLiteral("pushButton_dev_110"));
        pushButton_dev_110->setMinimumSize(QSize(64, 64));
        pushButton_dev_110->setMaximumSize(QSize(64, 64));

        horizontalLayout_130->addWidget(pushButton_dev_110);


        verticalLayout_64->addLayout(horizontalLayout_130);

        horizontalLayout_131 = new QHBoxLayout();
        horizontalLayout_131->setSpacing(6);
        horizontalLayout_131->setObjectName(QStringLiteral("horizontalLayout_131"));
        pushButton_dev_111 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_111->setObjectName(QStringLiteral("pushButton_dev_111"));
        pushButton_dev_111->setMinimumSize(QSize(64, 64));
        pushButton_dev_111->setMaximumSize(QSize(64, 64));

        horizontalLayout_131->addWidget(pushButton_dev_111);

        pushButton_dev_112 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_112->setObjectName(QStringLiteral("pushButton_dev_112"));
        pushButton_dev_112->setMinimumSize(QSize(64, 64));
        pushButton_dev_112->setMaximumSize(QSize(64, 64));

        horizontalLayout_131->addWidget(pushButton_dev_112);

        pushButton_dev_113 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_113->setObjectName(QStringLiteral("pushButton_dev_113"));
        pushButton_dev_113->setMinimumSize(QSize(64, 64));
        pushButton_dev_113->setMaximumSize(QSize(64, 64));

        horizontalLayout_131->addWidget(pushButton_dev_113);

        pushButton_dev_114 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_114->setObjectName(QStringLiteral("pushButton_dev_114"));
        pushButton_dev_114->setMinimumSize(QSize(64, 64));
        pushButton_dev_114->setMaximumSize(QSize(64, 64));

        horizontalLayout_131->addWidget(pushButton_dev_114);

        pushButton_dev_115 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_115->setObjectName(QStringLiteral("pushButton_dev_115"));
        pushButton_dev_115->setMinimumSize(QSize(64, 64));
        pushButton_dev_115->setMaximumSize(QSize(64, 64));

        horizontalLayout_131->addWidget(pushButton_dev_115);


        verticalLayout_64->addLayout(horizontalLayout_131);

        horizontalLayout_132 = new QHBoxLayout();
        horizontalLayout_132->setSpacing(6);
        horizontalLayout_132->setObjectName(QStringLiteral("horizontalLayout_132"));
        pushButton_dev_116 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_116->setObjectName(QStringLiteral("pushButton_dev_116"));
        pushButton_dev_116->setMinimumSize(QSize(64, 64));
        pushButton_dev_116->setMaximumSize(QSize(64, 64));

        horizontalLayout_132->addWidget(pushButton_dev_116);

        pushButton_dev_117 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_117->setObjectName(QStringLiteral("pushButton_dev_117"));
        pushButton_dev_117->setMinimumSize(QSize(64, 64));
        pushButton_dev_117->setMaximumSize(QSize(64, 64));

        horizontalLayout_132->addWidget(pushButton_dev_117);

        pushButton_dev_118 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_118->setObjectName(QStringLiteral("pushButton_dev_118"));
        pushButton_dev_118->setMinimumSize(QSize(64, 64));
        pushButton_dev_118->setMaximumSize(QSize(64, 64));

        horizontalLayout_132->addWidget(pushButton_dev_118);

        pushButton_dev_119 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_119->setObjectName(QStringLiteral("pushButton_dev_119"));
        pushButton_dev_119->setMinimumSize(QSize(64, 64));
        pushButton_dev_119->setMaximumSize(QSize(64, 64));

        horizontalLayout_132->addWidget(pushButton_dev_119);

        pushButton_dev_120 = new QPushButton(scrollAreaWidgetContents);
        pushButton_dev_120->setObjectName(QStringLiteral("pushButton_dev_120"));
        pushButton_dev_120->setMinimumSize(QSize(64, 64));
        pushButton_dev_120->setMaximumSize(QSize(64, 64));

        horizontalLayout_132->addWidget(pushButton_dev_120);


        verticalLayout_64->addLayout(horizontalLayout_132);


        verticalLayout_63->addLayout(verticalLayout_64);


        verticalLayout_62->addLayout(verticalLayout_63);


        verticalLayout_65->addLayout(verticalLayout_62);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_61->addWidget(scrollArea);


        horizontalLayout_134->addWidget(groupBox_24);

        verticalLayout_55 = new QVBoxLayout();
        verticalLayout_55->setSpacing(6);
        verticalLayout_55->setObjectName(QStringLiteral("verticalLayout_55"));
        horizontalLayout_105 = new QHBoxLayout();
        horizontalLayout_105->setSpacing(6);
        horizontalLayout_105->setObjectName(QStringLiteral("horizontalLayout_105"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_105->addItem(horizontalSpacer);

        label_status_led = new QLabel(tab);
        label_status_led->setObjectName(QStringLiteral("label_status_led"));
        label_status_led->setMinimumSize(QSize(128, 128));
        label_status_led->setMaximumSize(QSize(128, 129));
        label_status_led->setFrameShape(QFrame::NoFrame);
        label_status_led->setTextFormat(Qt::AutoText);
        label_status_led->setPixmap(QPixmap(QString::fromUtf8(":/src/source/gray.png")));
        label_status_led->setWordWrap(false);

        horizontalLayout_105->addWidget(label_status_led);


        verticalLayout_55->addLayout(horizontalLayout_105);

        horizontalLayout_133 = new QHBoxLayout();
        horizontalLayout_133->setSpacing(6);
        horizontalLayout_133->setObjectName(QStringLiteral("horizontalLayout_133"));
        horizontalSpacer_87 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_133->addItem(horizontalSpacer_87);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_133->addItem(verticalSpacer_2);


        verticalLayout_55->addLayout(horizontalLayout_133);

        horizontalLayout_107 = new QHBoxLayout();
        horizontalLayout_107->setSpacing(6);
        horizontalLayout_107->setObjectName(QStringLiteral("horizontalLayout_107"));
        horizontalSpacer_86 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_107->addItem(horizontalSpacer_86);

        label_5 = new QLabel(tab);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_107->addWidget(label_5);

        label_timerStepTestTime = new QLabel(tab);
        label_timerStepTestTime->setObjectName(QStringLiteral("label_timerStepTestTime"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font2.setPointSize(36);
        label_timerStepTestTime->setFont(font2);

        horizontalLayout_107->addWidget(label_timerStepTestTime);


        verticalLayout_55->addLayout(horizontalLayout_107);

        pushButton_ResetTest = new QPushButton(tab);
        pushButton_ResetTest->setObjectName(QStringLiteral("pushButton_ResetTest"));
        pushButton_ResetTest->setEnabled(false);
        pushButton_ResetTest->setMinimumSize(QSize(0, 75));
        pushButton_ResetTest->setFont(font1);

        verticalLayout_55->addWidget(pushButton_ResetTest);


        horizontalLayout_134->addLayout(verticalLayout_55);


        verticalLayout_57->addLayout(horizontalLayout_134);

        verticalLayout_53 = new QVBoxLayout();
        verticalLayout_53->setSpacing(6);
        verticalLayout_53->setObjectName(QStringLiteral("verticalLayout_53"));
        horizontalLayout_100 = new QHBoxLayout();
        horizontalLayout_100->setSpacing(6);
        horizontalLayout_100->setObjectName(QStringLiteral("horizontalLayout_100"));
        pushButton_startStep_1 = new QPushButton(tab);
        pushButton_startStep_1->setObjectName(QStringLiteral("pushButton_startStep_1"));
        pushButton_startStep_1->setMinimumSize(QSize(0, 80));
        QFont font3;
        font3.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font3.setPointSize(20);
        pushButton_startStep_1->setFont(font3);

        horizontalLayout_100->addWidget(pushButton_startStep_1);

        line_25 = new QFrame(tab);
        line_25->setObjectName(QStringLiteral("line_25"));
        line_25->setFrameShape(QFrame::VLine);
        line_25->setFrameShadow(QFrame::Sunken);

        horizontalLayout_100->addWidget(line_25);

        pushButton_startStep_2 = new QPushButton(tab);
        pushButton_startStep_2->setObjectName(QStringLiteral("pushButton_startStep_2"));
        pushButton_startStep_2->setMinimumSize(QSize(0, 80));
        pushButton_startStep_2->setFont(font3);

        horizontalLayout_100->addWidget(pushButton_startStep_2);

        line_26 = new QFrame(tab);
        line_26->setObjectName(QStringLiteral("line_26"));
        line_26->setFrameShape(QFrame::VLine);
        line_26->setFrameShadow(QFrame::Sunken);

        horizontalLayout_100->addWidget(line_26);

        pushButton_startStep_3 = new QPushButton(tab);
        pushButton_startStep_3->setObjectName(QStringLiteral("pushButton_startStep_3"));
        pushButton_startStep_3->setMinimumSize(QSize(0, 80));
        pushButton_startStep_3->setFont(font3);

        horizontalLayout_100->addWidget(pushButton_startStep_3);

        line_27 = new QFrame(tab);
        line_27->setObjectName(QStringLiteral("line_27"));
        line_27->setFrameShape(QFrame::VLine);
        line_27->setFrameShadow(QFrame::Sunken);

        horizontalLayout_100->addWidget(line_27);

        pushButton_startStep_4 = new QPushButton(tab);
        pushButton_startStep_4->setObjectName(QStringLiteral("pushButton_startStep_4"));
        pushButton_startStep_4->setMinimumSize(QSize(0, 80));
        pushButton_startStep_4->setFont(font3);

        horizontalLayout_100->addWidget(pushButton_startStep_4);

        line_28 = new QFrame(tab);
        line_28->setObjectName(QStringLiteral("line_28"));
        line_28->setFrameShape(QFrame::VLine);
        line_28->setFrameShadow(QFrame::Sunken);

        horizontalLayout_100->addWidget(line_28);

        pushButton_startStep_5 = new QPushButton(tab);
        pushButton_startStep_5->setObjectName(QStringLiteral("pushButton_startStep_5"));
        pushButton_startStep_5->setMinimumSize(QSize(0, 80));
        pushButton_startStep_5->setFont(font3);

        horizontalLayout_100->addWidget(pushButton_startStep_5);


        verticalLayout_53->addLayout(horizontalLayout_100);

        line_21 = new QFrame(tab);
        line_21->setObjectName(QStringLiteral("line_21"));
        line_21->setFrameShape(QFrame::HLine);
        line_21->setFrameShadow(QFrame::Sunken);

        verticalLayout_53->addWidget(line_21);

        horizontalLayout_101 = new QHBoxLayout();
        horizontalLayout_101->setSpacing(6);
        horizontalLayout_101->setObjectName(QStringLiteral("horizontalLayout_101"));
        pushButton_startStep_6 = new QPushButton(tab);
        pushButton_startStep_6->setObjectName(QStringLiteral("pushButton_startStep_6"));
        pushButton_startStep_6->setMinimumSize(QSize(0, 80));
        pushButton_startStep_6->setFont(font3);

        horizontalLayout_101->addWidget(pushButton_startStep_6);

        line_32 = new QFrame(tab);
        line_32->setObjectName(QStringLiteral("line_32"));
        line_32->setFrameShape(QFrame::VLine);
        line_32->setFrameShadow(QFrame::Sunken);

        horizontalLayout_101->addWidget(line_32);

        pushButton_startStep_7 = new QPushButton(tab);
        pushButton_startStep_7->setObjectName(QStringLiteral("pushButton_startStep_7"));
        pushButton_startStep_7->setMinimumSize(QSize(0, 80));
        pushButton_startStep_7->setFont(font3);

        horizontalLayout_101->addWidget(pushButton_startStep_7);

        line_31 = new QFrame(tab);
        line_31->setObjectName(QStringLiteral("line_31"));
        line_31->setFrameShape(QFrame::VLine);
        line_31->setFrameShadow(QFrame::Sunken);

        horizontalLayout_101->addWidget(line_31);

        pushButton_startStep_8 = new QPushButton(tab);
        pushButton_startStep_8->setObjectName(QStringLiteral("pushButton_startStep_8"));
        pushButton_startStep_8->setMinimumSize(QSize(0, 80));
        pushButton_startStep_8->setFont(font3);

        horizontalLayout_101->addWidget(pushButton_startStep_8);

        line_30 = new QFrame(tab);
        line_30->setObjectName(QStringLiteral("line_30"));
        line_30->setFrameShape(QFrame::VLine);
        line_30->setFrameShadow(QFrame::Sunken);

        horizontalLayout_101->addWidget(line_30);

        pushButton_startStep_9 = new QPushButton(tab);
        pushButton_startStep_9->setObjectName(QStringLiteral("pushButton_startStep_9"));
        pushButton_startStep_9->setMinimumSize(QSize(0, 80));
        pushButton_startStep_9->setFont(font3);

        horizontalLayout_101->addWidget(pushButton_startStep_9);

        line_29 = new QFrame(tab);
        line_29->setObjectName(QStringLiteral("line_29"));
        line_29->setFrameShape(QFrame::VLine);
        line_29->setFrameShadow(QFrame::Sunken);

        horizontalLayout_101->addWidget(line_29);

        pushButton_startStep_10 = new QPushButton(tab);
        pushButton_startStep_10->setObjectName(QStringLiteral("pushButton_startStep_10"));
        pushButton_startStep_10->setMinimumSize(QSize(0, 80));
        pushButton_startStep_10->setFont(font3);

        horizontalLayout_101->addWidget(pushButton_startStep_10);


        verticalLayout_53->addLayout(horizontalLayout_101);


        verticalLayout_57->addLayout(verticalLayout_53);


        verticalLayout_58->addLayout(verticalLayout_57);

        line_22 = new QFrame(tab);
        line_22->setObjectName(QStringLiteral("line_22"));
        line_22->setFrameShape(QFrame::HLine);
        line_22->setFrameShadow(QFrame::Sunken);

        verticalLayout_58->addWidget(line_22);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_40 = new QVBoxLayout(tab_2);
        verticalLayout_40->setSpacing(6);
        verticalLayout_40->setContentsMargins(11, 11, 11, 11);
        verticalLayout_40->setObjectName(QStringLiteral("verticalLayout_40"));
        widget = new QWidget(tab_2);
        widget->setObjectName(QStringLiteral("widget"));
        verticalLayout_13 = new QVBoxLayout(widget);
        verticalLayout_13->setSpacing(6);
        verticalLayout_13->setContentsMargins(11, 11, 11, 11);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setSpacing(6);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        tabWidget_2 = new QTabWidget(widget);
        tabWidget_2->setObjectName(QStringLiteral("tabWidget_2"));
        tabWidget_2->setStyleSheet(QString::fromUtf8("QTabBar::tab{height:30;width:61;font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";}\n"
"QTabBar::tab:selected\n"
"{\n"
"color: red;\n"
"\n"
"}"));
        tabWidget_2->setTabPosition(QTabWidget::North);
        tabWidget_2->setTabShape(QTabWidget::Triangular);
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        horizontalLayout_18 = new QHBoxLayout(tab_3);
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        groupBox_2 = new QGroupBox(tab_3);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setAlignment(Qt::AlignCenter);
        verticalLayout_6 = new QVBoxLayout(groupBox_2);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_7->addWidget(label_8);

        doubleSpinBox_SetOnTime_1 = new QDoubleSpinBox(groupBox_2);
        doubleSpinBox_SetOnTime_1->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_1"));
        doubleSpinBox_SetOnTime_1->setMinimumSize(QSize(150, 35));
        doubleSpinBox_SetOnTime_1->setMaximumSize(QSize(16777215, 40));
        QFont font4;
        font4.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font4.setPointSize(22);
        doubleSpinBox_SetOnTime_1->setFont(font4);
        doubleSpinBox_SetOnTime_1->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_1->setDecimals(1);
        doubleSpinBox_SetOnTime_1->setMaximum(999999);
        doubleSpinBox_SetOnTime_1->setValue(4.2);

        horizontalLayout_7->addWidget(doubleSpinBox_SetOnTime_1);

        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        QFont font5;
        font5.setPointSize(22);
        label_9->setFont(font5);

        horizontalLayout_7->addWidget(label_9);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_6);


        verticalLayout_3->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QStringLiteral("label_11"));
        QFont font6;
        font6.setPointSize(26);
        label_11->setFont(font6);

        horizontalLayout_8->addWidget(label_11);

        doubleSpinBox_SetOnTimeError_1 = new QDoubleSpinBox(groupBox_2);
        doubleSpinBox_SetOnTimeError_1->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_1"));
        doubleSpinBox_SetOnTimeError_1->setMinimumSize(QSize(150, 35));
        doubleSpinBox_SetOnTimeError_1->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_1->setFont(font4);
        doubleSpinBox_SetOnTimeError_1->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_1->setDecimals(1);
        doubleSpinBox_SetOnTimeError_1->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_1->setValue(1.8);

        horizontalLayout_8->addWidget(doubleSpinBox_SetOnTimeError_1);

        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font5);

        horizontalLayout_8->addWidget(label_10);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout_8);

        line = new QFrame(groupBox_2);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout_9->addWidget(label_13);

        doubleSpinBox_SetOffTime_1 = new QDoubleSpinBox(groupBox_2);
        doubleSpinBox_SetOffTime_1->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_1"));
        doubleSpinBox_SetOffTime_1->setMinimumSize(QSize(150, 35));
        doubleSpinBox_SetOffTime_1->setMaximumSize(QSize(16777215, 40));
        QFont font7;
        font7.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font7.setPointSize(22);
        font7.setBold(false);
        font7.setWeight(50);
        doubleSpinBox_SetOffTime_1->setFont(font7);
        doubleSpinBox_SetOffTime_1->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_1->setDecimals(1);
        doubleSpinBox_SetOffTime_1->setMaximum(999999);
        doubleSpinBox_SetOffTime_1->setValue(20.8);

        horizontalLayout_9->addWidget(doubleSpinBox_SetOffTime_1);

        label_15 = new QLabel(groupBox_2);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setFont(font5);

        horizontalLayout_9->addWidget(label_15);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_9);


        verticalLayout_3->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setFont(font6);

        horizontalLayout_10->addWidget(label_14);

        doubleSpinBox_SetOffTimeError_1 = new QDoubleSpinBox(groupBox_2);
        doubleSpinBox_SetOffTimeError_1->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_1"));
        doubleSpinBox_SetOffTimeError_1->setMinimumSize(QSize(150, 35));
        doubleSpinBox_SetOffTimeError_1->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_1->setFont(font4);
        doubleSpinBox_SetOffTimeError_1->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_1->setDecimals(1);
        doubleSpinBox_SetOffTimeError_1->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_1->setValue(1.8);

        horizontalLayout_10->addWidget(doubleSpinBox_SetOffTimeError_1);

        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setFont(font5);

        horizontalLayout_10->addWidget(label_12);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_3);


        verticalLayout_3->addLayout(horizontalLayout_10);


        verticalLayout_6->addLayout(verticalLayout_3);


        horizontalLayout_18->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(tab_3);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setAlignment(Qt::AlignCenter);
        verticalLayout_5 = new QVBoxLayout(groupBox_3);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label_16 = new QLabel(groupBox_3);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_11->addWidget(label_16);

        doubleSpinBox_SetOnTime_2 = new QDoubleSpinBox(groupBox_3);
        doubleSpinBox_SetOnTime_2->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_2"));
        doubleSpinBox_SetOnTime_2->setMinimumSize(QSize(150, 35));
        doubleSpinBox_SetOnTime_2->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_2->setFont(font4);
        doubleSpinBox_SetOnTime_2->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_2->setDecimals(1);
        doubleSpinBox_SetOnTime_2->setMaximum(999999);
        doubleSpinBox_SetOnTime_2->setValue(4.2);

        horizontalLayout_11->addWidget(doubleSpinBox_SetOnTime_2);

        label_17 = new QLabel(groupBox_3);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setFont(font5);

        horizontalLayout_11->addWidget(label_17);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_10);


        verticalLayout_4->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        label_18 = new QLabel(groupBox_3);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setFont(font6);

        horizontalLayout_12->addWidget(label_18);

        doubleSpinBox_SetOnTimeError_2 = new QDoubleSpinBox(groupBox_3);
        doubleSpinBox_SetOnTimeError_2->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_2"));
        doubleSpinBox_SetOnTimeError_2->setMinimumSize(QSize(150, 35));
        doubleSpinBox_SetOnTimeError_2->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_2->setFont(font4);
        doubleSpinBox_SetOnTimeError_2->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_2->setDecimals(1);
        doubleSpinBox_SetOnTimeError_2->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_2->setValue(1.8);

        horizontalLayout_12->addWidget(doubleSpinBox_SetOnTimeError_2);

        label_19 = new QLabel(groupBox_3);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setFont(font5);

        horizontalLayout_12->addWidget(label_19);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_4);


        verticalLayout_4->addLayout(horizontalLayout_12);

        line_2 = new QFrame(groupBox_3);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line_2);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        label_20 = new QLabel(groupBox_3);
        label_20->setObjectName(QStringLiteral("label_20"));

        horizontalLayout_13->addWidget(label_20);

        doubleSpinBox_SetOffTime_2 = new QDoubleSpinBox(groupBox_3);
        doubleSpinBox_SetOffTime_2->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_2"));
        doubleSpinBox_SetOffTime_2->setMinimumSize(QSize(150, 35));
        doubleSpinBox_SetOffTime_2->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_2->setFont(font7);
        doubleSpinBox_SetOffTime_2->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_2->setDecimals(1);
        doubleSpinBox_SetOffTime_2->setMaximum(999999);
        doubleSpinBox_SetOffTime_2->setValue(20.8);

        horizontalLayout_13->addWidget(doubleSpinBox_SetOffTime_2);

        label_21 = new QLabel(groupBox_3);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setFont(font5);

        horizontalLayout_13->addWidget(label_21);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_11);


        verticalLayout_4->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        label_22 = new QLabel(groupBox_3);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setFont(font6);

        horizontalLayout_14->addWidget(label_22);

        doubleSpinBox_SetOffTimeError_2 = new QDoubleSpinBox(groupBox_3);
        doubleSpinBox_SetOffTimeError_2->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_2"));
        doubleSpinBox_SetOffTimeError_2->setMinimumSize(QSize(150, 35));
        doubleSpinBox_SetOffTimeError_2->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_2->setFont(font4);
        doubleSpinBox_SetOffTimeError_2->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_2->setDecimals(1);
        doubleSpinBox_SetOffTimeError_2->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_2->setValue(1.8);

        horizontalLayout_14->addWidget(doubleSpinBox_SetOffTimeError_2);

        label_23 = new QLabel(groupBox_3);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setFont(font5);

        horizontalLayout_14->addWidget(label_23);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_5);


        verticalLayout_4->addLayout(horizontalLayout_14);


        verticalLayout_5->addLayout(verticalLayout_4);


        horizontalLayout_18->addWidget(groupBox_3);

        tabWidget_2->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        horizontalLayout_27 = new QHBoxLayout(tab_4);
        horizontalLayout_27->setSpacing(6);
        horizontalLayout_27->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_27->setObjectName(QStringLiteral("horizontalLayout_27"));
        groupBox_6 = new QGroupBox(tab_4);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setAlignment(Qt::AlignCenter);
        verticalLayout_16 = new QVBoxLayout(groupBox_6);
        verticalLayout_16->setSpacing(6);
        verticalLayout_16->setContentsMargins(11, 11, 11, 11);
        verticalLayout_16->setObjectName(QStringLiteral("verticalLayout_16"));
        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setSpacing(6);
        verticalLayout_17->setObjectName(QStringLiteral("verticalLayout_17"));
        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        label_24 = new QLabel(groupBox_6);
        label_24->setObjectName(QStringLiteral("label_24"));

        horizontalLayout_19->addWidget(label_24);

        doubleSpinBox_SetOnTime_3 = new QDoubleSpinBox(groupBox_6);
        doubleSpinBox_SetOnTime_3->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_3"));
        doubleSpinBox_SetOnTime_3->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_3->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_3->setFont(font4);
        doubleSpinBox_SetOnTime_3->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_3->setDecimals(1);
        doubleSpinBox_SetOnTime_3->setMaximum(999999);
        doubleSpinBox_SetOnTime_3->setValue(4.2);

        horizontalLayout_19->addWidget(doubleSpinBox_SetOnTime_3);

        label_25 = new QLabel(groupBox_6);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setFont(font5);

        horizontalLayout_19->addWidget(label_25);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_12);


        verticalLayout_17->addLayout(horizontalLayout_19);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        label_26 = new QLabel(groupBox_6);
        label_26->setObjectName(QStringLiteral("label_26"));
        label_26->setFont(font6);

        horizontalLayout_20->addWidget(label_26);

        doubleSpinBox_SetOnTimeError_3 = new QDoubleSpinBox(groupBox_6);
        doubleSpinBox_SetOnTimeError_3->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_3"));
        doubleSpinBox_SetOnTimeError_3->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_3->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_3->setFont(font4);
        doubleSpinBox_SetOnTimeError_3->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_3->setDecimals(1);
        doubleSpinBox_SetOnTimeError_3->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_3->setValue(1.8);

        horizontalLayout_20->addWidget(doubleSpinBox_SetOnTimeError_3);

        label_27 = new QLabel(groupBox_6);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setFont(font5);

        horizontalLayout_20->addWidget(label_27);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_13);


        verticalLayout_17->addLayout(horizontalLayout_20);

        line_3 = new QFrame(groupBox_6);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout_17->addWidget(line_3);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        label_28 = new QLabel(groupBox_6);
        label_28->setObjectName(QStringLiteral("label_28"));

        horizontalLayout_21->addWidget(label_28);

        doubleSpinBox_SetOffTime_3 = new QDoubleSpinBox(groupBox_6);
        doubleSpinBox_SetOffTime_3->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_3"));
        doubleSpinBox_SetOffTime_3->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_3->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_3->setFont(font7);
        doubleSpinBox_SetOffTime_3->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_3->setDecimals(1);
        doubleSpinBox_SetOffTime_3->setMaximum(999999);
        doubleSpinBox_SetOffTime_3->setValue(20.8);

        horizontalLayout_21->addWidget(doubleSpinBox_SetOffTime_3);

        label_29 = new QLabel(groupBox_6);
        label_29->setObjectName(QStringLiteral("label_29"));
        label_29->setFont(font5);

        horizontalLayout_21->addWidget(label_29);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_14);


        verticalLayout_17->addLayout(horizontalLayout_21);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        label_30 = new QLabel(groupBox_6);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setFont(font6);

        horizontalLayout_22->addWidget(label_30);

        doubleSpinBox_SetOffTimeError_3 = new QDoubleSpinBox(groupBox_6);
        doubleSpinBox_SetOffTimeError_3->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_3"));
        doubleSpinBox_SetOffTimeError_3->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_3->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_3->setFont(font4);
        doubleSpinBox_SetOffTimeError_3->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_3->setDecimals(1);
        doubleSpinBox_SetOffTimeError_3->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_3->setValue(1.8);

        horizontalLayout_22->addWidget(doubleSpinBox_SetOffTimeError_3);

        label_31 = new QLabel(groupBox_6);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setFont(font5);

        horizontalLayout_22->addWidget(label_31);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_15);


        verticalLayout_17->addLayout(horizontalLayout_22);


        verticalLayout_16->addLayout(verticalLayout_17);


        horizontalLayout_27->addWidget(groupBox_6);

        groupBox_7 = new QGroupBox(tab_4);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setAlignment(Qt::AlignCenter);
        verticalLayout_18 = new QVBoxLayout(groupBox_7);
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setContentsMargins(11, 11, 11, 11);
        verticalLayout_18->setObjectName(QStringLiteral("verticalLayout_18"));
        verticalLayout_19 = new QVBoxLayout();
        verticalLayout_19->setSpacing(6);
        verticalLayout_19->setObjectName(QStringLiteral("verticalLayout_19"));
        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setObjectName(QStringLiteral("horizontalLayout_23"));
        label_32 = new QLabel(groupBox_7);
        label_32->setObjectName(QStringLiteral("label_32"));

        horizontalLayout_23->addWidget(label_32);

        doubleSpinBox_SetOnTime_4 = new QDoubleSpinBox(groupBox_7);
        doubleSpinBox_SetOnTime_4->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_4"));
        doubleSpinBox_SetOnTime_4->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_4->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_4->setFont(font4);
        doubleSpinBox_SetOnTime_4->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_4->setDecimals(1);
        doubleSpinBox_SetOnTime_4->setMaximum(999999);
        doubleSpinBox_SetOnTime_4->setValue(4.2);

        horizontalLayout_23->addWidget(doubleSpinBox_SetOnTime_4);

        label_33 = new QLabel(groupBox_7);
        label_33->setObjectName(QStringLiteral("label_33"));
        label_33->setFont(font5);

        horizontalLayout_23->addWidget(label_33);

        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_16);


        verticalLayout_19->addLayout(horizontalLayout_23);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QStringLiteral("horizontalLayout_24"));
        label_34 = new QLabel(groupBox_7);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setFont(font6);

        horizontalLayout_24->addWidget(label_34);

        doubleSpinBox_SetOnTimeError_4 = new QDoubleSpinBox(groupBox_7);
        doubleSpinBox_SetOnTimeError_4->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_4"));
        doubleSpinBox_SetOnTimeError_4->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_4->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_4->setFont(font4);
        doubleSpinBox_SetOnTimeError_4->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_4->setDecimals(1);
        doubleSpinBox_SetOnTimeError_4->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_4->setValue(1.8);

        horizontalLayout_24->addWidget(doubleSpinBox_SetOnTimeError_4);

        label_35 = new QLabel(groupBox_7);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setFont(font5);

        horizontalLayout_24->addWidget(label_35);

        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_24->addItem(horizontalSpacer_17);


        verticalLayout_19->addLayout(horizontalLayout_24);

        line_4 = new QFrame(groupBox_7);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);

        verticalLayout_19->addWidget(line_4);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setSpacing(6);
        horizontalLayout_25->setObjectName(QStringLiteral("horizontalLayout_25"));
        label_36 = new QLabel(groupBox_7);
        label_36->setObjectName(QStringLiteral("label_36"));

        horizontalLayout_25->addWidget(label_36);

        doubleSpinBox_SetOffTime_4 = new QDoubleSpinBox(groupBox_7);
        doubleSpinBox_SetOffTime_4->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_4"));
        doubleSpinBox_SetOffTime_4->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_4->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_4->setFont(font7);
        doubleSpinBox_SetOffTime_4->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_4->setDecimals(1);
        doubleSpinBox_SetOffTime_4->setMaximum(999999);
        doubleSpinBox_SetOffTime_4->setValue(20.8);

        horizontalLayout_25->addWidget(doubleSpinBox_SetOffTime_4);

        label_37 = new QLabel(groupBox_7);
        label_37->setObjectName(QStringLiteral("label_37"));
        label_37->setFont(font5);

        horizontalLayout_25->addWidget(label_37);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_25->addItem(horizontalSpacer_18);


        verticalLayout_19->addLayout(horizontalLayout_25);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setSpacing(6);
        horizontalLayout_26->setObjectName(QStringLiteral("horizontalLayout_26"));
        label_38 = new QLabel(groupBox_7);
        label_38->setObjectName(QStringLiteral("label_38"));
        label_38->setFont(font6);

        horizontalLayout_26->addWidget(label_38);

        doubleSpinBox_SetOffTimeError_4 = new QDoubleSpinBox(groupBox_7);
        doubleSpinBox_SetOffTimeError_4->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_4"));
        doubleSpinBox_SetOffTimeError_4->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_4->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_4->setFont(font4);
        doubleSpinBox_SetOffTimeError_4->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_4->setDecimals(1);
        doubleSpinBox_SetOffTimeError_4->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_4->setValue(1.8);

        horizontalLayout_26->addWidget(doubleSpinBox_SetOffTimeError_4);

        label_39 = new QLabel(groupBox_7);
        label_39->setObjectName(QStringLiteral("label_39"));
        label_39->setFont(font5);

        horizontalLayout_26->addWidget(label_39);

        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_26->addItem(horizontalSpacer_19);


        verticalLayout_19->addLayout(horizontalLayout_26);


        verticalLayout_18->addLayout(verticalLayout_19);


        horizontalLayout_27->addWidget(groupBox_7);

        tabWidget_2->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        horizontalLayout_36 = new QHBoxLayout(tab_5);
        horizontalLayout_36->setSpacing(6);
        horizontalLayout_36->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_36->setObjectName(QStringLiteral("horizontalLayout_36"));
        groupBox_9 = new QGroupBox(tab_5);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        groupBox_9->setAlignment(Qt::AlignCenter);
        verticalLayout_22 = new QVBoxLayout(groupBox_9);
        verticalLayout_22->setSpacing(6);
        verticalLayout_22->setContentsMargins(11, 11, 11, 11);
        verticalLayout_22->setObjectName(QStringLiteral("verticalLayout_22"));
        verticalLayout_21 = new QVBoxLayout();
        verticalLayout_21->setSpacing(6);
        verticalLayout_21->setObjectName(QStringLiteral("verticalLayout_21"));
        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setSpacing(6);
        horizontalLayout_28->setObjectName(QStringLiteral("horizontalLayout_28"));
        label_40 = new QLabel(groupBox_9);
        label_40->setObjectName(QStringLiteral("label_40"));

        horizontalLayout_28->addWidget(label_40);

        doubleSpinBox_SetOnTime_5 = new QDoubleSpinBox(groupBox_9);
        doubleSpinBox_SetOnTime_5->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_5"));
        doubleSpinBox_SetOnTime_5->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_5->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_5->setFont(font4);
        doubleSpinBox_SetOnTime_5->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_5->setDecimals(1);
        doubleSpinBox_SetOnTime_5->setMaximum(999999);
        doubleSpinBox_SetOnTime_5->setValue(4.2);

        horizontalLayout_28->addWidget(doubleSpinBox_SetOnTime_5);

        label_41 = new QLabel(groupBox_9);
        label_41->setObjectName(QStringLiteral("label_41"));
        label_41->setFont(font5);

        horizontalLayout_28->addWidget(label_41);

        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_28->addItem(horizontalSpacer_20);


        verticalLayout_21->addLayout(horizontalLayout_28);

        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setSpacing(6);
        horizontalLayout_29->setObjectName(QStringLiteral("horizontalLayout_29"));
        label_42 = new QLabel(groupBox_9);
        label_42->setObjectName(QStringLiteral("label_42"));
        label_42->setFont(font6);

        horizontalLayout_29->addWidget(label_42);

        doubleSpinBox_SetOnTimeError_5 = new QDoubleSpinBox(groupBox_9);
        doubleSpinBox_SetOnTimeError_5->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_5"));
        doubleSpinBox_SetOnTimeError_5->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_5->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_5->setFont(font4);
        doubleSpinBox_SetOnTimeError_5->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_5->setDecimals(1);
        doubleSpinBox_SetOnTimeError_5->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_5->setValue(1.8);

        horizontalLayout_29->addWidget(doubleSpinBox_SetOnTimeError_5);

        label_43 = new QLabel(groupBox_9);
        label_43->setObjectName(QStringLiteral("label_43"));
        label_43->setFont(font5);

        horizontalLayout_29->addWidget(label_43);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_29->addItem(horizontalSpacer_21);


        verticalLayout_21->addLayout(horizontalLayout_29);

        line_5 = new QFrame(groupBox_9);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        verticalLayout_21->addWidget(line_5);

        horizontalLayout_30 = new QHBoxLayout();
        horizontalLayout_30->setSpacing(6);
        horizontalLayout_30->setObjectName(QStringLiteral("horizontalLayout_30"));
        label_44 = new QLabel(groupBox_9);
        label_44->setObjectName(QStringLiteral("label_44"));

        horizontalLayout_30->addWidget(label_44);

        doubleSpinBox_SetOffTime_5 = new QDoubleSpinBox(groupBox_9);
        doubleSpinBox_SetOffTime_5->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_5"));
        doubleSpinBox_SetOffTime_5->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_5->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_5->setFont(font7);
        doubleSpinBox_SetOffTime_5->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_5->setDecimals(1);
        doubleSpinBox_SetOffTime_5->setMaximum(999999);
        doubleSpinBox_SetOffTime_5->setValue(20.8);

        horizontalLayout_30->addWidget(doubleSpinBox_SetOffTime_5);

        label_45 = new QLabel(groupBox_9);
        label_45->setObjectName(QStringLiteral("label_45"));
        label_45->setFont(font5);

        horizontalLayout_30->addWidget(label_45);

        horizontalSpacer_22 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_30->addItem(horizontalSpacer_22);


        verticalLayout_21->addLayout(horizontalLayout_30);

        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setSpacing(6);
        horizontalLayout_31->setObjectName(QStringLiteral("horizontalLayout_31"));
        label_46 = new QLabel(groupBox_9);
        label_46->setObjectName(QStringLiteral("label_46"));
        label_46->setFont(font6);

        horizontalLayout_31->addWidget(label_46);

        doubleSpinBox_SetOffTimeError_5 = new QDoubleSpinBox(groupBox_9);
        doubleSpinBox_SetOffTimeError_5->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_5"));
        doubleSpinBox_SetOffTimeError_5->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_5->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_5->setFont(font4);
        doubleSpinBox_SetOffTimeError_5->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_5->setDecimals(1);
        doubleSpinBox_SetOffTimeError_5->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_5->setValue(1.8);

        horizontalLayout_31->addWidget(doubleSpinBox_SetOffTimeError_5);

        label_47 = new QLabel(groupBox_9);
        label_47->setObjectName(QStringLiteral("label_47"));
        label_47->setFont(font5);

        horizontalLayout_31->addWidget(label_47);

        horizontalSpacer_23 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_31->addItem(horizontalSpacer_23);


        verticalLayout_21->addLayout(horizontalLayout_31);


        verticalLayout_22->addLayout(verticalLayout_21);


        horizontalLayout_36->addWidget(groupBox_9);

        groupBox_8 = new QGroupBox(tab_5);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        groupBox_8->setAlignment(Qt::AlignCenter);
        verticalLayout_20 = new QVBoxLayout(groupBox_8);
        verticalLayout_20->setSpacing(6);
        verticalLayout_20->setContentsMargins(11, 11, 11, 11);
        verticalLayout_20->setObjectName(QStringLiteral("verticalLayout_20"));
        verticalLayout_23 = new QVBoxLayout();
        verticalLayout_23->setSpacing(6);
        verticalLayout_23->setObjectName(QStringLiteral("verticalLayout_23"));
        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setSpacing(6);
        horizontalLayout_32->setObjectName(QStringLiteral("horizontalLayout_32"));
        label_48 = new QLabel(groupBox_8);
        label_48->setObjectName(QStringLiteral("label_48"));

        horizontalLayout_32->addWidget(label_48);

        doubleSpinBox_SetOnTime_6 = new QDoubleSpinBox(groupBox_8);
        doubleSpinBox_SetOnTime_6->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_6"));
        doubleSpinBox_SetOnTime_6->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_6->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_6->setFont(font4);
        doubleSpinBox_SetOnTime_6->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_6->setDecimals(1);
        doubleSpinBox_SetOnTime_6->setMaximum(999999);
        doubleSpinBox_SetOnTime_6->setValue(4.2);

        horizontalLayout_32->addWidget(doubleSpinBox_SetOnTime_6);

        label_49 = new QLabel(groupBox_8);
        label_49->setObjectName(QStringLiteral("label_49"));
        label_49->setFont(font5);

        horizontalLayout_32->addWidget(label_49);

        horizontalSpacer_24 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_32->addItem(horizontalSpacer_24);


        verticalLayout_23->addLayout(horizontalLayout_32);

        horizontalLayout_33 = new QHBoxLayout();
        horizontalLayout_33->setSpacing(6);
        horizontalLayout_33->setObjectName(QStringLiteral("horizontalLayout_33"));
        label_50 = new QLabel(groupBox_8);
        label_50->setObjectName(QStringLiteral("label_50"));
        label_50->setFont(font6);

        horizontalLayout_33->addWidget(label_50);

        doubleSpinBox_SetOnTimeError_6 = new QDoubleSpinBox(groupBox_8);
        doubleSpinBox_SetOnTimeError_6->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_6"));
        doubleSpinBox_SetOnTimeError_6->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_6->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_6->setFont(font4);
        doubleSpinBox_SetOnTimeError_6->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_6->setDecimals(1);
        doubleSpinBox_SetOnTimeError_6->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_6->setValue(1.8);

        horizontalLayout_33->addWidget(doubleSpinBox_SetOnTimeError_6);

        label_51 = new QLabel(groupBox_8);
        label_51->setObjectName(QStringLiteral("label_51"));
        label_51->setFont(font5);

        horizontalLayout_33->addWidget(label_51);

        horizontalSpacer_25 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_33->addItem(horizontalSpacer_25);


        verticalLayout_23->addLayout(horizontalLayout_33);

        line_6 = new QFrame(groupBox_8);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);

        verticalLayout_23->addWidget(line_6);

        horizontalLayout_34 = new QHBoxLayout();
        horizontalLayout_34->setSpacing(6);
        horizontalLayout_34->setObjectName(QStringLiteral("horizontalLayout_34"));
        label_52 = new QLabel(groupBox_8);
        label_52->setObjectName(QStringLiteral("label_52"));

        horizontalLayout_34->addWidget(label_52);

        doubleSpinBox_SetOffTime_6 = new QDoubleSpinBox(groupBox_8);
        doubleSpinBox_SetOffTime_6->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_6"));
        doubleSpinBox_SetOffTime_6->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_6->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_6->setFont(font7);
        doubleSpinBox_SetOffTime_6->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_6->setDecimals(1);
        doubleSpinBox_SetOffTime_6->setMaximum(999999);
        doubleSpinBox_SetOffTime_6->setValue(20.8);

        horizontalLayout_34->addWidget(doubleSpinBox_SetOffTime_6);

        label_53 = new QLabel(groupBox_8);
        label_53->setObjectName(QStringLiteral("label_53"));
        label_53->setFont(font5);

        horizontalLayout_34->addWidget(label_53);

        horizontalSpacer_26 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_34->addItem(horizontalSpacer_26);


        verticalLayout_23->addLayout(horizontalLayout_34);

        horizontalLayout_35 = new QHBoxLayout();
        horizontalLayout_35->setSpacing(6);
        horizontalLayout_35->setObjectName(QStringLiteral("horizontalLayout_35"));
        label_54 = new QLabel(groupBox_8);
        label_54->setObjectName(QStringLiteral("label_54"));
        label_54->setFont(font6);

        horizontalLayout_35->addWidget(label_54);

        doubleSpinBox_SetOffTimeError_6 = new QDoubleSpinBox(groupBox_8);
        doubleSpinBox_SetOffTimeError_6->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_6"));
        doubleSpinBox_SetOffTimeError_6->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_6->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_6->setFont(font4);
        doubleSpinBox_SetOffTimeError_6->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_6->setDecimals(1);
        doubleSpinBox_SetOffTimeError_6->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_6->setValue(1.8);

        horizontalLayout_35->addWidget(doubleSpinBox_SetOffTimeError_6);

        label_55 = new QLabel(groupBox_8);
        label_55->setObjectName(QStringLiteral("label_55"));
        label_55->setFont(font5);

        horizontalLayout_35->addWidget(label_55);

        horizontalSpacer_27 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_27);


        verticalLayout_23->addLayout(horizontalLayout_35);


        verticalLayout_20->addLayout(verticalLayout_23);


        horizontalLayout_36->addWidget(groupBox_8);

        tabWidget_2->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QStringLiteral("tab_6"));
        horizontalLayout_45 = new QHBoxLayout(tab_6);
        horizontalLayout_45->setSpacing(6);
        horizontalLayout_45->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_45->setObjectName(QStringLiteral("horizontalLayout_45"));
        groupBox_11 = new QGroupBox(tab_6);
        groupBox_11->setObjectName(QStringLiteral("groupBox_11"));
        groupBox_11->setAlignment(Qt::AlignCenter);
        verticalLayout_26 = new QVBoxLayout(groupBox_11);
        verticalLayout_26->setSpacing(6);
        verticalLayout_26->setContentsMargins(11, 11, 11, 11);
        verticalLayout_26->setObjectName(QStringLiteral("verticalLayout_26"));
        verticalLayout_25 = new QVBoxLayout();
        verticalLayout_25->setSpacing(6);
        verticalLayout_25->setObjectName(QStringLiteral("verticalLayout_25"));
        horizontalLayout_37 = new QHBoxLayout();
        horizontalLayout_37->setSpacing(6);
        horizontalLayout_37->setObjectName(QStringLiteral("horizontalLayout_37"));
        label_56 = new QLabel(groupBox_11);
        label_56->setObjectName(QStringLiteral("label_56"));

        horizontalLayout_37->addWidget(label_56);

        doubleSpinBox_SetOnTime_7 = new QDoubleSpinBox(groupBox_11);
        doubleSpinBox_SetOnTime_7->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_7"));
        doubleSpinBox_SetOnTime_7->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_7->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_7->setFont(font4);
        doubleSpinBox_SetOnTime_7->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_7->setDecimals(1);
        doubleSpinBox_SetOnTime_7->setMaximum(999999);
        doubleSpinBox_SetOnTime_7->setValue(4.2);

        horizontalLayout_37->addWidget(doubleSpinBox_SetOnTime_7);

        label_57 = new QLabel(groupBox_11);
        label_57->setObjectName(QStringLiteral("label_57"));
        label_57->setFont(font5);

        horizontalLayout_37->addWidget(label_57);

        horizontalSpacer_28 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_37->addItem(horizontalSpacer_28);


        verticalLayout_25->addLayout(horizontalLayout_37);

        horizontalLayout_38 = new QHBoxLayout();
        horizontalLayout_38->setSpacing(6);
        horizontalLayout_38->setObjectName(QStringLiteral("horizontalLayout_38"));
        label_58 = new QLabel(groupBox_11);
        label_58->setObjectName(QStringLiteral("label_58"));
        label_58->setFont(font6);

        horizontalLayout_38->addWidget(label_58);

        doubleSpinBox_SetOnTimeError_7 = new QDoubleSpinBox(groupBox_11);
        doubleSpinBox_SetOnTimeError_7->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_7"));
        doubleSpinBox_SetOnTimeError_7->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_7->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_7->setFont(font4);
        doubleSpinBox_SetOnTimeError_7->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_7->setDecimals(1);
        doubleSpinBox_SetOnTimeError_7->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_7->setValue(1.8);

        horizontalLayout_38->addWidget(doubleSpinBox_SetOnTimeError_7);

        label_59 = new QLabel(groupBox_11);
        label_59->setObjectName(QStringLiteral("label_59"));
        label_59->setFont(font5);

        horizontalLayout_38->addWidget(label_59);

        horizontalSpacer_29 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_38->addItem(horizontalSpacer_29);


        verticalLayout_25->addLayout(horizontalLayout_38);

        line_7 = new QFrame(groupBox_11);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setFrameShape(QFrame::HLine);
        line_7->setFrameShadow(QFrame::Sunken);

        verticalLayout_25->addWidget(line_7);

        horizontalLayout_39 = new QHBoxLayout();
        horizontalLayout_39->setSpacing(6);
        horizontalLayout_39->setObjectName(QStringLiteral("horizontalLayout_39"));
        label_60 = new QLabel(groupBox_11);
        label_60->setObjectName(QStringLiteral("label_60"));

        horizontalLayout_39->addWidget(label_60);

        doubleSpinBox_SetOffTime_7 = new QDoubleSpinBox(groupBox_11);
        doubleSpinBox_SetOffTime_7->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_7"));
        doubleSpinBox_SetOffTime_7->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_7->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_7->setFont(font7);
        doubleSpinBox_SetOffTime_7->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_7->setDecimals(1);
        doubleSpinBox_SetOffTime_7->setMaximum(999999);
        doubleSpinBox_SetOffTime_7->setValue(20.8);

        horizontalLayout_39->addWidget(doubleSpinBox_SetOffTime_7);

        label_61 = new QLabel(groupBox_11);
        label_61->setObjectName(QStringLiteral("label_61"));
        label_61->setFont(font5);

        horizontalLayout_39->addWidget(label_61);

        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_39->addItem(horizontalSpacer_30);


        verticalLayout_25->addLayout(horizontalLayout_39);

        horizontalLayout_40 = new QHBoxLayout();
        horizontalLayout_40->setSpacing(6);
        horizontalLayout_40->setObjectName(QStringLiteral("horizontalLayout_40"));
        label_62 = new QLabel(groupBox_11);
        label_62->setObjectName(QStringLiteral("label_62"));
        label_62->setFont(font6);

        horizontalLayout_40->addWidget(label_62);

        doubleSpinBox_SetOffTimeError_7 = new QDoubleSpinBox(groupBox_11);
        doubleSpinBox_SetOffTimeError_7->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_7"));
        doubleSpinBox_SetOffTimeError_7->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_7->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_7->setFont(font4);
        doubleSpinBox_SetOffTimeError_7->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_7->setDecimals(1);
        doubleSpinBox_SetOffTimeError_7->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_7->setValue(1.8);

        horizontalLayout_40->addWidget(doubleSpinBox_SetOffTimeError_7);

        label_63 = new QLabel(groupBox_11);
        label_63->setObjectName(QStringLiteral("label_63"));
        label_63->setFont(font5);

        horizontalLayout_40->addWidget(label_63);

        horizontalSpacer_31 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_40->addItem(horizontalSpacer_31);


        verticalLayout_25->addLayout(horizontalLayout_40);


        verticalLayout_26->addLayout(verticalLayout_25);


        horizontalLayout_45->addWidget(groupBox_11);

        groupBox_10 = new QGroupBox(tab_6);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        groupBox_10->setAlignment(Qt::AlignCenter);
        verticalLayout_24 = new QVBoxLayout(groupBox_10);
        verticalLayout_24->setSpacing(6);
        verticalLayout_24->setContentsMargins(11, 11, 11, 11);
        verticalLayout_24->setObjectName(QStringLiteral("verticalLayout_24"));
        verticalLayout_27 = new QVBoxLayout();
        verticalLayout_27->setSpacing(6);
        verticalLayout_27->setObjectName(QStringLiteral("verticalLayout_27"));
        horizontalLayout_41 = new QHBoxLayout();
        horizontalLayout_41->setSpacing(6);
        horizontalLayout_41->setObjectName(QStringLiteral("horizontalLayout_41"));
        label_64 = new QLabel(groupBox_10);
        label_64->setObjectName(QStringLiteral("label_64"));

        horizontalLayout_41->addWidget(label_64);

        doubleSpinBox_SetOnTime_8 = new QDoubleSpinBox(groupBox_10);
        doubleSpinBox_SetOnTime_8->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_8"));
        doubleSpinBox_SetOnTime_8->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_8->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_8->setFont(font4);
        doubleSpinBox_SetOnTime_8->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_8->setDecimals(1);
        doubleSpinBox_SetOnTime_8->setMaximum(999999);
        doubleSpinBox_SetOnTime_8->setValue(4.2);

        horizontalLayout_41->addWidget(doubleSpinBox_SetOnTime_8);

        label_65 = new QLabel(groupBox_10);
        label_65->setObjectName(QStringLiteral("label_65"));
        label_65->setFont(font5);

        horizontalLayout_41->addWidget(label_65);

        horizontalSpacer_32 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_32);


        verticalLayout_27->addLayout(horizontalLayout_41);

        horizontalLayout_42 = new QHBoxLayout();
        horizontalLayout_42->setSpacing(6);
        horizontalLayout_42->setObjectName(QStringLiteral("horizontalLayout_42"));
        label_66 = new QLabel(groupBox_10);
        label_66->setObjectName(QStringLiteral("label_66"));
        label_66->setFont(font6);

        horizontalLayout_42->addWidget(label_66);

        doubleSpinBox_SetOnTimeError_8 = new QDoubleSpinBox(groupBox_10);
        doubleSpinBox_SetOnTimeError_8->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_8"));
        doubleSpinBox_SetOnTimeError_8->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_8->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_8->setFont(font4);
        doubleSpinBox_SetOnTimeError_8->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_8->setDecimals(1);
        doubleSpinBox_SetOnTimeError_8->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_8->setValue(1.8);

        horizontalLayout_42->addWidget(doubleSpinBox_SetOnTimeError_8);

        label_67 = new QLabel(groupBox_10);
        label_67->setObjectName(QStringLiteral("label_67"));
        label_67->setFont(font5);

        horizontalLayout_42->addWidget(label_67);

        horizontalSpacer_33 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_42->addItem(horizontalSpacer_33);


        verticalLayout_27->addLayout(horizontalLayout_42);

        line_8 = new QFrame(groupBox_10);
        line_8->setObjectName(QStringLiteral("line_8"));
        line_8->setFrameShape(QFrame::HLine);
        line_8->setFrameShadow(QFrame::Sunken);

        verticalLayout_27->addWidget(line_8);

        horizontalLayout_43 = new QHBoxLayout();
        horizontalLayout_43->setSpacing(6);
        horizontalLayout_43->setObjectName(QStringLiteral("horizontalLayout_43"));
        label_68 = new QLabel(groupBox_10);
        label_68->setObjectName(QStringLiteral("label_68"));

        horizontalLayout_43->addWidget(label_68);

        doubleSpinBox_SetOffTime_8 = new QDoubleSpinBox(groupBox_10);
        doubleSpinBox_SetOffTime_8->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_8"));
        doubleSpinBox_SetOffTime_8->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_8->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_8->setFont(font7);
        doubleSpinBox_SetOffTime_8->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_8->setDecimals(1);
        doubleSpinBox_SetOffTime_8->setMaximum(999999);
        doubleSpinBox_SetOffTime_8->setValue(20.8);

        horizontalLayout_43->addWidget(doubleSpinBox_SetOffTime_8);

        label_69 = new QLabel(groupBox_10);
        label_69->setObjectName(QStringLiteral("label_69"));
        label_69->setFont(font5);

        horizontalLayout_43->addWidget(label_69);

        horizontalSpacer_34 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_34);


        verticalLayout_27->addLayout(horizontalLayout_43);

        horizontalLayout_44 = new QHBoxLayout();
        horizontalLayout_44->setSpacing(6);
        horizontalLayout_44->setObjectName(QStringLiteral("horizontalLayout_44"));
        label_70 = new QLabel(groupBox_10);
        label_70->setObjectName(QStringLiteral("label_70"));
        label_70->setFont(font6);

        horizontalLayout_44->addWidget(label_70);

        doubleSpinBox_SetOffTimeError_8 = new QDoubleSpinBox(groupBox_10);
        doubleSpinBox_SetOffTimeError_8->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_8"));
        doubleSpinBox_SetOffTimeError_8->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_8->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_8->setFont(font4);
        doubleSpinBox_SetOffTimeError_8->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_8->setDecimals(1);
        doubleSpinBox_SetOffTimeError_8->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_8->setValue(1.8);

        horizontalLayout_44->addWidget(doubleSpinBox_SetOffTimeError_8);

        label_71 = new QLabel(groupBox_10);
        label_71->setObjectName(QStringLiteral("label_71"));
        label_71->setFont(font5);

        horizontalLayout_44->addWidget(label_71);

        horizontalSpacer_35 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_44->addItem(horizontalSpacer_35);


        verticalLayout_27->addLayout(horizontalLayout_44);


        verticalLayout_24->addLayout(verticalLayout_27);


        horizontalLayout_45->addWidget(groupBox_10);

        tabWidget_2->addTab(tab_6, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QStringLiteral("tab_7"));
        horizontalLayout_54 = new QHBoxLayout(tab_7);
        horizontalLayout_54->setSpacing(6);
        horizontalLayout_54->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_54->setObjectName(QStringLiteral("horizontalLayout_54"));
        groupBox_13 = new QGroupBox(tab_7);
        groupBox_13->setObjectName(QStringLiteral("groupBox_13"));
        groupBox_13->setAlignment(Qt::AlignCenter);
        verticalLayout_30 = new QVBoxLayout(groupBox_13);
        verticalLayout_30->setSpacing(6);
        verticalLayout_30->setContentsMargins(11, 11, 11, 11);
        verticalLayout_30->setObjectName(QStringLiteral("verticalLayout_30"));
        verticalLayout_29 = new QVBoxLayout();
        verticalLayout_29->setSpacing(6);
        verticalLayout_29->setObjectName(QStringLiteral("verticalLayout_29"));
        horizontalLayout_46 = new QHBoxLayout();
        horizontalLayout_46->setSpacing(6);
        horizontalLayout_46->setObjectName(QStringLiteral("horizontalLayout_46"));
        label_72 = new QLabel(groupBox_13);
        label_72->setObjectName(QStringLiteral("label_72"));

        horizontalLayout_46->addWidget(label_72);

        doubleSpinBox_SetOnTime_9 = new QDoubleSpinBox(groupBox_13);
        doubleSpinBox_SetOnTime_9->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_9"));
        doubleSpinBox_SetOnTime_9->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_9->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_9->setFont(font4);
        doubleSpinBox_SetOnTime_9->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_9->setDecimals(1);
        doubleSpinBox_SetOnTime_9->setMaximum(999999);
        doubleSpinBox_SetOnTime_9->setValue(4.2);

        horizontalLayout_46->addWidget(doubleSpinBox_SetOnTime_9);

        label_73 = new QLabel(groupBox_13);
        label_73->setObjectName(QStringLiteral("label_73"));
        label_73->setFont(font5);

        horizontalLayout_46->addWidget(label_73);

        horizontalSpacer_36 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_46->addItem(horizontalSpacer_36);


        verticalLayout_29->addLayout(horizontalLayout_46);

        horizontalLayout_47 = new QHBoxLayout();
        horizontalLayout_47->setSpacing(6);
        horizontalLayout_47->setObjectName(QStringLiteral("horizontalLayout_47"));
        label_74 = new QLabel(groupBox_13);
        label_74->setObjectName(QStringLiteral("label_74"));
        label_74->setFont(font6);

        horizontalLayout_47->addWidget(label_74);

        doubleSpinBox_SetOnTimeError_9 = new QDoubleSpinBox(groupBox_13);
        doubleSpinBox_SetOnTimeError_9->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_9"));
        doubleSpinBox_SetOnTimeError_9->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_9->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_9->setFont(font4);
        doubleSpinBox_SetOnTimeError_9->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_9->setDecimals(1);
        doubleSpinBox_SetOnTimeError_9->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_9->setValue(1.8);

        horizontalLayout_47->addWidget(doubleSpinBox_SetOnTimeError_9);

        label_75 = new QLabel(groupBox_13);
        label_75->setObjectName(QStringLiteral("label_75"));
        label_75->setFont(font5);

        horizontalLayout_47->addWidget(label_75);

        horizontalSpacer_37 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_47->addItem(horizontalSpacer_37);


        verticalLayout_29->addLayout(horizontalLayout_47);

        line_9 = new QFrame(groupBox_13);
        line_9->setObjectName(QStringLiteral("line_9"));
        line_9->setFrameShape(QFrame::HLine);
        line_9->setFrameShadow(QFrame::Sunken);

        verticalLayout_29->addWidget(line_9);

        horizontalLayout_48 = new QHBoxLayout();
        horizontalLayout_48->setSpacing(6);
        horizontalLayout_48->setObjectName(QStringLiteral("horizontalLayout_48"));
        label_76 = new QLabel(groupBox_13);
        label_76->setObjectName(QStringLiteral("label_76"));

        horizontalLayout_48->addWidget(label_76);

        doubleSpinBox_SetOffTime_9 = new QDoubleSpinBox(groupBox_13);
        doubleSpinBox_SetOffTime_9->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_9"));
        doubleSpinBox_SetOffTime_9->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_9->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_9->setFont(font7);
        doubleSpinBox_SetOffTime_9->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_9->setDecimals(1);
        doubleSpinBox_SetOffTime_9->setMaximum(999999);
        doubleSpinBox_SetOffTime_9->setValue(20.8);

        horizontalLayout_48->addWidget(doubleSpinBox_SetOffTime_9);

        label_77 = new QLabel(groupBox_13);
        label_77->setObjectName(QStringLiteral("label_77"));
        label_77->setFont(font5);

        horizontalLayout_48->addWidget(label_77);

        horizontalSpacer_38 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_48->addItem(horizontalSpacer_38);


        verticalLayout_29->addLayout(horizontalLayout_48);

        horizontalLayout_49 = new QHBoxLayout();
        horizontalLayout_49->setSpacing(6);
        horizontalLayout_49->setObjectName(QStringLiteral("horizontalLayout_49"));
        label_78 = new QLabel(groupBox_13);
        label_78->setObjectName(QStringLiteral("label_78"));
        label_78->setFont(font6);

        horizontalLayout_49->addWidget(label_78);

        doubleSpinBox_SetOffTimeError_9 = new QDoubleSpinBox(groupBox_13);
        doubleSpinBox_SetOffTimeError_9->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_9"));
        doubleSpinBox_SetOffTimeError_9->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_9->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_9->setFont(font4);
        doubleSpinBox_SetOffTimeError_9->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_9->setDecimals(1);
        doubleSpinBox_SetOffTimeError_9->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_9->setValue(1.8);

        horizontalLayout_49->addWidget(doubleSpinBox_SetOffTimeError_9);

        label_79 = new QLabel(groupBox_13);
        label_79->setObjectName(QStringLiteral("label_79"));
        label_79->setFont(font5);

        horizontalLayout_49->addWidget(label_79);

        horizontalSpacer_39 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_49->addItem(horizontalSpacer_39);


        verticalLayout_29->addLayout(horizontalLayout_49);


        verticalLayout_30->addLayout(verticalLayout_29);


        horizontalLayout_54->addWidget(groupBox_13);

        groupBox_12 = new QGroupBox(tab_7);
        groupBox_12->setObjectName(QStringLiteral("groupBox_12"));
        groupBox_12->setAlignment(Qt::AlignCenter);
        verticalLayout_28 = new QVBoxLayout(groupBox_12);
        verticalLayout_28->setSpacing(6);
        verticalLayout_28->setContentsMargins(11, 11, 11, 11);
        verticalLayout_28->setObjectName(QStringLiteral("verticalLayout_28"));
        verticalLayout_31 = new QVBoxLayout();
        verticalLayout_31->setSpacing(6);
        verticalLayout_31->setObjectName(QStringLiteral("verticalLayout_31"));
        horizontalLayout_50 = new QHBoxLayout();
        horizontalLayout_50->setSpacing(6);
        horizontalLayout_50->setObjectName(QStringLiteral("horizontalLayout_50"));
        label_80 = new QLabel(groupBox_12);
        label_80->setObjectName(QStringLiteral("label_80"));

        horizontalLayout_50->addWidget(label_80);

        doubleSpinBox_SetOnTime_10 = new QDoubleSpinBox(groupBox_12);
        doubleSpinBox_SetOnTime_10->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_10"));
        doubleSpinBox_SetOnTime_10->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_10->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_10->setFont(font4);
        doubleSpinBox_SetOnTime_10->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_10->setDecimals(1);
        doubleSpinBox_SetOnTime_10->setMaximum(999999);
        doubleSpinBox_SetOnTime_10->setValue(4.2);

        horizontalLayout_50->addWidget(doubleSpinBox_SetOnTime_10);

        label_81 = new QLabel(groupBox_12);
        label_81->setObjectName(QStringLiteral("label_81"));
        label_81->setFont(font5);

        horizontalLayout_50->addWidget(label_81);

        horizontalSpacer_40 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_50->addItem(horizontalSpacer_40);


        verticalLayout_31->addLayout(horizontalLayout_50);

        horizontalLayout_51 = new QHBoxLayout();
        horizontalLayout_51->setSpacing(6);
        horizontalLayout_51->setObjectName(QStringLiteral("horizontalLayout_51"));
        label_82 = new QLabel(groupBox_12);
        label_82->setObjectName(QStringLiteral("label_82"));
        label_82->setFont(font6);

        horizontalLayout_51->addWidget(label_82);

        doubleSpinBox_SetOnTimeError_10 = new QDoubleSpinBox(groupBox_12);
        doubleSpinBox_SetOnTimeError_10->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_10"));
        doubleSpinBox_SetOnTimeError_10->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_10->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_10->setFont(font4);
        doubleSpinBox_SetOnTimeError_10->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_10->setDecimals(1);
        doubleSpinBox_SetOnTimeError_10->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_10->setValue(1.8);

        horizontalLayout_51->addWidget(doubleSpinBox_SetOnTimeError_10);

        label_83 = new QLabel(groupBox_12);
        label_83->setObjectName(QStringLiteral("label_83"));
        label_83->setFont(font5);

        horizontalLayout_51->addWidget(label_83);

        horizontalSpacer_41 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_51->addItem(horizontalSpacer_41);


        verticalLayout_31->addLayout(horizontalLayout_51);

        line_10 = new QFrame(groupBox_12);
        line_10->setObjectName(QStringLiteral("line_10"));
        line_10->setFrameShape(QFrame::HLine);
        line_10->setFrameShadow(QFrame::Sunken);

        verticalLayout_31->addWidget(line_10);

        horizontalLayout_52 = new QHBoxLayout();
        horizontalLayout_52->setSpacing(6);
        horizontalLayout_52->setObjectName(QStringLiteral("horizontalLayout_52"));
        label_84 = new QLabel(groupBox_12);
        label_84->setObjectName(QStringLiteral("label_84"));

        horizontalLayout_52->addWidget(label_84);

        doubleSpinBox_SetOffTime_10 = new QDoubleSpinBox(groupBox_12);
        doubleSpinBox_SetOffTime_10->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_10"));
        doubleSpinBox_SetOffTime_10->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_10->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_10->setFont(font7);
        doubleSpinBox_SetOffTime_10->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_10->setDecimals(1);
        doubleSpinBox_SetOffTime_10->setMaximum(999999);
        doubleSpinBox_SetOffTime_10->setValue(20.8);

        horizontalLayout_52->addWidget(doubleSpinBox_SetOffTime_10);

        label_85 = new QLabel(groupBox_12);
        label_85->setObjectName(QStringLiteral("label_85"));
        label_85->setFont(font5);

        horizontalLayout_52->addWidget(label_85);

        horizontalSpacer_42 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_52->addItem(horizontalSpacer_42);


        verticalLayout_31->addLayout(horizontalLayout_52);

        horizontalLayout_53 = new QHBoxLayout();
        horizontalLayout_53->setSpacing(6);
        horizontalLayout_53->setObjectName(QStringLiteral("horizontalLayout_53"));
        label_86 = new QLabel(groupBox_12);
        label_86->setObjectName(QStringLiteral("label_86"));
        label_86->setFont(font6);

        horizontalLayout_53->addWidget(label_86);

        doubleSpinBox_SetOffTimeError_10 = new QDoubleSpinBox(groupBox_12);
        doubleSpinBox_SetOffTimeError_10->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_10"));
        doubleSpinBox_SetOffTimeError_10->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_10->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_10->setFont(font4);
        doubleSpinBox_SetOffTimeError_10->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_10->setDecimals(1);
        doubleSpinBox_SetOffTimeError_10->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_10->setValue(1.8);

        horizontalLayout_53->addWidget(doubleSpinBox_SetOffTimeError_10);

        label_87 = new QLabel(groupBox_12);
        label_87->setObjectName(QStringLiteral("label_87"));
        label_87->setFont(font5);

        horizontalLayout_53->addWidget(label_87);

        horizontalSpacer_43 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_53->addItem(horizontalSpacer_43);


        verticalLayout_31->addLayout(horizontalLayout_53);


        verticalLayout_28->addLayout(verticalLayout_31);


        horizontalLayout_54->addWidget(groupBox_12);

        tabWidget_2->addTab(tab_7, QString());
        tab_8 = new QWidget();
        tab_8->setObjectName(QStringLiteral("tab_8"));
        horizontalLayout_63 = new QHBoxLayout(tab_8);
        horizontalLayout_63->setSpacing(6);
        horizontalLayout_63->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_63->setObjectName(QStringLiteral("horizontalLayout_63"));
        groupBox_15 = new QGroupBox(tab_8);
        groupBox_15->setObjectName(QStringLiteral("groupBox_15"));
        groupBox_15->setAlignment(Qt::AlignCenter);
        verticalLayout_34 = new QVBoxLayout(groupBox_15);
        verticalLayout_34->setSpacing(6);
        verticalLayout_34->setContentsMargins(11, 11, 11, 11);
        verticalLayout_34->setObjectName(QStringLiteral("verticalLayout_34"));
        verticalLayout_33 = new QVBoxLayout();
        verticalLayout_33->setSpacing(6);
        verticalLayout_33->setObjectName(QStringLiteral("verticalLayout_33"));
        horizontalLayout_55 = new QHBoxLayout();
        horizontalLayout_55->setSpacing(6);
        horizontalLayout_55->setObjectName(QStringLiteral("horizontalLayout_55"));
        label_88 = new QLabel(groupBox_15);
        label_88->setObjectName(QStringLiteral("label_88"));

        horizontalLayout_55->addWidget(label_88);

        doubleSpinBox_SetOnTime_11 = new QDoubleSpinBox(groupBox_15);
        doubleSpinBox_SetOnTime_11->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_11"));
        doubleSpinBox_SetOnTime_11->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_11->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_11->setFont(font4);
        doubleSpinBox_SetOnTime_11->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_11->setDecimals(1);
        doubleSpinBox_SetOnTime_11->setMaximum(999999);
        doubleSpinBox_SetOnTime_11->setValue(4.2);

        horizontalLayout_55->addWidget(doubleSpinBox_SetOnTime_11);

        label_89 = new QLabel(groupBox_15);
        label_89->setObjectName(QStringLiteral("label_89"));
        label_89->setFont(font5);

        horizontalLayout_55->addWidget(label_89);

        horizontalSpacer_44 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_55->addItem(horizontalSpacer_44);


        verticalLayout_33->addLayout(horizontalLayout_55);

        horizontalLayout_56 = new QHBoxLayout();
        horizontalLayout_56->setSpacing(6);
        horizontalLayout_56->setObjectName(QStringLiteral("horizontalLayout_56"));
        label_90 = new QLabel(groupBox_15);
        label_90->setObjectName(QStringLiteral("label_90"));
        label_90->setFont(font6);

        horizontalLayout_56->addWidget(label_90);

        doubleSpinBox_SetOnTimeError_11 = new QDoubleSpinBox(groupBox_15);
        doubleSpinBox_SetOnTimeError_11->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_11"));
        doubleSpinBox_SetOnTimeError_11->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_11->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_11->setFont(font4);
        doubleSpinBox_SetOnTimeError_11->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_11->setDecimals(1);
        doubleSpinBox_SetOnTimeError_11->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_11->setValue(1.8);

        horizontalLayout_56->addWidget(doubleSpinBox_SetOnTimeError_11);

        label_91 = new QLabel(groupBox_15);
        label_91->setObjectName(QStringLiteral("label_91"));
        label_91->setFont(font5);

        horizontalLayout_56->addWidget(label_91);

        horizontalSpacer_45 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_56->addItem(horizontalSpacer_45);


        verticalLayout_33->addLayout(horizontalLayout_56);

        line_11 = new QFrame(groupBox_15);
        line_11->setObjectName(QStringLiteral("line_11"));
        line_11->setFrameShape(QFrame::HLine);
        line_11->setFrameShadow(QFrame::Sunken);

        verticalLayout_33->addWidget(line_11);

        horizontalLayout_57 = new QHBoxLayout();
        horizontalLayout_57->setSpacing(6);
        horizontalLayout_57->setObjectName(QStringLiteral("horizontalLayout_57"));
        label_92 = new QLabel(groupBox_15);
        label_92->setObjectName(QStringLiteral("label_92"));

        horizontalLayout_57->addWidget(label_92);

        doubleSpinBox_SetOffTime_11 = new QDoubleSpinBox(groupBox_15);
        doubleSpinBox_SetOffTime_11->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_11"));
        doubleSpinBox_SetOffTime_11->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_11->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_11->setFont(font7);
        doubleSpinBox_SetOffTime_11->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_11->setDecimals(1);
        doubleSpinBox_SetOffTime_11->setMaximum(999999);
        doubleSpinBox_SetOffTime_11->setValue(20.8);

        horizontalLayout_57->addWidget(doubleSpinBox_SetOffTime_11);

        label_93 = new QLabel(groupBox_15);
        label_93->setObjectName(QStringLiteral("label_93"));
        label_93->setFont(font5);

        horizontalLayout_57->addWidget(label_93);

        horizontalSpacer_46 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_57->addItem(horizontalSpacer_46);


        verticalLayout_33->addLayout(horizontalLayout_57);

        horizontalLayout_58 = new QHBoxLayout();
        horizontalLayout_58->setSpacing(6);
        horizontalLayout_58->setObjectName(QStringLiteral("horizontalLayout_58"));
        label_94 = new QLabel(groupBox_15);
        label_94->setObjectName(QStringLiteral("label_94"));
        label_94->setFont(font6);

        horizontalLayout_58->addWidget(label_94);

        doubleSpinBox_SetOffTimeError_11 = new QDoubleSpinBox(groupBox_15);
        doubleSpinBox_SetOffTimeError_11->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_11"));
        doubleSpinBox_SetOffTimeError_11->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_11->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_11->setFont(font4);
        doubleSpinBox_SetOffTimeError_11->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_11->setDecimals(1);
        doubleSpinBox_SetOffTimeError_11->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_11->setValue(1.8);

        horizontalLayout_58->addWidget(doubleSpinBox_SetOffTimeError_11);

        label_95 = new QLabel(groupBox_15);
        label_95->setObjectName(QStringLiteral("label_95"));
        label_95->setFont(font5);

        horizontalLayout_58->addWidget(label_95);

        horizontalSpacer_47 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_58->addItem(horizontalSpacer_47);


        verticalLayout_33->addLayout(horizontalLayout_58);


        verticalLayout_34->addLayout(verticalLayout_33);


        horizontalLayout_63->addWidget(groupBox_15);

        groupBox_14 = new QGroupBox(tab_8);
        groupBox_14->setObjectName(QStringLiteral("groupBox_14"));
        groupBox_14->setAlignment(Qt::AlignCenter);
        verticalLayout_32 = new QVBoxLayout(groupBox_14);
        verticalLayout_32->setSpacing(6);
        verticalLayout_32->setContentsMargins(11, 11, 11, 11);
        verticalLayout_32->setObjectName(QStringLiteral("verticalLayout_32"));
        verticalLayout_35 = new QVBoxLayout();
        verticalLayout_35->setSpacing(6);
        verticalLayout_35->setObjectName(QStringLiteral("verticalLayout_35"));
        horizontalLayout_59 = new QHBoxLayout();
        horizontalLayout_59->setSpacing(6);
        horizontalLayout_59->setObjectName(QStringLiteral("horizontalLayout_59"));
        label_96 = new QLabel(groupBox_14);
        label_96->setObjectName(QStringLiteral("label_96"));

        horizontalLayout_59->addWidget(label_96);

        doubleSpinBox_SetOnTime_12 = new QDoubleSpinBox(groupBox_14);
        doubleSpinBox_SetOnTime_12->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_12"));
        doubleSpinBox_SetOnTime_12->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_12->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_12->setFont(font4);
        doubleSpinBox_SetOnTime_12->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_12->setDecimals(1);
        doubleSpinBox_SetOnTime_12->setMaximum(999999);
        doubleSpinBox_SetOnTime_12->setValue(4.2);

        horizontalLayout_59->addWidget(doubleSpinBox_SetOnTime_12);

        label_97 = new QLabel(groupBox_14);
        label_97->setObjectName(QStringLiteral("label_97"));
        label_97->setFont(font5);

        horizontalLayout_59->addWidget(label_97);

        horizontalSpacer_48 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_59->addItem(horizontalSpacer_48);


        verticalLayout_35->addLayout(horizontalLayout_59);

        horizontalLayout_60 = new QHBoxLayout();
        horizontalLayout_60->setSpacing(6);
        horizontalLayout_60->setObjectName(QStringLiteral("horizontalLayout_60"));
        label_98 = new QLabel(groupBox_14);
        label_98->setObjectName(QStringLiteral("label_98"));
        label_98->setFont(font6);

        horizontalLayout_60->addWidget(label_98);

        doubleSpinBox_SetOnTimeError_12 = new QDoubleSpinBox(groupBox_14);
        doubleSpinBox_SetOnTimeError_12->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_12"));
        doubleSpinBox_SetOnTimeError_12->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_12->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_12->setFont(font4);
        doubleSpinBox_SetOnTimeError_12->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_12->setDecimals(1);
        doubleSpinBox_SetOnTimeError_12->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_12->setValue(1.8);

        horizontalLayout_60->addWidget(doubleSpinBox_SetOnTimeError_12);

        label_99 = new QLabel(groupBox_14);
        label_99->setObjectName(QStringLiteral("label_99"));
        label_99->setFont(font5);

        horizontalLayout_60->addWidget(label_99);

        horizontalSpacer_49 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_60->addItem(horizontalSpacer_49);


        verticalLayout_35->addLayout(horizontalLayout_60);

        line_12 = new QFrame(groupBox_14);
        line_12->setObjectName(QStringLiteral("line_12"));
        line_12->setFrameShape(QFrame::HLine);
        line_12->setFrameShadow(QFrame::Sunken);

        verticalLayout_35->addWidget(line_12);

        horizontalLayout_61 = new QHBoxLayout();
        horizontalLayout_61->setSpacing(6);
        horizontalLayout_61->setObjectName(QStringLiteral("horizontalLayout_61"));
        label_100 = new QLabel(groupBox_14);
        label_100->setObjectName(QStringLiteral("label_100"));

        horizontalLayout_61->addWidget(label_100);

        doubleSpinBox_SetOffTime_12 = new QDoubleSpinBox(groupBox_14);
        doubleSpinBox_SetOffTime_12->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_12"));
        doubleSpinBox_SetOffTime_12->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_12->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_12->setFont(font7);
        doubleSpinBox_SetOffTime_12->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_12->setDecimals(1);
        doubleSpinBox_SetOffTime_12->setMaximum(999999);
        doubleSpinBox_SetOffTime_12->setValue(20.8);

        horizontalLayout_61->addWidget(doubleSpinBox_SetOffTime_12);

        label_101 = new QLabel(groupBox_14);
        label_101->setObjectName(QStringLiteral("label_101"));
        label_101->setFont(font5);

        horizontalLayout_61->addWidget(label_101);

        horizontalSpacer_50 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_61->addItem(horizontalSpacer_50);


        verticalLayout_35->addLayout(horizontalLayout_61);

        horizontalLayout_62 = new QHBoxLayout();
        horizontalLayout_62->setSpacing(6);
        horizontalLayout_62->setObjectName(QStringLiteral("horizontalLayout_62"));
        label_102 = new QLabel(groupBox_14);
        label_102->setObjectName(QStringLiteral("label_102"));
        label_102->setFont(font6);

        horizontalLayout_62->addWidget(label_102);

        doubleSpinBox_SetOffTimeError_12 = new QDoubleSpinBox(groupBox_14);
        doubleSpinBox_SetOffTimeError_12->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_12"));
        doubleSpinBox_SetOffTimeError_12->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_12->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_12->setFont(font4);
        doubleSpinBox_SetOffTimeError_12->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_12->setDecimals(1);
        doubleSpinBox_SetOffTimeError_12->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_12->setValue(1.8);

        horizontalLayout_62->addWidget(doubleSpinBox_SetOffTimeError_12);

        label_103 = new QLabel(groupBox_14);
        label_103->setObjectName(QStringLiteral("label_103"));
        label_103->setFont(font5);

        horizontalLayout_62->addWidget(label_103);

        horizontalSpacer_51 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_62->addItem(horizontalSpacer_51);


        verticalLayout_35->addLayout(horizontalLayout_62);


        verticalLayout_32->addLayout(verticalLayout_35);


        horizontalLayout_63->addWidget(groupBox_14);

        tabWidget_2->addTab(tab_8, QString());
        tab_9 = new QWidget();
        tab_9->setObjectName(QStringLiteral("tab_9"));
        horizontalLayout_72 = new QHBoxLayout(tab_9);
        horizontalLayout_72->setSpacing(6);
        horizontalLayout_72->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_72->setObjectName(QStringLiteral("horizontalLayout_72"));
        groupBox_17 = new QGroupBox(tab_9);
        groupBox_17->setObjectName(QStringLiteral("groupBox_17"));
        groupBox_17->setAlignment(Qt::AlignCenter);
        verticalLayout_38 = new QVBoxLayout(groupBox_17);
        verticalLayout_38->setSpacing(6);
        verticalLayout_38->setContentsMargins(11, 11, 11, 11);
        verticalLayout_38->setObjectName(QStringLiteral("verticalLayout_38"));
        verticalLayout_37 = new QVBoxLayout();
        verticalLayout_37->setSpacing(6);
        verticalLayout_37->setObjectName(QStringLiteral("verticalLayout_37"));
        horizontalLayout_64 = new QHBoxLayout();
        horizontalLayout_64->setSpacing(6);
        horizontalLayout_64->setObjectName(QStringLiteral("horizontalLayout_64"));
        label_104 = new QLabel(groupBox_17);
        label_104->setObjectName(QStringLiteral("label_104"));

        horizontalLayout_64->addWidget(label_104);

        doubleSpinBox_SetOnTime_13 = new QDoubleSpinBox(groupBox_17);
        doubleSpinBox_SetOnTime_13->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_13"));
        doubleSpinBox_SetOnTime_13->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_13->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_13->setFont(font4);
        doubleSpinBox_SetOnTime_13->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_13->setDecimals(1);
        doubleSpinBox_SetOnTime_13->setMaximum(999999);
        doubleSpinBox_SetOnTime_13->setValue(4.2);

        horizontalLayout_64->addWidget(doubleSpinBox_SetOnTime_13);

        label_105 = new QLabel(groupBox_17);
        label_105->setObjectName(QStringLiteral("label_105"));
        label_105->setFont(font5);

        horizontalLayout_64->addWidget(label_105);

        horizontalSpacer_52 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_64->addItem(horizontalSpacer_52);


        verticalLayout_37->addLayout(horizontalLayout_64);

        horizontalLayout_65 = new QHBoxLayout();
        horizontalLayout_65->setSpacing(6);
        horizontalLayout_65->setObjectName(QStringLiteral("horizontalLayout_65"));
        label_106 = new QLabel(groupBox_17);
        label_106->setObjectName(QStringLiteral("label_106"));
        label_106->setFont(font6);

        horizontalLayout_65->addWidget(label_106);

        doubleSpinBox_SetOnTimeError_13 = new QDoubleSpinBox(groupBox_17);
        doubleSpinBox_SetOnTimeError_13->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_13"));
        doubleSpinBox_SetOnTimeError_13->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_13->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_13->setFont(font4);
        doubleSpinBox_SetOnTimeError_13->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_13->setDecimals(1);
        doubleSpinBox_SetOnTimeError_13->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_13->setValue(1.8);

        horizontalLayout_65->addWidget(doubleSpinBox_SetOnTimeError_13);

        label_107 = new QLabel(groupBox_17);
        label_107->setObjectName(QStringLiteral("label_107"));
        label_107->setFont(font5);

        horizontalLayout_65->addWidget(label_107);

        horizontalSpacer_53 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_65->addItem(horizontalSpacer_53);


        verticalLayout_37->addLayout(horizontalLayout_65);

        line_13 = new QFrame(groupBox_17);
        line_13->setObjectName(QStringLiteral("line_13"));
        line_13->setFrameShape(QFrame::HLine);
        line_13->setFrameShadow(QFrame::Sunken);

        verticalLayout_37->addWidget(line_13);

        horizontalLayout_66 = new QHBoxLayout();
        horizontalLayout_66->setSpacing(6);
        horizontalLayout_66->setObjectName(QStringLiteral("horizontalLayout_66"));
        label_108 = new QLabel(groupBox_17);
        label_108->setObjectName(QStringLiteral("label_108"));

        horizontalLayout_66->addWidget(label_108);

        doubleSpinBox_SetOffTime_13 = new QDoubleSpinBox(groupBox_17);
        doubleSpinBox_SetOffTime_13->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_13"));
        doubleSpinBox_SetOffTime_13->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_13->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_13->setFont(font7);
        doubleSpinBox_SetOffTime_13->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_13->setDecimals(1);
        doubleSpinBox_SetOffTime_13->setMaximum(999999);
        doubleSpinBox_SetOffTime_13->setValue(20.8);

        horizontalLayout_66->addWidget(doubleSpinBox_SetOffTime_13);

        label_109 = new QLabel(groupBox_17);
        label_109->setObjectName(QStringLiteral("label_109"));
        label_109->setFont(font5);

        horizontalLayout_66->addWidget(label_109);

        horizontalSpacer_54 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_66->addItem(horizontalSpacer_54);


        verticalLayout_37->addLayout(horizontalLayout_66);

        horizontalLayout_67 = new QHBoxLayout();
        horizontalLayout_67->setSpacing(6);
        horizontalLayout_67->setObjectName(QStringLiteral("horizontalLayout_67"));
        label_110 = new QLabel(groupBox_17);
        label_110->setObjectName(QStringLiteral("label_110"));
        label_110->setFont(font6);

        horizontalLayout_67->addWidget(label_110);

        doubleSpinBox_SetOffTimeError_13 = new QDoubleSpinBox(groupBox_17);
        doubleSpinBox_SetOffTimeError_13->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_13"));
        doubleSpinBox_SetOffTimeError_13->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_13->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_13->setFont(font4);
        doubleSpinBox_SetOffTimeError_13->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_13->setDecimals(1);
        doubleSpinBox_SetOffTimeError_13->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_13->setValue(1.8);

        horizontalLayout_67->addWidget(doubleSpinBox_SetOffTimeError_13);

        label_111 = new QLabel(groupBox_17);
        label_111->setObjectName(QStringLiteral("label_111"));
        label_111->setFont(font5);

        horizontalLayout_67->addWidget(label_111);

        horizontalSpacer_55 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_67->addItem(horizontalSpacer_55);


        verticalLayout_37->addLayout(horizontalLayout_67);


        verticalLayout_38->addLayout(verticalLayout_37);


        horizontalLayout_72->addWidget(groupBox_17);

        groupBox_16 = new QGroupBox(tab_9);
        groupBox_16->setObjectName(QStringLiteral("groupBox_16"));
        groupBox_16->setAlignment(Qt::AlignCenter);
        verticalLayout_36 = new QVBoxLayout(groupBox_16);
        verticalLayout_36->setSpacing(6);
        verticalLayout_36->setContentsMargins(11, 11, 11, 11);
        verticalLayout_36->setObjectName(QStringLiteral("verticalLayout_36"));
        verticalLayout_39 = new QVBoxLayout();
        verticalLayout_39->setSpacing(6);
        verticalLayout_39->setObjectName(QStringLiteral("verticalLayout_39"));
        horizontalLayout_68 = new QHBoxLayout();
        horizontalLayout_68->setSpacing(6);
        horizontalLayout_68->setObjectName(QStringLiteral("horizontalLayout_68"));
        label_112 = new QLabel(groupBox_16);
        label_112->setObjectName(QStringLiteral("label_112"));

        horizontalLayout_68->addWidget(label_112);

        doubleSpinBox_SetOnTime_14 = new QDoubleSpinBox(groupBox_16);
        doubleSpinBox_SetOnTime_14->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_14"));
        doubleSpinBox_SetOnTime_14->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_14->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_14->setFont(font4);
        doubleSpinBox_SetOnTime_14->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_14->setDecimals(1);
        doubleSpinBox_SetOnTime_14->setMaximum(999999);
        doubleSpinBox_SetOnTime_14->setValue(4.2);

        horizontalLayout_68->addWidget(doubleSpinBox_SetOnTime_14);

        label_113 = new QLabel(groupBox_16);
        label_113->setObjectName(QStringLiteral("label_113"));
        label_113->setFont(font5);

        horizontalLayout_68->addWidget(label_113);

        horizontalSpacer_56 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_68->addItem(horizontalSpacer_56);


        verticalLayout_39->addLayout(horizontalLayout_68);

        horizontalLayout_69 = new QHBoxLayout();
        horizontalLayout_69->setSpacing(6);
        horizontalLayout_69->setObjectName(QStringLiteral("horizontalLayout_69"));
        label_114 = new QLabel(groupBox_16);
        label_114->setObjectName(QStringLiteral("label_114"));
        label_114->setFont(font6);

        horizontalLayout_69->addWidget(label_114);

        doubleSpinBox_SetOnTimeError_14 = new QDoubleSpinBox(groupBox_16);
        doubleSpinBox_SetOnTimeError_14->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_14"));
        doubleSpinBox_SetOnTimeError_14->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_14->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_14->setFont(font4);
        doubleSpinBox_SetOnTimeError_14->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_14->setDecimals(1);
        doubleSpinBox_SetOnTimeError_14->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_14->setValue(1.8);

        horizontalLayout_69->addWidget(doubleSpinBox_SetOnTimeError_14);

        label_115 = new QLabel(groupBox_16);
        label_115->setObjectName(QStringLiteral("label_115"));
        label_115->setFont(font5);

        horizontalLayout_69->addWidget(label_115);

        horizontalSpacer_57 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_69->addItem(horizontalSpacer_57);


        verticalLayout_39->addLayout(horizontalLayout_69);

        line_14 = new QFrame(groupBox_16);
        line_14->setObjectName(QStringLiteral("line_14"));
        line_14->setFrameShape(QFrame::HLine);
        line_14->setFrameShadow(QFrame::Sunken);

        verticalLayout_39->addWidget(line_14);

        horizontalLayout_70 = new QHBoxLayout();
        horizontalLayout_70->setSpacing(6);
        horizontalLayout_70->setObjectName(QStringLiteral("horizontalLayout_70"));
        label_116 = new QLabel(groupBox_16);
        label_116->setObjectName(QStringLiteral("label_116"));

        horizontalLayout_70->addWidget(label_116);

        doubleSpinBox_SetOffTime_14 = new QDoubleSpinBox(groupBox_16);
        doubleSpinBox_SetOffTime_14->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_14"));
        doubleSpinBox_SetOffTime_14->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_14->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_14->setFont(font7);
        doubleSpinBox_SetOffTime_14->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_14->setDecimals(1);
        doubleSpinBox_SetOffTime_14->setMaximum(999999);
        doubleSpinBox_SetOffTime_14->setValue(20.8);

        horizontalLayout_70->addWidget(doubleSpinBox_SetOffTime_14);

        label_117 = new QLabel(groupBox_16);
        label_117->setObjectName(QStringLiteral("label_117"));
        label_117->setFont(font5);

        horizontalLayout_70->addWidget(label_117);

        horizontalSpacer_58 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_70->addItem(horizontalSpacer_58);


        verticalLayout_39->addLayout(horizontalLayout_70);

        horizontalLayout_71 = new QHBoxLayout();
        horizontalLayout_71->setSpacing(6);
        horizontalLayout_71->setObjectName(QStringLiteral("horizontalLayout_71"));
        label_118 = new QLabel(groupBox_16);
        label_118->setObjectName(QStringLiteral("label_118"));
        label_118->setFont(font6);

        horizontalLayout_71->addWidget(label_118);

        doubleSpinBox_SetOffTimeError_14 = new QDoubleSpinBox(groupBox_16);
        doubleSpinBox_SetOffTimeError_14->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_14"));
        doubleSpinBox_SetOffTimeError_14->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_14->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_14->setFont(font4);
        doubleSpinBox_SetOffTimeError_14->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_14->setDecimals(1);
        doubleSpinBox_SetOffTimeError_14->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_14->setValue(1.8);

        horizontalLayout_71->addWidget(doubleSpinBox_SetOffTimeError_14);

        label_119 = new QLabel(groupBox_16);
        label_119->setObjectName(QStringLiteral("label_119"));
        label_119->setFont(font5);

        horizontalLayout_71->addWidget(label_119);

        horizontalSpacer_59 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_71->addItem(horizontalSpacer_59);


        verticalLayout_39->addLayout(horizontalLayout_71);


        verticalLayout_36->addLayout(verticalLayout_39);


        horizontalLayout_72->addWidget(groupBox_16);

        tabWidget_2->addTab(tab_9, QString());
        tab_10 = new QWidget();
        tab_10->setObjectName(QStringLiteral("tab_10"));
        horizontalLayout_81 = new QHBoxLayout(tab_10);
        horizontalLayout_81->setSpacing(6);
        horizontalLayout_81->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_81->setObjectName(QStringLiteral("horizontalLayout_81"));
        groupBox_19 = new QGroupBox(tab_10);
        groupBox_19->setObjectName(QStringLiteral("groupBox_19"));
        groupBox_19->setAlignment(Qt::AlignCenter);
        verticalLayout_43 = new QVBoxLayout(groupBox_19);
        verticalLayout_43->setSpacing(6);
        verticalLayout_43->setContentsMargins(11, 11, 11, 11);
        verticalLayout_43->setObjectName(QStringLiteral("verticalLayout_43"));
        verticalLayout_42 = new QVBoxLayout();
        verticalLayout_42->setSpacing(6);
        verticalLayout_42->setObjectName(QStringLiteral("verticalLayout_42"));
        horizontalLayout_73 = new QHBoxLayout();
        horizontalLayout_73->setSpacing(6);
        horizontalLayout_73->setObjectName(QStringLiteral("horizontalLayout_73"));
        label_120 = new QLabel(groupBox_19);
        label_120->setObjectName(QStringLiteral("label_120"));

        horizontalLayout_73->addWidget(label_120);

        doubleSpinBox_SetOnTime_15 = new QDoubleSpinBox(groupBox_19);
        doubleSpinBox_SetOnTime_15->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_15"));
        doubleSpinBox_SetOnTime_15->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_15->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_15->setFont(font4);
        doubleSpinBox_SetOnTime_15->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_15->setDecimals(1);
        doubleSpinBox_SetOnTime_15->setMaximum(999999);
        doubleSpinBox_SetOnTime_15->setValue(4.2);

        horizontalLayout_73->addWidget(doubleSpinBox_SetOnTime_15);

        label_121 = new QLabel(groupBox_19);
        label_121->setObjectName(QStringLiteral("label_121"));
        label_121->setFont(font5);

        horizontalLayout_73->addWidget(label_121);

        horizontalSpacer_60 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_73->addItem(horizontalSpacer_60);


        verticalLayout_42->addLayout(horizontalLayout_73);

        horizontalLayout_74 = new QHBoxLayout();
        horizontalLayout_74->setSpacing(6);
        horizontalLayout_74->setObjectName(QStringLiteral("horizontalLayout_74"));
        label_122 = new QLabel(groupBox_19);
        label_122->setObjectName(QStringLiteral("label_122"));
        label_122->setFont(font6);

        horizontalLayout_74->addWidget(label_122);

        doubleSpinBox_SetOnTimeError_15 = new QDoubleSpinBox(groupBox_19);
        doubleSpinBox_SetOnTimeError_15->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_15"));
        doubleSpinBox_SetOnTimeError_15->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_15->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_15->setFont(font4);
        doubleSpinBox_SetOnTimeError_15->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_15->setDecimals(1);
        doubleSpinBox_SetOnTimeError_15->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_15->setValue(1.8);

        horizontalLayout_74->addWidget(doubleSpinBox_SetOnTimeError_15);

        label_123 = new QLabel(groupBox_19);
        label_123->setObjectName(QStringLiteral("label_123"));
        label_123->setFont(font5);

        horizontalLayout_74->addWidget(label_123);

        horizontalSpacer_61 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_74->addItem(horizontalSpacer_61);


        verticalLayout_42->addLayout(horizontalLayout_74);

        line_15 = new QFrame(groupBox_19);
        line_15->setObjectName(QStringLiteral("line_15"));
        line_15->setFrameShape(QFrame::HLine);
        line_15->setFrameShadow(QFrame::Sunken);

        verticalLayout_42->addWidget(line_15);

        horizontalLayout_75 = new QHBoxLayout();
        horizontalLayout_75->setSpacing(6);
        horizontalLayout_75->setObjectName(QStringLiteral("horizontalLayout_75"));
        label_124 = new QLabel(groupBox_19);
        label_124->setObjectName(QStringLiteral("label_124"));

        horizontalLayout_75->addWidget(label_124);

        doubleSpinBox_SetOffTime_15 = new QDoubleSpinBox(groupBox_19);
        doubleSpinBox_SetOffTime_15->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_15"));
        doubleSpinBox_SetOffTime_15->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_15->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_15->setFont(font7);
        doubleSpinBox_SetOffTime_15->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_15->setDecimals(1);
        doubleSpinBox_SetOffTime_15->setMaximum(999999);
        doubleSpinBox_SetOffTime_15->setValue(20.8);

        horizontalLayout_75->addWidget(doubleSpinBox_SetOffTime_15);

        label_125 = new QLabel(groupBox_19);
        label_125->setObjectName(QStringLiteral("label_125"));
        label_125->setFont(font5);

        horizontalLayout_75->addWidget(label_125);

        horizontalSpacer_62 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_75->addItem(horizontalSpacer_62);


        verticalLayout_42->addLayout(horizontalLayout_75);

        horizontalLayout_76 = new QHBoxLayout();
        horizontalLayout_76->setSpacing(6);
        horizontalLayout_76->setObjectName(QStringLiteral("horizontalLayout_76"));
        label_126 = new QLabel(groupBox_19);
        label_126->setObjectName(QStringLiteral("label_126"));
        label_126->setFont(font6);

        horizontalLayout_76->addWidget(label_126);

        doubleSpinBox_SetOffTimeError_15 = new QDoubleSpinBox(groupBox_19);
        doubleSpinBox_SetOffTimeError_15->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_15"));
        doubleSpinBox_SetOffTimeError_15->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_15->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_15->setFont(font4);
        doubleSpinBox_SetOffTimeError_15->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_15->setDecimals(1);
        doubleSpinBox_SetOffTimeError_15->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_15->setValue(1.8);

        horizontalLayout_76->addWidget(doubleSpinBox_SetOffTimeError_15);

        label_127 = new QLabel(groupBox_19);
        label_127->setObjectName(QStringLiteral("label_127"));
        label_127->setFont(font5);

        horizontalLayout_76->addWidget(label_127);

        horizontalSpacer_63 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_76->addItem(horizontalSpacer_63);


        verticalLayout_42->addLayout(horizontalLayout_76);


        verticalLayout_43->addLayout(verticalLayout_42);


        horizontalLayout_81->addWidget(groupBox_19);

        groupBox_18 = new QGroupBox(tab_10);
        groupBox_18->setObjectName(QStringLiteral("groupBox_18"));
        groupBox_18->setAlignment(Qt::AlignCenter);
        verticalLayout_41 = new QVBoxLayout(groupBox_18);
        verticalLayout_41->setSpacing(6);
        verticalLayout_41->setContentsMargins(11, 11, 11, 11);
        verticalLayout_41->setObjectName(QStringLiteral("verticalLayout_41"));
        verticalLayout_44 = new QVBoxLayout();
        verticalLayout_44->setSpacing(6);
        verticalLayout_44->setObjectName(QStringLiteral("verticalLayout_44"));
        horizontalLayout_77 = new QHBoxLayout();
        horizontalLayout_77->setSpacing(6);
        horizontalLayout_77->setObjectName(QStringLiteral("horizontalLayout_77"));
        label_128 = new QLabel(groupBox_18);
        label_128->setObjectName(QStringLiteral("label_128"));

        horizontalLayout_77->addWidget(label_128);

        doubleSpinBox_SetOnTime_16 = new QDoubleSpinBox(groupBox_18);
        doubleSpinBox_SetOnTime_16->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_16"));
        doubleSpinBox_SetOnTime_16->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_16->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_16->setFont(font4);
        doubleSpinBox_SetOnTime_16->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_16->setDecimals(1);
        doubleSpinBox_SetOnTime_16->setMaximum(999999);
        doubleSpinBox_SetOnTime_16->setValue(4.2);

        horizontalLayout_77->addWidget(doubleSpinBox_SetOnTime_16);

        label_129 = new QLabel(groupBox_18);
        label_129->setObjectName(QStringLiteral("label_129"));
        label_129->setFont(font5);

        horizontalLayout_77->addWidget(label_129);

        horizontalSpacer_64 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_77->addItem(horizontalSpacer_64);


        verticalLayout_44->addLayout(horizontalLayout_77);

        horizontalLayout_78 = new QHBoxLayout();
        horizontalLayout_78->setSpacing(6);
        horizontalLayout_78->setObjectName(QStringLiteral("horizontalLayout_78"));
        label_130 = new QLabel(groupBox_18);
        label_130->setObjectName(QStringLiteral("label_130"));
        label_130->setFont(font6);

        horizontalLayout_78->addWidget(label_130);

        doubleSpinBox_SetOnTimeError_16 = new QDoubleSpinBox(groupBox_18);
        doubleSpinBox_SetOnTimeError_16->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_16"));
        doubleSpinBox_SetOnTimeError_16->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_16->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_16->setFont(font4);
        doubleSpinBox_SetOnTimeError_16->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_16->setDecimals(1);
        doubleSpinBox_SetOnTimeError_16->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_16->setValue(1.8);

        horizontalLayout_78->addWidget(doubleSpinBox_SetOnTimeError_16);

        label_131 = new QLabel(groupBox_18);
        label_131->setObjectName(QStringLiteral("label_131"));
        label_131->setFont(font5);

        horizontalLayout_78->addWidget(label_131);

        horizontalSpacer_65 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_78->addItem(horizontalSpacer_65);


        verticalLayout_44->addLayout(horizontalLayout_78);

        line_16 = new QFrame(groupBox_18);
        line_16->setObjectName(QStringLiteral("line_16"));
        line_16->setFrameShape(QFrame::HLine);
        line_16->setFrameShadow(QFrame::Sunken);

        verticalLayout_44->addWidget(line_16);

        horizontalLayout_79 = new QHBoxLayout();
        horizontalLayout_79->setSpacing(6);
        horizontalLayout_79->setObjectName(QStringLiteral("horizontalLayout_79"));
        label_132 = new QLabel(groupBox_18);
        label_132->setObjectName(QStringLiteral("label_132"));

        horizontalLayout_79->addWidget(label_132);

        doubleSpinBox_SetOffTime_16 = new QDoubleSpinBox(groupBox_18);
        doubleSpinBox_SetOffTime_16->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_16"));
        doubleSpinBox_SetOffTime_16->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_16->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_16->setFont(font7);
        doubleSpinBox_SetOffTime_16->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_16->setDecimals(1);
        doubleSpinBox_SetOffTime_16->setMaximum(999999);
        doubleSpinBox_SetOffTime_16->setValue(20.8);

        horizontalLayout_79->addWidget(doubleSpinBox_SetOffTime_16);

        label_133 = new QLabel(groupBox_18);
        label_133->setObjectName(QStringLiteral("label_133"));
        label_133->setFont(font5);

        horizontalLayout_79->addWidget(label_133);

        horizontalSpacer_66 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_79->addItem(horizontalSpacer_66);


        verticalLayout_44->addLayout(horizontalLayout_79);

        horizontalLayout_80 = new QHBoxLayout();
        horizontalLayout_80->setSpacing(6);
        horizontalLayout_80->setObjectName(QStringLiteral("horizontalLayout_80"));
        label_134 = new QLabel(groupBox_18);
        label_134->setObjectName(QStringLiteral("label_134"));
        label_134->setFont(font6);

        horizontalLayout_80->addWidget(label_134);

        doubleSpinBox_SetOffTimeError_16 = new QDoubleSpinBox(groupBox_18);
        doubleSpinBox_SetOffTimeError_16->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_16"));
        doubleSpinBox_SetOffTimeError_16->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_16->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_16->setFont(font4);
        doubleSpinBox_SetOffTimeError_16->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_16->setDecimals(1);
        doubleSpinBox_SetOffTimeError_16->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_16->setValue(1.8);

        horizontalLayout_80->addWidget(doubleSpinBox_SetOffTimeError_16);

        label_135 = new QLabel(groupBox_18);
        label_135->setObjectName(QStringLiteral("label_135"));
        label_135->setFont(font5);

        horizontalLayout_80->addWidget(label_135);

        horizontalSpacer_67 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_80->addItem(horizontalSpacer_67);


        verticalLayout_44->addLayout(horizontalLayout_80);


        verticalLayout_41->addLayout(verticalLayout_44);


        horizontalLayout_81->addWidget(groupBox_18);

        tabWidget_2->addTab(tab_10, QString());
        tab_11 = new QWidget();
        tab_11->setObjectName(QStringLiteral("tab_11"));
        horizontalLayout_90 = new QHBoxLayout(tab_11);
        horizontalLayout_90->setSpacing(6);
        horizontalLayout_90->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_90->setObjectName(QStringLiteral("horizontalLayout_90"));
        groupBox_21 = new QGroupBox(tab_11);
        groupBox_21->setObjectName(QStringLiteral("groupBox_21"));
        groupBox_21->setAlignment(Qt::AlignCenter);
        verticalLayout_47 = new QVBoxLayout(groupBox_21);
        verticalLayout_47->setSpacing(6);
        verticalLayout_47->setContentsMargins(11, 11, 11, 11);
        verticalLayout_47->setObjectName(QStringLiteral("verticalLayout_47"));
        verticalLayout_46 = new QVBoxLayout();
        verticalLayout_46->setSpacing(6);
        verticalLayout_46->setObjectName(QStringLiteral("verticalLayout_46"));
        horizontalLayout_82 = new QHBoxLayout();
        horizontalLayout_82->setSpacing(6);
        horizontalLayout_82->setObjectName(QStringLiteral("horizontalLayout_82"));
        label_136 = new QLabel(groupBox_21);
        label_136->setObjectName(QStringLiteral("label_136"));

        horizontalLayout_82->addWidget(label_136);

        doubleSpinBox_SetOnTime_17 = new QDoubleSpinBox(groupBox_21);
        doubleSpinBox_SetOnTime_17->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_17"));
        doubleSpinBox_SetOnTime_17->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_17->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_17->setFont(font4);
        doubleSpinBox_SetOnTime_17->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_17->setDecimals(1);
        doubleSpinBox_SetOnTime_17->setMaximum(999999);
        doubleSpinBox_SetOnTime_17->setValue(4.2);

        horizontalLayout_82->addWidget(doubleSpinBox_SetOnTime_17);

        label_137 = new QLabel(groupBox_21);
        label_137->setObjectName(QStringLiteral("label_137"));
        label_137->setFont(font5);

        horizontalLayout_82->addWidget(label_137);

        horizontalSpacer_68 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_82->addItem(horizontalSpacer_68);


        verticalLayout_46->addLayout(horizontalLayout_82);

        horizontalLayout_83 = new QHBoxLayout();
        horizontalLayout_83->setSpacing(6);
        horizontalLayout_83->setObjectName(QStringLiteral("horizontalLayout_83"));
        label_138 = new QLabel(groupBox_21);
        label_138->setObjectName(QStringLiteral("label_138"));
        label_138->setFont(font6);

        horizontalLayout_83->addWidget(label_138);

        doubleSpinBox_SetOnTimeError_17 = new QDoubleSpinBox(groupBox_21);
        doubleSpinBox_SetOnTimeError_17->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_17"));
        doubleSpinBox_SetOnTimeError_17->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_17->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_17->setFont(font4);
        doubleSpinBox_SetOnTimeError_17->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_17->setDecimals(1);
        doubleSpinBox_SetOnTimeError_17->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_17->setValue(1.8);

        horizontalLayout_83->addWidget(doubleSpinBox_SetOnTimeError_17);

        label_139 = new QLabel(groupBox_21);
        label_139->setObjectName(QStringLiteral("label_139"));
        label_139->setFont(font5);

        horizontalLayout_83->addWidget(label_139);

        horizontalSpacer_69 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_83->addItem(horizontalSpacer_69);


        verticalLayout_46->addLayout(horizontalLayout_83);

        line_17 = new QFrame(groupBox_21);
        line_17->setObjectName(QStringLiteral("line_17"));
        line_17->setFrameShape(QFrame::HLine);
        line_17->setFrameShadow(QFrame::Sunken);

        verticalLayout_46->addWidget(line_17);

        horizontalLayout_84 = new QHBoxLayout();
        horizontalLayout_84->setSpacing(6);
        horizontalLayout_84->setObjectName(QStringLiteral("horizontalLayout_84"));
        label_140 = new QLabel(groupBox_21);
        label_140->setObjectName(QStringLiteral("label_140"));

        horizontalLayout_84->addWidget(label_140);

        doubleSpinBox_SetOffTime_17 = new QDoubleSpinBox(groupBox_21);
        doubleSpinBox_SetOffTime_17->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_17"));
        doubleSpinBox_SetOffTime_17->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_17->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_17->setFont(font7);
        doubleSpinBox_SetOffTime_17->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_17->setDecimals(1);
        doubleSpinBox_SetOffTime_17->setMaximum(999999);
        doubleSpinBox_SetOffTime_17->setValue(20.8);

        horizontalLayout_84->addWidget(doubleSpinBox_SetOffTime_17);

        label_141 = new QLabel(groupBox_21);
        label_141->setObjectName(QStringLiteral("label_141"));
        label_141->setFont(font5);

        horizontalLayout_84->addWidget(label_141);

        horizontalSpacer_70 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_84->addItem(horizontalSpacer_70);


        verticalLayout_46->addLayout(horizontalLayout_84);

        horizontalLayout_85 = new QHBoxLayout();
        horizontalLayout_85->setSpacing(6);
        horizontalLayout_85->setObjectName(QStringLiteral("horizontalLayout_85"));
        label_142 = new QLabel(groupBox_21);
        label_142->setObjectName(QStringLiteral("label_142"));
        label_142->setFont(font6);

        horizontalLayout_85->addWidget(label_142);

        doubleSpinBox_SetOffTimeError_17 = new QDoubleSpinBox(groupBox_21);
        doubleSpinBox_SetOffTimeError_17->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_17"));
        doubleSpinBox_SetOffTimeError_17->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_17->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_17->setFont(font4);
        doubleSpinBox_SetOffTimeError_17->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_17->setDecimals(1);
        doubleSpinBox_SetOffTimeError_17->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_17->setValue(1.8);

        horizontalLayout_85->addWidget(doubleSpinBox_SetOffTimeError_17);

        label_143 = new QLabel(groupBox_21);
        label_143->setObjectName(QStringLiteral("label_143"));
        label_143->setFont(font5);

        horizontalLayout_85->addWidget(label_143);

        horizontalSpacer_71 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_85->addItem(horizontalSpacer_71);


        verticalLayout_46->addLayout(horizontalLayout_85);


        verticalLayout_47->addLayout(verticalLayout_46);


        horizontalLayout_90->addWidget(groupBox_21);

        groupBox_20 = new QGroupBox(tab_11);
        groupBox_20->setObjectName(QStringLiteral("groupBox_20"));
        groupBox_20->setAlignment(Qt::AlignCenter);
        verticalLayout_45 = new QVBoxLayout(groupBox_20);
        verticalLayout_45->setSpacing(6);
        verticalLayout_45->setContentsMargins(11, 11, 11, 11);
        verticalLayout_45->setObjectName(QStringLiteral("verticalLayout_45"));
        verticalLayout_48 = new QVBoxLayout();
        verticalLayout_48->setSpacing(6);
        verticalLayout_48->setObjectName(QStringLiteral("verticalLayout_48"));
        horizontalLayout_86 = new QHBoxLayout();
        horizontalLayout_86->setSpacing(6);
        horizontalLayout_86->setObjectName(QStringLiteral("horizontalLayout_86"));
        label_144 = new QLabel(groupBox_20);
        label_144->setObjectName(QStringLiteral("label_144"));

        horizontalLayout_86->addWidget(label_144);

        doubleSpinBox_SetOnTime_18 = new QDoubleSpinBox(groupBox_20);
        doubleSpinBox_SetOnTime_18->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_18"));
        doubleSpinBox_SetOnTime_18->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_18->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_18->setFont(font4);
        doubleSpinBox_SetOnTime_18->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_18->setDecimals(1);
        doubleSpinBox_SetOnTime_18->setMaximum(999999);
        doubleSpinBox_SetOnTime_18->setValue(4.2);

        horizontalLayout_86->addWidget(doubleSpinBox_SetOnTime_18);

        label_145 = new QLabel(groupBox_20);
        label_145->setObjectName(QStringLiteral("label_145"));
        label_145->setFont(font5);

        horizontalLayout_86->addWidget(label_145);

        horizontalSpacer_72 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_86->addItem(horizontalSpacer_72);


        verticalLayout_48->addLayout(horizontalLayout_86);

        horizontalLayout_87 = new QHBoxLayout();
        horizontalLayout_87->setSpacing(6);
        horizontalLayout_87->setObjectName(QStringLiteral("horizontalLayout_87"));
        label_146 = new QLabel(groupBox_20);
        label_146->setObjectName(QStringLiteral("label_146"));
        label_146->setFont(font6);

        horizontalLayout_87->addWidget(label_146);

        doubleSpinBox_SetOnTimeError_18 = new QDoubleSpinBox(groupBox_20);
        doubleSpinBox_SetOnTimeError_18->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_18"));
        doubleSpinBox_SetOnTimeError_18->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_18->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_18->setFont(font4);
        doubleSpinBox_SetOnTimeError_18->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_18->setDecimals(1);
        doubleSpinBox_SetOnTimeError_18->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_18->setValue(1.8);

        horizontalLayout_87->addWidget(doubleSpinBox_SetOnTimeError_18);

        label_147 = new QLabel(groupBox_20);
        label_147->setObjectName(QStringLiteral("label_147"));
        label_147->setFont(font5);

        horizontalLayout_87->addWidget(label_147);

        horizontalSpacer_73 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_87->addItem(horizontalSpacer_73);


        verticalLayout_48->addLayout(horizontalLayout_87);

        line_18 = new QFrame(groupBox_20);
        line_18->setObjectName(QStringLiteral("line_18"));
        line_18->setFrameShape(QFrame::HLine);
        line_18->setFrameShadow(QFrame::Sunken);

        verticalLayout_48->addWidget(line_18);

        horizontalLayout_88 = new QHBoxLayout();
        horizontalLayout_88->setSpacing(6);
        horizontalLayout_88->setObjectName(QStringLiteral("horizontalLayout_88"));
        label_148 = new QLabel(groupBox_20);
        label_148->setObjectName(QStringLiteral("label_148"));

        horizontalLayout_88->addWidget(label_148);

        doubleSpinBox_SetOffTime_18 = new QDoubleSpinBox(groupBox_20);
        doubleSpinBox_SetOffTime_18->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_18"));
        doubleSpinBox_SetOffTime_18->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_18->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_18->setFont(font7);
        doubleSpinBox_SetOffTime_18->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_18->setDecimals(1);
        doubleSpinBox_SetOffTime_18->setMaximum(999999);
        doubleSpinBox_SetOffTime_18->setValue(20.8);

        horizontalLayout_88->addWidget(doubleSpinBox_SetOffTime_18);

        label_149 = new QLabel(groupBox_20);
        label_149->setObjectName(QStringLiteral("label_149"));
        label_149->setFont(font5);

        horizontalLayout_88->addWidget(label_149);

        horizontalSpacer_74 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_88->addItem(horizontalSpacer_74);


        verticalLayout_48->addLayout(horizontalLayout_88);

        horizontalLayout_89 = new QHBoxLayout();
        horizontalLayout_89->setSpacing(6);
        horizontalLayout_89->setObjectName(QStringLiteral("horizontalLayout_89"));
        label_150 = new QLabel(groupBox_20);
        label_150->setObjectName(QStringLiteral("label_150"));
        label_150->setFont(font6);

        horizontalLayout_89->addWidget(label_150);

        doubleSpinBox_SetOffTimeError_18 = new QDoubleSpinBox(groupBox_20);
        doubleSpinBox_SetOffTimeError_18->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_18"));
        doubleSpinBox_SetOffTimeError_18->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_18->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_18->setFont(font4);
        doubleSpinBox_SetOffTimeError_18->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_18->setDecimals(1);
        doubleSpinBox_SetOffTimeError_18->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_18->setValue(1.8);

        horizontalLayout_89->addWidget(doubleSpinBox_SetOffTimeError_18);

        label_151 = new QLabel(groupBox_20);
        label_151->setObjectName(QStringLiteral("label_151"));
        label_151->setFont(font5);

        horizontalLayout_89->addWidget(label_151);

        horizontalSpacer_75 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_89->addItem(horizontalSpacer_75);


        verticalLayout_48->addLayout(horizontalLayout_89);


        verticalLayout_45->addLayout(verticalLayout_48);


        horizontalLayout_90->addWidget(groupBox_20);

        tabWidget_2->addTab(tab_11, QString());
        tab_12 = new QWidget();
        tab_12->setObjectName(QStringLiteral("tab_12"));
        horizontalLayout_99 = new QHBoxLayout(tab_12);
        horizontalLayout_99->setSpacing(6);
        horizontalLayout_99->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_99->setObjectName(QStringLiteral("horizontalLayout_99"));
        groupBox_23 = new QGroupBox(tab_12);
        groupBox_23->setObjectName(QStringLiteral("groupBox_23"));
        groupBox_23->setAlignment(Qt::AlignCenter);
        verticalLayout_51 = new QVBoxLayout(groupBox_23);
        verticalLayout_51->setSpacing(6);
        verticalLayout_51->setContentsMargins(11, 11, 11, 11);
        verticalLayout_51->setObjectName(QStringLiteral("verticalLayout_51"));
        verticalLayout_50 = new QVBoxLayout();
        verticalLayout_50->setSpacing(6);
        verticalLayout_50->setObjectName(QStringLiteral("verticalLayout_50"));
        horizontalLayout_91 = new QHBoxLayout();
        horizontalLayout_91->setSpacing(6);
        horizontalLayout_91->setObjectName(QStringLiteral("horizontalLayout_91"));
        label_152 = new QLabel(groupBox_23);
        label_152->setObjectName(QStringLiteral("label_152"));

        horizontalLayout_91->addWidget(label_152);

        doubleSpinBox_SetOnTime_19 = new QDoubleSpinBox(groupBox_23);
        doubleSpinBox_SetOnTime_19->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_19"));
        doubleSpinBox_SetOnTime_19->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_19->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_19->setFont(font4);
        doubleSpinBox_SetOnTime_19->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_19->setDecimals(1);
        doubleSpinBox_SetOnTime_19->setMaximum(999999);
        doubleSpinBox_SetOnTime_19->setValue(4.2);

        horizontalLayout_91->addWidget(doubleSpinBox_SetOnTime_19);

        label_153 = new QLabel(groupBox_23);
        label_153->setObjectName(QStringLiteral("label_153"));
        label_153->setFont(font5);

        horizontalLayout_91->addWidget(label_153);

        horizontalSpacer_76 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_91->addItem(horizontalSpacer_76);


        verticalLayout_50->addLayout(horizontalLayout_91);

        horizontalLayout_92 = new QHBoxLayout();
        horizontalLayout_92->setSpacing(6);
        horizontalLayout_92->setObjectName(QStringLiteral("horizontalLayout_92"));
        label_154 = new QLabel(groupBox_23);
        label_154->setObjectName(QStringLiteral("label_154"));
        label_154->setFont(font6);

        horizontalLayout_92->addWidget(label_154);

        doubleSpinBox_SetOnTimeError_19 = new QDoubleSpinBox(groupBox_23);
        doubleSpinBox_SetOnTimeError_19->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_19"));
        doubleSpinBox_SetOnTimeError_19->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_19->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_19->setFont(font4);
        doubleSpinBox_SetOnTimeError_19->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_19->setDecimals(1);
        doubleSpinBox_SetOnTimeError_19->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_19->setValue(1.8);

        horizontalLayout_92->addWidget(doubleSpinBox_SetOnTimeError_19);

        label_155 = new QLabel(groupBox_23);
        label_155->setObjectName(QStringLiteral("label_155"));
        label_155->setFont(font5);

        horizontalLayout_92->addWidget(label_155);

        horizontalSpacer_77 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_92->addItem(horizontalSpacer_77);


        verticalLayout_50->addLayout(horizontalLayout_92);

        line_19 = new QFrame(groupBox_23);
        line_19->setObjectName(QStringLiteral("line_19"));
        line_19->setFrameShape(QFrame::HLine);
        line_19->setFrameShadow(QFrame::Sunken);

        verticalLayout_50->addWidget(line_19);

        horizontalLayout_93 = new QHBoxLayout();
        horizontalLayout_93->setSpacing(6);
        horizontalLayout_93->setObjectName(QStringLiteral("horizontalLayout_93"));
        label_156 = new QLabel(groupBox_23);
        label_156->setObjectName(QStringLiteral("label_156"));

        horizontalLayout_93->addWidget(label_156);

        doubleSpinBox_SetOffTime_19 = new QDoubleSpinBox(groupBox_23);
        doubleSpinBox_SetOffTime_19->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_19"));
        doubleSpinBox_SetOffTime_19->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_19->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_19->setFont(font7);
        doubleSpinBox_SetOffTime_19->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_19->setDecimals(1);
        doubleSpinBox_SetOffTime_19->setMaximum(999999);
        doubleSpinBox_SetOffTime_19->setValue(20.8);

        horizontalLayout_93->addWidget(doubleSpinBox_SetOffTime_19);

        label_157 = new QLabel(groupBox_23);
        label_157->setObjectName(QStringLiteral("label_157"));
        label_157->setFont(font5);

        horizontalLayout_93->addWidget(label_157);

        horizontalSpacer_78 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_93->addItem(horizontalSpacer_78);


        verticalLayout_50->addLayout(horizontalLayout_93);

        horizontalLayout_94 = new QHBoxLayout();
        horizontalLayout_94->setSpacing(6);
        horizontalLayout_94->setObjectName(QStringLiteral("horizontalLayout_94"));
        label_158 = new QLabel(groupBox_23);
        label_158->setObjectName(QStringLiteral("label_158"));
        label_158->setFont(font6);

        horizontalLayout_94->addWidget(label_158);

        doubleSpinBox_SetOffTimeError_19 = new QDoubleSpinBox(groupBox_23);
        doubleSpinBox_SetOffTimeError_19->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_19"));
        doubleSpinBox_SetOffTimeError_19->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_19->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_19->setFont(font4);
        doubleSpinBox_SetOffTimeError_19->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_19->setDecimals(1);
        doubleSpinBox_SetOffTimeError_19->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_19->setValue(1.8);

        horizontalLayout_94->addWidget(doubleSpinBox_SetOffTimeError_19);

        label_159 = new QLabel(groupBox_23);
        label_159->setObjectName(QStringLiteral("label_159"));
        label_159->setFont(font5);

        horizontalLayout_94->addWidget(label_159);

        horizontalSpacer_79 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_94->addItem(horizontalSpacer_79);


        verticalLayout_50->addLayout(horizontalLayout_94);


        verticalLayout_51->addLayout(verticalLayout_50);


        horizontalLayout_99->addWidget(groupBox_23);

        groupBox_22 = new QGroupBox(tab_12);
        groupBox_22->setObjectName(QStringLiteral("groupBox_22"));
        groupBox_22->setAlignment(Qt::AlignCenter);
        verticalLayout_49 = new QVBoxLayout(groupBox_22);
        verticalLayout_49->setSpacing(6);
        verticalLayout_49->setContentsMargins(11, 11, 11, 11);
        verticalLayout_49->setObjectName(QStringLiteral("verticalLayout_49"));
        verticalLayout_52 = new QVBoxLayout();
        verticalLayout_52->setSpacing(6);
        verticalLayout_52->setObjectName(QStringLiteral("verticalLayout_52"));
        horizontalLayout_95 = new QHBoxLayout();
        horizontalLayout_95->setSpacing(6);
        horizontalLayout_95->setObjectName(QStringLiteral("horizontalLayout_95"));
        label_160 = new QLabel(groupBox_22);
        label_160->setObjectName(QStringLiteral("label_160"));

        horizontalLayout_95->addWidget(label_160);

        doubleSpinBox_SetOnTime_20 = new QDoubleSpinBox(groupBox_22);
        doubleSpinBox_SetOnTime_20->setObjectName(QStringLiteral("doubleSpinBox_SetOnTime_20"));
        doubleSpinBox_SetOnTime_20->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTime_20->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTime_20->setFont(font4);
        doubleSpinBox_SetOnTime_20->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTime_20->setDecimals(1);
        doubleSpinBox_SetOnTime_20->setMaximum(999999);
        doubleSpinBox_SetOnTime_20->setValue(4.2);

        horizontalLayout_95->addWidget(doubleSpinBox_SetOnTime_20);

        label_161 = new QLabel(groupBox_22);
        label_161->setObjectName(QStringLiteral("label_161"));
        label_161->setFont(font5);

        horizontalLayout_95->addWidget(label_161);

        horizontalSpacer_80 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_95->addItem(horizontalSpacer_80);


        verticalLayout_52->addLayout(horizontalLayout_95);

        horizontalLayout_96 = new QHBoxLayout();
        horizontalLayout_96->setSpacing(6);
        horizontalLayout_96->setObjectName(QStringLiteral("horizontalLayout_96"));
        label_162 = new QLabel(groupBox_22);
        label_162->setObjectName(QStringLiteral("label_162"));
        label_162->setFont(font6);

        horizontalLayout_96->addWidget(label_162);

        doubleSpinBox_SetOnTimeError_20 = new QDoubleSpinBox(groupBox_22);
        doubleSpinBox_SetOnTimeError_20->setObjectName(QStringLiteral("doubleSpinBox_SetOnTimeError_20"));
        doubleSpinBox_SetOnTimeError_20->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOnTimeError_20->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOnTimeError_20->setFont(font4);
        doubleSpinBox_SetOnTimeError_20->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOnTimeError_20->setDecimals(1);
        doubleSpinBox_SetOnTimeError_20->setMaximum(999999);
        doubleSpinBox_SetOnTimeError_20->setValue(1.8);

        horizontalLayout_96->addWidget(doubleSpinBox_SetOnTimeError_20);

        label_163 = new QLabel(groupBox_22);
        label_163->setObjectName(QStringLiteral("label_163"));
        label_163->setFont(font5);

        horizontalLayout_96->addWidget(label_163);

        horizontalSpacer_81 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_96->addItem(horizontalSpacer_81);


        verticalLayout_52->addLayout(horizontalLayout_96);

        line_20 = new QFrame(groupBox_22);
        line_20->setObjectName(QStringLiteral("line_20"));
        line_20->setFrameShape(QFrame::HLine);
        line_20->setFrameShadow(QFrame::Sunken);

        verticalLayout_52->addWidget(line_20);

        horizontalLayout_97 = new QHBoxLayout();
        horizontalLayout_97->setSpacing(6);
        horizontalLayout_97->setObjectName(QStringLiteral("horizontalLayout_97"));
        label_164 = new QLabel(groupBox_22);
        label_164->setObjectName(QStringLiteral("label_164"));

        horizontalLayout_97->addWidget(label_164);

        doubleSpinBox_SetOffTime_20 = new QDoubleSpinBox(groupBox_22);
        doubleSpinBox_SetOffTime_20->setObjectName(QStringLiteral("doubleSpinBox_SetOffTime_20"));
        doubleSpinBox_SetOffTime_20->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTime_20->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTime_20->setFont(font7);
        doubleSpinBox_SetOffTime_20->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTime_20->setDecimals(1);
        doubleSpinBox_SetOffTime_20->setMaximum(999999);
        doubleSpinBox_SetOffTime_20->setValue(20.8);

        horizontalLayout_97->addWidget(doubleSpinBox_SetOffTime_20);

        label_165 = new QLabel(groupBox_22);
        label_165->setObjectName(QStringLiteral("label_165"));
        label_165->setFont(font5);

        horizontalLayout_97->addWidget(label_165);

        horizontalSpacer_82 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_97->addItem(horizontalSpacer_82);


        verticalLayout_52->addLayout(horizontalLayout_97);

        horizontalLayout_98 = new QHBoxLayout();
        horizontalLayout_98->setSpacing(6);
        horizontalLayout_98->setObjectName(QStringLiteral("horizontalLayout_98"));
        label_166 = new QLabel(groupBox_22);
        label_166->setObjectName(QStringLiteral("label_166"));
        label_166->setFont(font6);

        horizontalLayout_98->addWidget(label_166);

        doubleSpinBox_SetOffTimeError_20 = new QDoubleSpinBox(groupBox_22);
        doubleSpinBox_SetOffTimeError_20->setObjectName(QStringLiteral("doubleSpinBox_SetOffTimeError_20"));
        doubleSpinBox_SetOffTimeError_20->setMinimumSize(QSize(150, 40));
        doubleSpinBox_SetOffTimeError_20->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetOffTimeError_20->setFont(font4);
        doubleSpinBox_SetOffTimeError_20->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetOffTimeError_20->setDecimals(1);
        doubleSpinBox_SetOffTimeError_20->setMaximum(999999);
        doubleSpinBox_SetOffTimeError_20->setValue(1.8);

        horizontalLayout_98->addWidget(doubleSpinBox_SetOffTimeError_20);

        label_167 = new QLabel(groupBox_22);
        label_167->setObjectName(QStringLiteral("label_167"));
        label_167->setFont(font5);

        horizontalLayout_98->addWidget(label_167);

        horizontalSpacer_83 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_98->addItem(horizontalSpacer_83);


        verticalLayout_52->addLayout(horizontalLayout_98);


        verticalLayout_49->addLayout(verticalLayout_52);


        horizontalLayout_99->addWidget(groupBox_22);

        tabWidget_2->addTab(tab_12, QString());

        horizontalLayout_5->addWidget(tabWidget_2);


        verticalLayout_12->addLayout(horizontalLayout_5);

        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));

        verticalLayout_12->addLayout(verticalLayout_15);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        groupBox = new QGroupBox(widget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setAlignment(Qt::AlignCenter);
        verticalLayout_9 = new QVBoxLayout(groupBox);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalLayout_9->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_9->setContentsMargins(9, -1, -1, -1);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        QFont font8;
        font8.setFamily(QStringLiteral("04b_21"));
        font8.setPointSize(22);
        label->setFont(font8);

        horizontalLayout_3->addWidget(label);

        doubleSpinBox_SetAllTime = new QDoubleSpinBox(groupBox);
        doubleSpinBox_SetAllTime->setObjectName(QStringLiteral("doubleSpinBox_SetAllTime"));
        doubleSpinBox_SetAllTime->setMinimumSize(QSize(130, 50));
        doubleSpinBox_SetAllTime->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetAllTime->setFont(font4);
        doubleSpinBox_SetAllTime->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetAllTime->setDecimals(1);
        doubleSpinBox_SetAllTime->setMaximum(999999);
        doubleSpinBox_SetAllTime->setValue(1800);

        horizontalLayout_3->addWidget(doubleSpinBox_SetAllTime);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font4);

        horizontalLayout_3->addWidget(label_2);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_7);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font9;
        font9.setPointSize(20);
        label_3->setFont(font9);

        horizontalLayout_4->addWidget(label_3);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font8);

        horizontalLayout_4->addWidget(label_4);

        doubleSpinBox_SetAllTimeError = new QDoubleSpinBox(groupBox);
        doubleSpinBox_SetAllTimeError->setObjectName(QStringLiteral("doubleSpinBox_SetAllTimeError"));
        doubleSpinBox_SetAllTimeError->setMinimumSize(QSize(130, 50));
        doubleSpinBox_SetAllTimeError->setMaximumSize(QSize(16777215, 40));
        doubleSpinBox_SetAllTimeError->setFont(font4);
        doubleSpinBox_SetAllTimeError->setCursor(QCursor(Qt::IBeamCursor));
        doubleSpinBox_SetAllTimeError->setDecimals(1);
        doubleSpinBox_SetAllTimeError->setMaximum(999999);
        doubleSpinBox_SetAllTimeError->setValue(90);

        horizontalLayout_4->addWidget(doubleSpinBox_SetAllTimeError);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font4);

        horizontalLayout_4->addWidget(label_6);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout_9->addLayout(verticalLayout_2);


        horizontalLayout_6->addWidget(groupBox);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));

        horizontalLayout_6->addLayout(horizontalLayout_16);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        groupBox_4 = new QGroupBox(widget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setAlignment(Qt::AlignCenter);
        verticalLayout_7 = new QVBoxLayout(groupBox_4);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        radioButton_220V = new QRadioButton(groupBox_4);
        radioButton_220V->setObjectName(QStringLiteral("radioButton_220V"));
        radioButton_220V->setAcceptDrops(false);
        radioButton_220V->setChecked(true);

        verticalLayout_7->addWidget(radioButton_220V);

        radioButton_30V = new QRadioButton(groupBox_4);
        radioButton_30V->setObjectName(QStringLiteral("radioButton_30V"));

        verticalLayout_7->addWidget(radioButton_30V);


        verticalLayout_8->addWidget(groupBox_4);

        pushButton_sendConfig = new QPushButton(widget);
        pushButton_sendConfig->setObjectName(QStringLiteral("pushButton_sendConfig"));
        pushButton_sendConfig->setMinimumSize(QSize(130, 40));
        pushButton_sendConfig->setStyleSheet(QStringLiteral("color:rgb(85, 0, 255)"));

        verticalLayout_8->addWidget(pushButton_sendConfig);


        horizontalLayout_6->addLayout(verticalLayout_8);


        horizontalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));

        horizontalLayout_2->addLayout(horizontalLayout_17);

        line_24 = new QFrame(widget);
        line_24->setObjectName(QStringLiteral("line_24"));
        line_24->setFrameShape(QFrame::HLine);
        line_24->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(line_24);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        groupBox_5 = new QGroupBox(widget);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setAlignment(Qt::AlignCenter);
        verticalLayout_14 = new QVBoxLayout(groupBox_5);
        verticalLayout_14->setSpacing(6);
        verticalLayout_14->setContentsMargins(11, 11, 11, 11);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        lineEdit_fileconfig = new QLineEdit(groupBox_5);
        lineEdit_fileconfig->setObjectName(QStringLiteral("lineEdit_fileconfig"));
        lineEdit_fileconfig->setEnabled(false);
        lineEdit_fileconfig->setMinimumSize(QSize(0, 30));

        verticalLayout_14->addWidget(lineEdit_fileconfig);

        pushButton_openfile = new QPushButton(groupBox_5);
        pushButton_openfile->setObjectName(QStringLiteral("pushButton_openfile"));
        pushButton_openfile->setMinimumSize(QSize(130, 40));

        verticalLayout_14->addWidget(pushButton_openfile);

        pushButton_save = new QPushButton(groupBox_5);
        pushButton_save->setObjectName(QStringLiteral("pushButton_save"));
        pushButton_save->setMinimumSize(QSize(130, 40));

        verticalLayout_14->addWidget(pushButton_save);


        verticalLayout_11->addWidget(groupBox_5);


        horizontalLayout_2->addLayout(verticalLayout_11);


        verticalLayout_12->addLayout(horizontalLayout_2);


        horizontalLayout_15->addLayout(verticalLayout_12);

        line_23 = new QFrame(widget);
        line_23->setObjectName(QStringLiteral("line_23"));
        line_23->setFrameShape(QFrame::VLine);
        line_23->setFrameShadow(QFrame::Sunken);

        horizontalLayout_15->addWidget(line_23);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        groupBox_25 = new QGroupBox(widget);
        groupBox_25->setObjectName(QStringLiteral("groupBox_25"));
        groupBox_25->setMinimumSize(QSize(200, 0));
        groupBox_25->setMaximumSize(QSize(600, 16777215));
        verticalLayout_59 = new QVBoxLayout(groupBox_25);
        verticalLayout_59->setSpacing(6);
        verticalLayout_59->setContentsMargins(11, 11, 11, 11);
        verticalLayout_59->setObjectName(QStringLiteral("verticalLayout_59"));
        lineEdit_filebin = new QLineEdit(groupBox_25);
        lineEdit_filebin->setObjectName(QStringLiteral("lineEdit_filebin"));
        lineEdit_filebin->setEnabled(false);
        lineEdit_filebin->setMinimumSize(QSize(0, 30));

        verticalLayout_59->addWidget(lineEdit_filebin);

        pushButton_openbin = new QPushButton(groupBox_25);
        pushButton_openbin->setObjectName(QStringLiteral("pushButton_openbin"));
        pushButton_openbin->setMinimumSize(QSize(130, 60));
        pushButton_openbin->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_59->addWidget(pushButton_openbin);

        pushButton_updatebin = new QPushButton(groupBox_25);
        pushButton_updatebin->setObjectName(QStringLiteral("pushButton_updatebin"));
        pushButton_updatebin->setMinimumSize(QSize(130, 60));
        pushButton_updatebin->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_59->addWidget(pushButton_updatebin);


        verticalLayout_10->addWidget(groupBox_25);

        textEdit = new QTextEdit(widget);
        textEdit->setObjectName(QStringLiteral("textEdit"));

        verticalLayout_10->addWidget(textEdit);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        comboBox_com = new QComboBox(widget);
        comboBox_com->setObjectName(QStringLiteral("comboBox_com"));
        comboBox_com->setMinimumSize(QSize(70, 60));

        horizontalLayout->addWidget(comboBox_com);

        pushButton_openCOM = new QPushButton(widget);
        pushButton_openCOM->setObjectName(QStringLiteral("pushButton_openCOM"));
        pushButton_openCOM->setMinimumSize(QSize(70, 60));

        horizontalLayout->addWidget(pushButton_openCOM);


        verticalLayout_10->addLayout(horizontalLayout);


        horizontalLayout_15->addLayout(verticalLayout_10);


        verticalLayout_13->addLayout(horizontalLayout_15);


        verticalLayout_40->addWidget(widget);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);
        tabWidget_2->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionOpen->setText(QApplication::translate("MainWindow", "open", Q_NULLPTR));
        actionSave->setText(QApplication::translate("MainWindow", "save", Q_NULLPTR));
        actionExit->setText(QApplication::translate("MainWindow", "exit", Q_NULLPTR));
        actionOptions->setText(QApplication::translate("MainWindow", "options", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "\345\234\250\347\272\277\350\256\276\345\244\207\346\225\260\351\207\217\357\274\232", Q_NULLPTR));
        label_169->setText(QApplication::translate("MainWindow", "\346\255\243\345\234\250\346\265\213\350\257\225\346\225\260\351\207\217\357\274\232", Q_NULLPTR));
        label_172->setText(QApplication::translate("MainWindow", "\347\264\257\347\247\257\346\225\205\351\232\234\346\225\260\351\207\217\357\274\232", Q_NULLPTR));
        pushButton_clearErrorNum->setText(QApplication::translate("MainWindow", "\346\270\205\351\233\266", Q_NULLPTR));
        label_168->setText(QApplication::translate("MainWindow", "\347\264\257\347\247\257\345\256\214\346\210\220\346\225\260\351\207\217\357\274\232", Q_NULLPTR));
        pushButton_clearOkNum->setText(QApplication::translate("MainWindow", "\346\270\205\351\233\266", Q_NULLPTR));
        label_nowStep_value->setText(QApplication::translate("MainWindow", "\345\207\206\345\244\207\346\243\200\346\265\213\357\274\232\344\270\200\346\241\243", Q_NULLPTR));
        label_170->setText(QApplication::translate("MainWindow", "\345\205\210\351\200\211\346\213\251\346\241\243\344\275\215\357\274\214\347\204\266\345\220\216\345\206\215\345\274\200\345\247\213\346\243\200\346\265\213", Q_NULLPTR));
        pushButton_stop->setText(QApplication::translate("MainWindow", "\345\274\200\345\247\213\346\243\200\346\265\213", Q_NULLPTR));
        groupBox_24->setTitle(QApplication::translate("MainWindow", "\345\267\245\344\275\234\347\212\266\346\200\201\344\277\241\346\201\257", Q_NULLPTR));
        pushButton_dev_1->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        pushButton_dev_2->setText(QApplication::translate("MainWindow", "2", Q_NULLPTR));
        pushButton_dev_3->setText(QApplication::translate("MainWindow", "3", Q_NULLPTR));
        pushButton_dev_4->setText(QApplication::translate("MainWindow", "4", Q_NULLPTR));
        pushButton_dev_5->setText(QApplication::translate("MainWindow", "5", Q_NULLPTR));
        pushButton_dev_6->setText(QApplication::translate("MainWindow", "6", Q_NULLPTR));
        pushButton_dev_7->setText(QApplication::translate("MainWindow", "7", Q_NULLPTR));
        pushButton_dev_8->setText(QApplication::translate("MainWindow", "8", Q_NULLPTR));
        pushButton_dev_9->setText(QApplication::translate("MainWindow", "9", Q_NULLPTR));
        pushButton_dev_10->setText(QApplication::translate("MainWindow", "10", Q_NULLPTR));
        pushButton_dev_11->setText(QApplication::translate("MainWindow", "11", Q_NULLPTR));
        pushButton_dev_12->setText(QApplication::translate("MainWindow", "12", Q_NULLPTR));
        pushButton_dev_13->setText(QApplication::translate("MainWindow", "13", Q_NULLPTR));
        pushButton_dev_14->setText(QApplication::translate("MainWindow", "14", Q_NULLPTR));
        pushButton_dev_15->setText(QApplication::translate("MainWindow", "15", Q_NULLPTR));
        pushButton_dev_16->setText(QApplication::translate("MainWindow", "16", Q_NULLPTR));
        pushButton_dev_17->setText(QApplication::translate("MainWindow", "17", Q_NULLPTR));
        pushButton_dev_18->setText(QApplication::translate("MainWindow", "18", Q_NULLPTR));
        pushButton_dev_19->setText(QApplication::translate("MainWindow", "19", Q_NULLPTR));
        pushButton_dev_20->setText(QApplication::translate("MainWindow", "20", Q_NULLPTR));
        pushButton_dev_21->setText(QApplication::translate("MainWindow", "21", Q_NULLPTR));
        pushButton_dev_22->setText(QApplication::translate("MainWindow", "22", Q_NULLPTR));
        pushButton_dev_23->setText(QApplication::translate("MainWindow", "23", Q_NULLPTR));
        pushButton_dev_24->setText(QApplication::translate("MainWindow", "24", Q_NULLPTR));
        pushButton_dev_25->setText(QApplication::translate("MainWindow", "25", Q_NULLPTR));
        pushButton_dev_26->setText(QApplication::translate("MainWindow", "26", Q_NULLPTR));
        pushButton_dev_27->setText(QApplication::translate("MainWindow", "27", Q_NULLPTR));
        pushButton_dev_28->setText(QApplication::translate("MainWindow", "28", Q_NULLPTR));
        pushButton_dev_29->setText(QApplication::translate("MainWindow", "29", Q_NULLPTR));
        pushButton_dev_30->setText(QApplication::translate("MainWindow", "30", Q_NULLPTR));
        pushButton_dev_31->setText(QApplication::translate("MainWindow", "31", Q_NULLPTR));
        pushButton_dev_32->setText(QApplication::translate("MainWindow", "32", Q_NULLPTR));
        pushButton_dev_33->setText(QApplication::translate("MainWindow", "33", Q_NULLPTR));
        pushButton_dev_34->setText(QApplication::translate("MainWindow", "34", Q_NULLPTR));
        pushButton_dev_35->setText(QApplication::translate("MainWindow", "35", Q_NULLPTR));
        pushButton_dev_36->setText(QApplication::translate("MainWindow", "36", Q_NULLPTR));
        pushButton_dev_37->setText(QApplication::translate("MainWindow", "37", Q_NULLPTR));
        pushButton_dev_38->setText(QApplication::translate("MainWindow", "38", Q_NULLPTR));
        pushButton_dev_39->setText(QApplication::translate("MainWindow", "39", Q_NULLPTR));
        pushButton_dev_40->setText(QApplication::translate("MainWindow", "40", Q_NULLPTR));
        pushButton_dev_41->setText(QApplication::translate("MainWindow", "41", Q_NULLPTR));
        pushButton_dev_42->setText(QApplication::translate("MainWindow", "42", Q_NULLPTR));
        pushButton_dev_43->setText(QApplication::translate("MainWindow", "43", Q_NULLPTR));
        pushButton_dev_44->setText(QApplication::translate("MainWindow", "44", Q_NULLPTR));
        pushButton_dev_45->setText(QApplication::translate("MainWindow", "45", Q_NULLPTR));
        pushButton_dev_46->setText(QApplication::translate("MainWindow", "46", Q_NULLPTR));
        pushButton_dev_47->setText(QApplication::translate("MainWindow", "47", Q_NULLPTR));
        pushButton_dev_48->setText(QApplication::translate("MainWindow", "48", Q_NULLPTR));
        pushButton_dev_49->setText(QApplication::translate("MainWindow", "49", Q_NULLPTR));
        pushButton_dev_50->setText(QApplication::translate("MainWindow", "50", Q_NULLPTR));
        pushButton_dev_51->setText(QApplication::translate("MainWindow", "51", Q_NULLPTR));
        pushButton_dev_52->setText(QApplication::translate("MainWindow", "52", Q_NULLPTR));
        pushButton_dev_53->setText(QApplication::translate("MainWindow", "53", Q_NULLPTR));
        pushButton_dev_54->setText(QApplication::translate("MainWindow", "54", Q_NULLPTR));
        pushButton_dev_55->setText(QApplication::translate("MainWindow", "55", Q_NULLPTR));
        pushButton_dev_56->setText(QApplication::translate("MainWindow", "56", Q_NULLPTR));
        pushButton_dev_57->setText(QApplication::translate("MainWindow", "57", Q_NULLPTR));
        pushButton_dev_58->setText(QApplication::translate("MainWindow", "58", Q_NULLPTR));
        pushButton_dev_59->setText(QApplication::translate("MainWindow", "59", Q_NULLPTR));
        pushButton_dev_60->setText(QApplication::translate("MainWindow", "60", Q_NULLPTR));
        pushButton_dev_61->setText(QApplication::translate("MainWindow", "61", Q_NULLPTR));
        pushButton_dev_62->setText(QApplication::translate("MainWindow", "62", Q_NULLPTR));
        pushButton_dev_63->setText(QApplication::translate("MainWindow", "63", Q_NULLPTR));
        pushButton_dev_64->setText(QApplication::translate("MainWindow", "64", Q_NULLPTR));
        pushButton_dev_65->setText(QApplication::translate("MainWindow", "65", Q_NULLPTR));
        pushButton_dev_66->setText(QApplication::translate("MainWindow", "66", Q_NULLPTR));
        pushButton_dev_67->setText(QApplication::translate("MainWindow", "67", Q_NULLPTR));
        pushButton_dev_68->setText(QApplication::translate("MainWindow", "68", Q_NULLPTR));
        pushButton_dev_69->setText(QApplication::translate("MainWindow", "69", Q_NULLPTR));
        pushButton_dev_70->setText(QApplication::translate("MainWindow", "70", Q_NULLPTR));
        pushButton_dev_71->setText(QApplication::translate("MainWindow", "71", Q_NULLPTR));
        pushButton_dev_72->setText(QApplication::translate("MainWindow", "72", Q_NULLPTR));
        pushButton_dev_73->setText(QApplication::translate("MainWindow", "73", Q_NULLPTR));
        pushButton_dev_74->setText(QApplication::translate("MainWindow", "74", Q_NULLPTR));
        pushButton_dev_75->setText(QApplication::translate("MainWindow", "75", Q_NULLPTR));
        pushButton_dev_76->setText(QApplication::translate("MainWindow", "76", Q_NULLPTR));
        pushButton_dev_77->setText(QApplication::translate("MainWindow", "77", Q_NULLPTR));
        pushButton_dev_78->setText(QApplication::translate("MainWindow", "78", Q_NULLPTR));
        pushButton_dev_79->setText(QApplication::translate("MainWindow", "79", Q_NULLPTR));
        pushButton_dev_80->setText(QApplication::translate("MainWindow", "80", Q_NULLPTR));
        pushButton_dev_81->setText(QApplication::translate("MainWindow", "81", Q_NULLPTR));
        pushButton_dev_82->setText(QApplication::translate("MainWindow", "82", Q_NULLPTR));
        pushButton_dev_83->setText(QApplication::translate("MainWindow", "83", Q_NULLPTR));
        pushButton_dev_84->setText(QApplication::translate("MainWindow", "84", Q_NULLPTR));
        pushButton_dev_85->setText(QApplication::translate("MainWindow", "85", Q_NULLPTR));
        pushButton_dev_86->setText(QApplication::translate("MainWindow", "86", Q_NULLPTR));
        pushButton_dev_87->setText(QApplication::translate("MainWindow", "87", Q_NULLPTR));
        pushButton_dev_88->setText(QApplication::translate("MainWindow", "88", Q_NULLPTR));
        pushButton_dev_89->setText(QApplication::translate("MainWindow", "89", Q_NULLPTR));
        pushButton_dev_90->setText(QApplication::translate("MainWindow", "90", Q_NULLPTR));
        pushButton_dev_91->setText(QApplication::translate("MainWindow", "91", Q_NULLPTR));
        pushButton_dev_92->setText(QApplication::translate("MainWindow", "92", Q_NULLPTR));
        pushButton_dev_93->setText(QApplication::translate("MainWindow", "93", Q_NULLPTR));
        pushButton_dev_94->setText(QApplication::translate("MainWindow", "94", Q_NULLPTR));
        pushButton_dev_95->setText(QApplication::translate("MainWindow", "95", Q_NULLPTR));
        pushButton_dev_96->setText(QApplication::translate("MainWindow", "96", Q_NULLPTR));
        pushButton_dev_97->setText(QApplication::translate("MainWindow", "97", Q_NULLPTR));
        pushButton_dev_98->setText(QApplication::translate("MainWindow", "98", Q_NULLPTR));
        pushButton_dev_99->setText(QApplication::translate("MainWindow", "99", Q_NULLPTR));
        pushButton_dev_100->setText(QApplication::translate("MainWindow", "100", Q_NULLPTR));
        pushButton_dev_101->setText(QApplication::translate("MainWindow", "101", Q_NULLPTR));
        pushButton_dev_102->setText(QApplication::translate("MainWindow", "102", Q_NULLPTR));
        pushButton_dev_103->setText(QApplication::translate("MainWindow", "103", Q_NULLPTR));
        pushButton_dev_104->setText(QApplication::translate("MainWindow", "104", Q_NULLPTR));
        pushButton_dev_105->setText(QApplication::translate("MainWindow", "105", Q_NULLPTR));
        pushButton_dev_106->setText(QApplication::translate("MainWindow", "106", Q_NULLPTR));
        pushButton_dev_107->setText(QApplication::translate("MainWindow", "107", Q_NULLPTR));
        pushButton_dev_108->setText(QApplication::translate("MainWindow", "108", Q_NULLPTR));
        pushButton_dev_109->setText(QApplication::translate("MainWindow", "109", Q_NULLPTR));
        pushButton_dev_110->setText(QApplication::translate("MainWindow", "110", Q_NULLPTR));
        pushButton_dev_111->setText(QApplication::translate("MainWindow", "111", Q_NULLPTR));
        pushButton_dev_112->setText(QApplication::translate("MainWindow", "112", Q_NULLPTR));
        pushButton_dev_113->setText(QApplication::translate("MainWindow", "113", Q_NULLPTR));
        pushButton_dev_114->setText(QApplication::translate("MainWindow", "114", Q_NULLPTR));
        pushButton_dev_115->setText(QApplication::translate("MainWindow", "115", Q_NULLPTR));
        pushButton_dev_116->setText(QApplication::translate("MainWindow", "116", Q_NULLPTR));
        pushButton_dev_117->setText(QApplication::translate("MainWindow", "117", Q_NULLPTR));
        pushButton_dev_118->setText(QApplication::translate("MainWindow", "118", Q_NULLPTR));
        pushButton_dev_119->setText(QApplication::translate("MainWindow", "119", Q_NULLPTR));
        pushButton_dev_120->setText(QApplication::translate("MainWindow", "120", Q_NULLPTR));
        label_status_led->setText(QString());
        label_5->setText(QApplication::translate("MainWindow", "\345\275\223\345\211\215\346\241\243\344\275\215\346\227\266\351\227\264", Q_NULLPTR));
        label_timerStepTestTime->setText(QApplication::translate("MainWindow", "00:00:00", Q_NULLPTR));
        pushButton_ResetTest->setText(QApplication::translate("MainWindow", "\346\243\200\346\265\213\345\244\215\344\275\215", Q_NULLPTR));
        pushButton_startStep_1->setText(QApplication::translate("MainWindow", "\344\270\200\346\241\243", Q_NULLPTR));
        pushButton_startStep_2->setText(QApplication::translate("MainWindow", "\344\272\214\346\241\243", Q_NULLPTR));
        pushButton_startStep_3->setText(QApplication::translate("MainWindow", "\344\270\211\346\241\243", Q_NULLPTR));
        pushButton_startStep_4->setText(QApplication::translate("MainWindow", "\345\233\233\346\241\243", Q_NULLPTR));
        pushButton_startStep_5->setText(QApplication::translate("MainWindow", "\344\272\224\346\241\243", Q_NULLPTR));
        pushButton_startStep_6->setText(QApplication::translate("MainWindow", "\345\205\255\346\241\243", Q_NULLPTR));
        pushButton_startStep_7->setText(QApplication::translate("MainWindow", "\344\270\203\346\241\243", Q_NULLPTR));
        pushButton_startStep_8->setText(QApplication::translate("MainWindow", "\345\205\253\346\241\243", Q_NULLPTR));
        pushButton_startStep_9->setText(QApplication::translate("MainWindow", "\344\271\235\346\241\243", Q_NULLPTR));
        pushButton_startStep_10->setText(QApplication::translate("MainWindow", "\345\215\201\346\241\243", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "\345\267\245\344\275\234", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_9->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_11->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_15->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_14->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_12->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_16->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_17->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_18->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_19->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_20->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_21->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_22->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_23->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_3), QApplication::translate("MainWindow", "\346\241\243\344\275\2151", Q_NULLPTR));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_24->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_25->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_26->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_27->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_28->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_29->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_30->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_31->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_7->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_32->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_33->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_34->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_35->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_36->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_37->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_38->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_39->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_4), QApplication::translate("MainWindow", "\346\241\243\344\275\2152", Q_NULLPTR));
        groupBox_9->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_40->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_41->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_42->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_43->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_44->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_45->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_46->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_47->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_8->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_48->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_49->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_50->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_51->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_52->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_53->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_54->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_55->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_5), QApplication::translate("MainWindow", "\346\241\243\344\275\2153", Q_NULLPTR));
        groupBox_11->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_56->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_57->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_58->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_59->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_60->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_61->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_62->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_63->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_10->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_64->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_65->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_66->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_67->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_68->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_69->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_70->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_71->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_6), QApplication::translate("MainWindow", "\346\241\243\344\275\2154", Q_NULLPTR));
        groupBox_13->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_72->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_73->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_74->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_75->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_76->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_77->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_78->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_79->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_12->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_80->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_81->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_82->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_83->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_84->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_85->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_86->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_87->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_7), QApplication::translate("MainWindow", "\346\241\243\344\275\2155", Q_NULLPTR));
        groupBox_15->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_88->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_89->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_90->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_91->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_92->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_93->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_94->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_95->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_14->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_96->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_97->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_98->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_99->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_100->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_101->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_102->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_103->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_8), QApplication::translate("MainWindow", "\346\241\243\344\275\2156", Q_NULLPTR));
        groupBox_17->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_104->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_105->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_106->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_107->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_108->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_109->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_110->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_111->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_16->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_112->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_113->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_114->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_115->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_116->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_117->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_118->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_119->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_9), QApplication::translate("MainWindow", "\346\241\243\344\275\2157", Q_NULLPTR));
        groupBox_19->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_120->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_121->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_122->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_123->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_124->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_125->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_126->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_127->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_18->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_128->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_129->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_130->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_131->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_132->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_133->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_134->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_135->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_10), QApplication::translate("MainWindow", "\346\241\243\344\275\2158", Q_NULLPTR));
        groupBox_21->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_136->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_137->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_138->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_139->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_140->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_141->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_142->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_143->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_20->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_144->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_145->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_146->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_147->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_148->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_149->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_150->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_151->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_11), QApplication::translate("MainWindow", "\346\241\243\344\275\2159", Q_NULLPTR));
        groupBox_23->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2231", Q_NULLPTR));
        label_152->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_153->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_154->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_155->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_156->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_157->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_158->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_159->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_22->setTitle(QApplication::translate("MainWindow", "\351\200\232\351\201\2232", Q_NULLPTR));
        label_160->setText(QApplication::translate("MainWindow", "\346\216\245\351\200\232", Q_NULLPTR));
        label_161->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_162->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_163->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_164->setText(QApplication::translate("MainWindow", "\346\226\255\345\274\200", Q_NULLPTR));
        label_165->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_166->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_167->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_12), QApplication::translate("MainWindow", "\346\241\243\344\275\21510", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", " \346\200\273\346\227\266\351\227\264", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "\346\200\273\346\227\266\351\227\264", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "\350\257\257\345\267\256", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "\302\261", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "\347\247\222", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "\346\265\213\350\257\225\347\224\265\345\216\213", Q_NULLPTR));
        radioButton_220V->setText(QApplication::translate("MainWindow", "220V", Q_NULLPTR));
        radioButton_30V->setText(QApplication::translate("MainWindow", "30V", Q_NULLPTR));
        pushButton_sendConfig->setText(QApplication::translate("MainWindow", "\345\217\221\351\200\201\351\205\215\347\275\256", Q_NULLPTR));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "\351\205\215\347\275\256\346\226\207\344\273\266", Q_NULLPTR));
        pushButton_openfile->setText(QApplication::translate("MainWindow", "\346\211\223\345\274\200", Q_NULLPTR));
        pushButton_save->setText(QApplication::translate("MainWindow", "\344\277\235\345\255\230", Q_NULLPTR));
        groupBox_25->setTitle(QApplication::translate("MainWindow", "\345\233\272\344\273\266\345\215\207\347\272\247", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        pushButton_openbin->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_WHATSTHIS
        pushButton_openbin->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        pushButton_openbin->setText(QApplication::translate("MainWindow", "\351\200\211\346\213\251\345\233\272\344\273\266", Q_NULLPTR));
        pushButton_updatebin->setText(QApplication::translate("MainWindow", "\345\274\200\345\247\213\345\215\207\347\272\247", Q_NULLPTR));
        textEdit->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"http://dbfarm.win:8080\"><span style=\" text-decoration: underline; color:#0000ff;\">dbfarm.win</span></a></p></body></html>", Q_NULLPTR));
        comboBox_com->clear();
        comboBox_com->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "COM1", Q_NULLPTR)
         << QApplication::translate("MainWindow", "COM2", Q_NULLPTR)
         << QApplication::translate("MainWindow", "COM3", Q_NULLPTR)
         << QApplication::translate("MainWindow", "COM4", Q_NULLPTR)
         << QApplication::translate("MainWindow", "COM5", Q_NULLPTR)
         << QApplication::translate("MainWindow", "COM6", Q_NULLPTR)
         << QApplication::translate("MainWindow", "tty0", Q_NULLPTR)
         << QApplication::translate("MainWindow", "tty1", Q_NULLPTR)
         << QApplication::translate("MainWindow", "tty2", Q_NULLPTR)
         << QApplication::translate("MainWindow", "tty3", Q_NULLPTR)
         << QApplication::translate("MainWindow", "ttyUSB0", Q_NULLPTR)
         << QApplication::translate("MainWindow", "ttyUSB1", Q_NULLPTR)
        );
        pushButton_openCOM->setText(QApplication::translate("MainWindow", "\346\211\223\345\274\200\344\270\262\345\217\243", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "\350\256\276\347\275\256", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
