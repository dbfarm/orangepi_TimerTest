/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../orangepi_TimerTest/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[25];
    char stringdata0[717];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 16), // "Timer_100ms_Slot"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 13), // "readMyComSlot"
QT_MOC_LITERAL(4, 43, 29), // "on_pushButton_openCOM_clicked"
QT_MOC_LITERAL(5, 73, 32), // "on_pushButton_sendConfig_clicked"
QT_MOC_LITERAL(6, 106, 26), // "on_pushButton_save_clicked"
QT_MOC_LITERAL(7, 133, 30), // "on_pushButton_openfile_clicked"
QT_MOC_LITERAL(8, 164, 29), // "on_pushButton_openbin_clicked"
QT_MOC_LITERAL(9, 194, 31), // "on_pushButton_updatebin_clicked"
QT_MOC_LITERAL(10, 226, 33), // "on_pushButton_startStep_1_cli..."
QT_MOC_LITERAL(11, 260, 33), // "on_pushButton_startStep_2_cli..."
QT_MOC_LITERAL(12, 294, 33), // "on_pushButton_startStep_3_cli..."
QT_MOC_LITERAL(13, 328, 33), // "on_pushButton_startStep_4_cli..."
QT_MOC_LITERAL(14, 362, 33), // "on_pushButton_startStep_5_cli..."
QT_MOC_LITERAL(15, 396, 33), // "on_pushButton_startStep_6_cli..."
QT_MOC_LITERAL(16, 430, 33), // "on_pushButton_startStep_7_cli..."
QT_MOC_LITERAL(17, 464, 33), // "on_pushButton_startStep_8_cli..."
QT_MOC_LITERAL(18, 498, 33), // "on_pushButton_startStep_9_cli..."
QT_MOC_LITERAL(19, 532, 34), // "on_pushButton_startStep_10_cl..."
QT_MOC_LITERAL(20, 567, 26), // "on_pushButton_stop_clicked"
QT_MOC_LITERAL(21, 594, 31), // "on_pushButton_ResetTest_clicked"
QT_MOC_LITERAL(22, 626, 21), // "showDeviceDialog_Slot"
QT_MOC_LITERAL(23, 648, 35), // "on_pushButton_clearErrorNum_c..."
QT_MOC_LITERAL(24, 684, 32) // "on_pushButton_clearOkNum_clicked"

    },
    "MainWindow\0Timer_100ms_Slot\0\0readMyComSlot\0"
    "on_pushButton_openCOM_clicked\0"
    "on_pushButton_sendConfig_clicked\0"
    "on_pushButton_save_clicked\0"
    "on_pushButton_openfile_clicked\0"
    "on_pushButton_openbin_clicked\0"
    "on_pushButton_updatebin_clicked\0"
    "on_pushButton_startStep_1_clicked\0"
    "on_pushButton_startStep_2_clicked\0"
    "on_pushButton_startStep_3_clicked\0"
    "on_pushButton_startStep_4_clicked\0"
    "on_pushButton_startStep_5_clicked\0"
    "on_pushButton_startStep_6_clicked\0"
    "on_pushButton_startStep_7_clicked\0"
    "on_pushButton_startStep_8_clicked\0"
    "on_pushButton_startStep_9_clicked\0"
    "on_pushButton_startStep_10_clicked\0"
    "on_pushButton_stop_clicked\0"
    "on_pushButton_ResetTest_clicked\0"
    "showDeviceDialog_Slot\0"
    "on_pushButton_clearErrorNum_clicked\0"
    "on_pushButton_clearOkNum_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  129,    2, 0x08 /* Private */,
       3,    0,  130,    2, 0x08 /* Private */,
       4,    0,  131,    2, 0x08 /* Private */,
       5,    0,  132,    2, 0x08 /* Private */,
       6,    0,  133,    2, 0x08 /* Private */,
       7,    0,  134,    2, 0x08 /* Private */,
       8,    0,  135,    2, 0x08 /* Private */,
       9,    0,  136,    2, 0x08 /* Private */,
      10,    0,  137,    2, 0x08 /* Private */,
      11,    0,  138,    2, 0x08 /* Private */,
      12,    0,  139,    2, 0x08 /* Private */,
      13,    0,  140,    2, 0x08 /* Private */,
      14,    0,  141,    2, 0x08 /* Private */,
      15,    0,  142,    2, 0x08 /* Private */,
      16,    0,  143,    2, 0x08 /* Private */,
      17,    0,  144,    2, 0x08 /* Private */,
      18,    0,  145,    2, 0x08 /* Private */,
      19,    0,  146,    2, 0x08 /* Private */,
      20,    0,  147,    2, 0x08 /* Private */,
      21,    0,  148,    2, 0x08 /* Private */,
      22,    0,  149,    2, 0x08 /* Private */,
      23,    0,  150,    2, 0x08 /* Private */,
      24,    0,  151,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->Timer_100ms_Slot(); break;
        case 1: _t->readMyComSlot(); break;
        case 2: _t->on_pushButton_openCOM_clicked(); break;
        case 3: _t->on_pushButton_sendConfig_clicked(); break;
        case 4: _t->on_pushButton_save_clicked(); break;
        case 5: _t->on_pushButton_openfile_clicked(); break;
        case 6: _t->on_pushButton_openbin_clicked(); break;
        case 7: _t->on_pushButton_updatebin_clicked(); break;
        case 8: _t->on_pushButton_startStep_1_clicked(); break;
        case 9: _t->on_pushButton_startStep_2_clicked(); break;
        case 10: _t->on_pushButton_startStep_3_clicked(); break;
        case 11: _t->on_pushButton_startStep_4_clicked(); break;
        case 12: _t->on_pushButton_startStep_5_clicked(); break;
        case 13: _t->on_pushButton_startStep_6_clicked(); break;
        case 14: _t->on_pushButton_startStep_7_clicked(); break;
        case 15: _t->on_pushButton_startStep_8_clicked(); break;
        case 16: _t->on_pushButton_startStep_9_clicked(); break;
        case 17: _t->on_pushButton_startStep_10_clicked(); break;
        case 18: _t->on_pushButton_stop_clicked(); break;
        case 19: _t->on_pushButton_ResetTest_clicked(); break;
        case 20: _t->showDeviceDialog_Slot(); break;
        case 21: _t->on_pushButton_clearErrorNum_clicked(); break;
        case 22: _t->on_pushButton_clearOkNum_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
