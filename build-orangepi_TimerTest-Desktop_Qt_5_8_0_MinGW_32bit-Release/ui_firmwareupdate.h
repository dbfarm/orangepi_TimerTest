/********************************************************************************
** Form generated from reading UI file 'firmwareupdate.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FIRMWAREUPDATE_H
#define UI_FIRMWAREUPDATE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_firmwareupdate
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_5;
    QComboBox *comboBox_com;
    QComboBox *comboBox_;
    QPushButton *pushButton_openCOM;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_3;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *firmwareupdate)
    {
        if (firmwareupdate->objectName().isEmpty())
            firmwareupdate->setObjectName(QStringLiteral("firmwareupdate"));
        firmwareupdate->resize(408, 314);
        verticalLayout_3 = new QVBoxLayout(firmwareupdate);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        comboBox_com = new QComboBox(firmwareupdate);
        comboBox_com->setObjectName(QStringLiteral("comboBox_com"));
        comboBox_com->setMinimumSize(QSize(70, 60));

        horizontalLayout_5->addWidget(comboBox_com);

        comboBox_ = new QComboBox(firmwareupdate);
        comboBox_->setObjectName(QStringLiteral("comboBox_"));
        comboBox_->setMinimumSize(QSize(70, 60));

        horizontalLayout_5->addWidget(comboBox_);

        pushButton_openCOM = new QPushButton(firmwareupdate);
        pushButton_openCOM->setObjectName(QStringLiteral("pushButton_openCOM"));
        pushButton_openCOM->setMinimumSize(QSize(70, 60));

        horizontalLayout_5->addWidget(pushButton_openCOM);


        verticalLayout_3->addLayout(horizontalLayout_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        lineEdit = new QLineEdit(firmwareupdate);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setMinimumSize(QSize(200, 30));

        verticalLayout->addWidget(lineEdit);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        pushButton = new QPushButton(firmwareupdate);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMinimumSize(QSize(0, 60));

        horizontalLayout_3->addWidget(pushButton);

        pushButton_2 = new QPushButton(firmwareupdate);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(0, 60));

        horizontalLayout_3->addWidget(pushButton_2);


        verticalLayout->addLayout(horizontalLayout_3);


        horizontalLayout_4->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_2 = new QLabel(firmwareupdate);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        label = new QLabel(firmwareupdate);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_3 = new QLabel(firmwareupdate);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_2->addWidget(label_3);

        label_4 = new QLabel(firmwareupdate);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_2->addWidget(label_4);


        verticalLayout_2->addLayout(horizontalLayout_2);


        horizontalLayout_4->addLayout(verticalLayout_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout_4);


        retranslateUi(firmwareupdate);

        QMetaObject::connectSlotsByName(firmwareupdate);
    } // setupUi

    void retranslateUi(QWidget *firmwareupdate)
    {
        firmwareupdate->setWindowTitle(QApplication::translate("firmwareupdate", "Form", Q_NULLPTR));
        comboBox_com->clear();
        comboBox_com->insertItems(0, QStringList()
         << QApplication::translate("firmwareupdate", "COM1", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "COM2", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "COM3", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "COM4", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "COM5", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "COM6", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "tty0", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "tty1", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "tty2", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "tty3", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "ttyUSB0", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "ttyUSB1", Q_NULLPTR)
        );
        comboBox_->clear();
        comboBox_->insertItems(0, QStringList()
         << QApplication::translate("firmwareupdate", "115200", Q_NULLPTR)
         << QApplication::translate("firmwareupdate", "230400", Q_NULLPTR)
        );
        pushButton_openCOM->setText(QApplication::translate("firmwareupdate", "\346\211\223\345\274\200\344\270\262\345\217\243", Q_NULLPTR));
        pushButton->setText(QApplication::translate("firmwareupdate", "\351\200\211\346\213\251\345\233\272\344\273\266", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("firmwareupdate", "\345\274\200\345\247\213\345\215\207\347\272\247", Q_NULLPTR));
        label_2->setText(QApplication::translate("firmwareupdate", "\347\250\213\345\272\217\345\244\247\345\260\217\357\274\232", Q_NULLPTR));
        label->setText(QApplication::translate("firmwareupdate", "123", Q_NULLPTR));
        label_3->setText(QApplication::translate("firmwareupdate", "\345\210\206\346\256\265\346\225\260\351\207\217\357\274\232", Q_NULLPTR));
        label_4->setText(QApplication::translate("firmwareupdate", "123", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class firmwareupdate: public Ui_firmwareupdate {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FIRMWAREUPDATE_H
