#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    QObject::connect(ui->pushButton_ok,SIGNAL(clicked()),this,SLOT(SetContOkSlot()));
    QObject::connect(ui->pushButton_cancel,SIGNAL(clicked()),this,SLOT(SetContCancelSlot()));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::SetTitelStr(const QString &title)
{
    Dialog::setWindowTitle(title);
}
void Dialog::SetDeviceNum(unsigned short num)
{
    Dialog::deviceNum = num;
}

void Dialog::SetContOkSlot()
{
    //////////////////////////////////////
    close();
}
void Dialog::SetContCancelSlot()
{
    //////////////////////////////////////
    close();
}

