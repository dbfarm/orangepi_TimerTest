#ifndef FIRMWAREUPDATE_H
#define FIRMWAREUPDATE_H

#include <QWidget>

namespace Ui {
class firmwareupdate;
}

class firmwareupdate : public QWidget
{
    Q_OBJECT

public:
    explicit firmwareupdate(QWidget *parent = 0);
    ~firmwareupdate();

private:
    Ui::firmwareupdate *ui;
};

#endif // FIRMWAREUPDATE_H
