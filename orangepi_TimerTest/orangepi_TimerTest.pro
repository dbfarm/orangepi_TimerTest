#-------------------------------------------------
#
# Project created by QtCreator 2018-12-07T16:38:36
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = orangepi_TimerTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    firmwareupdate.cpp

HEADERS  += mainwindow.h \
    config.h \
    dialog.h \
    firmwareupdate.h

FORMS    += mainwindow.ui \
    dialog.ui \
    firmwareupdate.ui

RC_FILE += timer.rc

RESOURCES += \
    source.qrc
