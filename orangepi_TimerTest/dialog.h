#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "mainwindow.h"
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    unsigned short deviceNum=1;
    //QString SetTitelStr[41];
    void SetTitelStr(const QString &);
    void SetDeviceNum(unsigned short num);
private:
    Ui::Dialog *ui;
    void SetContMode();
    void ReadOptionData();
private slots:
    void SetContOkSlot();
    void SetContCancelSlot();
};

#endif // DIALOG_H
