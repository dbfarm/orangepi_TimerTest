#ifndef CONFIG_H
#define CONFIG_H

#define SYSINFORSIZE  (24+144)

typedef struct {
    unsigned short objWeiboClickTimeOn;
    unsigned short objWeiboClickTimeOnMaxMin;
    unsigned short objWeiboClickTimeOff;
    unsigned short objWeiboClickTimeOffMaxMin;

    unsigned short objShaokaoClickTimeOn;
    unsigned short objShaokaoClickTimeOnMaxMin;
    unsigned short objShaokaoClickTimeOff;
    unsigned short objShaokaoClickTimeOffMaxMin;
}stepConfig_t;

typedef struct{
    stepConfig_t stepConfig[10];//存储配置好的1-10档参数
    unsigned short objAllTime;
    unsigned short objAllTimeMaxMin;
    unsigned char objVolt;//电压
}TimerConfig_t;

#endif // CONFIG_H
