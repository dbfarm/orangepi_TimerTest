#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QFileDialog>

#include <QDateTime>
#include <QTimer>

#include <QSerialPort>
#include "config.h"

#include "dialog.h"

#define APPVERSION "V0.1"

#define RESETTEST 0x10 //更换定时器后重新检测
#define SETSTEP 0x12  //设置检测步骤
#define WRITECOMM  0xfe
#define READCOMM   0xf0
#define SETADDRESS 0xff
#define SETADDRESSSUM  0x15

#define LED_GRAY 0
#define LED_GREEN 1
#define LED_ORANGE 2
#define LED_YELLOW 3
#define LED_RED 4

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QString appVersion=QString(APPVERSION);
    //固件升级相关
    QByteArray *firmwareArray=NULL;//设备固件，用于设备升级
    QByteArray FirmwareVersion;
    unsigned char UpdateFirmwareFlag=0;
    unsigned char NotRecDataTimeCount = 0;

    unsigned char sentDatas[200];//发送的设置数据
    TimerConfig_t TimerConfig;//缓存配置数据
    TimerConfig_t TimerConfig_device[120];//缓存配置数据
    QTimer timer_100ms;//用来定时
    QSerialPort *myCom=NULL;
    QByteArray RecBuffer;

    QString NowDeviceNumString;
    unsigned char GetDeviceStatusFlag = 0;//标志着开始获取设备信息了
    unsigned char ShowThisDeviceStatus=0;//显示当前的设备信息

    unsigned char startFlag=0;//暂停和开始
    unsigned char DeviceRunStatus;//控制检测板的状态，
    unsigned char DeviceTestStep;//检测步骤
    unsigned short NowStepTestTime_s;//记录当前档位检测了多少秒
    unsigned short NowTestAllTime_s;//记录本组定时器检测的时间
    unsigned short SetNowStepTestTime_s;//用来设置当前档位检测多久的，用于自动暂停等待操作员换好档位后再由操作员启动检测

    int TestErrorNum=0;//记录故障的数量，包括总时间故障
    int TestAllTimeErrorNum=0;//记录总时间故障的数量
    int TestOkNum = 0;//记录良品数量

    void UpdateTimerConfigData();//将控件中内容更新到TimerConfig里面

    char OpenCom();
    void SendTestReset(unsigned char dev);//复位检测设备重新检测定时器，例如当前定时器检测好以后就复位一下等待下一次定时器插上
    void SendTestStep(unsigned char dev,unsigned char step);//发送指令，控制设备检测步骤;dev是目标，0xff标示所有的板子都进行step档位的检测
    void SendConfigDataToAll();//将配置信息发送到所有的板子上面
    void GetDeviceStatus(unsigned char dev);//获取对应设备的当前状态
    void GetDeviceConfigData(unsigned char dev);//获取对应设备的配置参数

    char OpenConfigFile(const QString &fileName);
    void ChangeAppInfor(void);//保存软件的设置
    void SetStatusLed(unsigned char index);//设置状态LED的显示颜色
private:
    Ui::MainWindow *ui;

    void SetDeviceButtonText(void);//设置120个按键的内容
private slots:
    void Timer_100ms_Slot();//定时100ms 的回调
    void readMyComSlot();
    void on_pushButton_openCOM_clicked();
    void on_pushButton_sendConfig_clicked();
    void on_pushButton_save_clicked();
    void on_pushButton_openfile_clicked();
    void on_pushButton_openbin_clicked();
    void on_pushButton_updatebin_clicked();

    void on_pushButton_startStep_1_clicked();
    void on_pushButton_startStep_2_clicked();
    void on_pushButton_startStep_3_clicked();
    void on_pushButton_startStep_4_clicked();
    void on_pushButton_startStep_5_clicked();
    void on_pushButton_startStep_6_clicked();
    void on_pushButton_startStep_7_clicked();
    void on_pushButton_startStep_8_clicked();
    void on_pushButton_startStep_9_clicked();
    void on_pushButton_startStep_10_clicked();

    void on_pushButton_stop_clicked();
    void on_pushButton_ResetTest_clicked();

    void showDeviceDialog_Slot();//用户按下设备对应的编号，弹出设备的信息
    void on_pushButton_clearErrorNum_clicked();
    void on_pushButton_clearOkNum_clicked();
};

#endif // MAINWINDOW_H
